﻿namespace Extreme.Cartesian.Project
{
    public class ProjectSettings
    {
        public string Name { get; }

        public ProjectSettings(string name)
        {
            Name = name;
        }
    }
}
