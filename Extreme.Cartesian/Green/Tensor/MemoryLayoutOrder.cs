﻿namespace Extreme.Cartesian.Green.Tensor
{
    public enum MemoryLayoutOrder 
    {
        AlongVertical,
        AlongLateral
    }
}
