﻿namespace Extreme.Cartesian.Green.Scalar
{
    public enum IntegrationType
    {
        VolumeToVolume,
        VolumeToPoint,
        PointToVolume,
        PointToPoint,
    }
}
