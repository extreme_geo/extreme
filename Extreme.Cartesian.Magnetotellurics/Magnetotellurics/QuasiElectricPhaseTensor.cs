﻿namespace Porvem.Magnetotellurics
{
    public class QuasiElectricPhaseTensor : PhaseTensor
    {
        public QuasiElectricPhaseTensor(double xx, double xy, double yx, double yy)
            : base(xx, xy, yx, yy)
        {
        }
    }
}
