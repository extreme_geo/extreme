﻿namespace Extreme.Cartesian.Magnetotellurics
{
    public enum Polarization
    {
        X = 1,
        Y = 2,
    }
}
