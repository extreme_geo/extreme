﻿namespace Extreme.Fgmres
{
    public enum InitialGuess
    {
        EqualsZero = 0,
        UserSupplied = 1,
    }
}
