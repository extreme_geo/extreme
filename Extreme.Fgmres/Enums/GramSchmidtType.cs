﻿namespace Extreme.Fgmres
{
    public enum GramSchmidtType
    {
        IterativeModified = 1,
        IterativeClassical = 3,
    }
}
