﻿namespace Extreme.Fgmres
{
    public enum ResidualAtRestart : long
    {
        ComputeTheTrue = 1,
        UseRecurrenceFormula = 0,
    }
}
