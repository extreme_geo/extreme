﻿namespace Extreme.Fgmres
{
    public enum BackwardErrorChecking
    {
        CheckArnoldiOnly = 0,
        CheckWithWarning = 1,
        CheckWithException = 2,
    }
}
