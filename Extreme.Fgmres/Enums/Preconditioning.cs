﻿namespace Extreme.Fgmres
{
    public enum Preconditioning : long
    {
        None = 0,
        Left = 1,
        Right = 2,
        DoubleSide = 3,
    }
}
