﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Extreme.Core.Model
{
    public interface IResistivityLayer1D
    {
        double Resistivity { get; }
    }
}
