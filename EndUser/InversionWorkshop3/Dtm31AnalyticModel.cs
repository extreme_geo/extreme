﻿using Extreme.Cartesian.Model;
using Extreme.Model;

using static System.Math;

namespace InversionWorkshop3
{
    class Dtm31AnalyticModel : AnalyticModel
    {
        private double _bkgSigma = 0.001;
        private double _localConductor = 1.0 / 3.0;
        private double _regionalConductor = 1.0 / 0.3;

        public Dtm31AnalyticModel(CartesianSection1D section1D, decimal maxZ, decimal minZ) : base(section1D, maxZ, minZ)
        {
        }

        public override double GetValue(decimal x, decimal y, decimal z)
        {
            if (x >= -8000 && x < 0 &&
                y >= -8000 && y < 0 &&
                z >= 0 && z <= 4000)
                return _localConductor;

            if (x > 0 && y < 0)
                return Determine(x, y, z);

            return _bkgSigma;
        }

        private readonly static double xb = 25;
        private readonly static double z0 = 3 * Sqrt(2);
        private readonly static double C = PI / (4 * xb);
        private static readonly double ExpZ0 = z0 * Exp(PI / 4);

        private double CalculateZ(double x)
        {
            var x1 = x/1000;

            var z = (ExpZ0 * Exp(-C * x1) * Sin(C * x1) + 1) * 1000;
            return z;
        }

        private double Determine(decimal x, decimal y, decimal z)
        {
            if (CalculateZ((double)x) > (double)z)
                return _regionalConductor;

            return _bkgSigma;
        }
    }
}
