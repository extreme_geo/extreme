﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Extreme.Cartesian.Model;
using Extreme.Core.Model;
using Extreme.Model;

namespace InversionWorkshop3
{
    public class Dtm30ModelCreater
    {
        public NonMeshedModel CreateNonMeshedModel()
        {
            var section1D = CreateSection1D();

            var model = new NonMeshedModel(section1D);

            model.AddAnomaly(new NonMeshedAnomaly(conductivity: 1.0/0.3,
                                        x: new Direction(0, 40000m),
                                        y: new Direction(-108000m, 108000m),
                                        z: new Direction(100m, 3900m)));

            model.AddAnomaly(new NonMeshedAnomaly(conductivity: 1.0/3.0,
                                        x: new Direction(-8000m, 8000m),
                                        y: new Direction(-8000m, 8000m),
                                        z: new Direction(100m, 3900m)));

            return model;
        }

        private ISection1D<Layer1D> CreateSection1D()
        {
            IEnumerable<Sigma1DLayer> layers = new[]
            {
                new Sigma1DLayer(0, 0),
                new Sigma1DLayer(4000m, 0.001),
                new Sigma1DLayer(0, 0.001),
            };

            var section1D = new Section1D<Sigma1DLayer>(0m, layers.ToArray());

            return section1D;
        }
    }
}
