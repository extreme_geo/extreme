﻿using System;
using System.Collections.Generic;
using System.Linq;
using Extreme.Cartesian.Model;
using Extreme.Model;

using static System.Math;

namespace InversionWorkshop3
{
    public class Dtm31ModelCreater
    {
        public AnalyticModel CreateAnalyticModel()
        {
            var section1D = CreateSection1D();
            var minZ = 0;
            var maxZ = 4000;

            return new Dtm31AnalyticModel(section1D, maxZ, minZ);
        }

        private CartesianSection1D CreateSection1D()
        {
            IEnumerable<Sigma1DLayer> layers = new[]
            {
                    new Sigma1DLayer(0, 0),
                    new Sigma1DLayer(4000m, 0.001),
                    new Sigma1DLayer(0, 0.001),
                };

            var section1D = new CartesianSection1D(0m, layers.ToArray());

            return section1D;
        }
    }
}
