﻿using System.Collections.Specialized;
using System.Diagnostics;
using System.IO;
//using System.Management.Instrumentation;
using Extreme.Cartesian.Model;
using Extreme.Core;
using Extreme.Core.Logger;
using Extreme.Model;
using Extreme.Model.Topography;

namespace InversionWorkshop3
{
    class Program
    {
        static void Main(string[] args)
        {
            var dir = @"Z:\ETH\Bari\DEC_17_2015";

            //CreateDtm30(dir);
            //CreateDtm31(dir);

            CreateModelWithTopography(dir);
        }

        private static void CreateModelWithTopography(string dir)
        {
            var path = @"Z:\Data\Topo\MT_data_for_Alexey\PhilippineSea_topo.xyz\xyz_double.bin";


            var logger = new ConsoleLogger();

            var startXInKm = -5800;
            var srartYInKm = -5400;

            var totalXInKm = 13000;
            var totalYInKm = 12000;

            var mesh = new MeshParameters(200, 200, 40) { UseGeometricStepAlongZ = false };
            var mb = new ManualBoundaries(startXInKm * 1000, srartYInKm * 1000, (startXInKm + totalXInKm) * 1000m, (srartYInKm + totalYInKm) * 1000m);

            logger.WriteStatus("Loading topo data from disk...");
            var topography = DiscretePhilippineTopographyProvider2.CreateProvider(path);

            logger.WriteStatus("Creating creator");

            var converter = new TopographyModelConverter(topography, mb, logger)
            {
                MinZ = (decimal)topography.GetMinZ(),
                MaxZ = (decimal)topography.GetMaxZ(),
            };


            logger.WriteStatus($"\tzMin = {converter.MinZ}");
            logger.WriteStatus($"\tzMax = {converter.MaxZ}");

            logger.WriteStatus("Creating model");

            var cartesianModel = converter.Convert(mesh);

            var modelPath = Path.Combine(dir, "topo.xmodel");
            var vtkPath = Path.Combine(dir, "topo.vtr");

            logger.WriteStatus("Writing model to disk...");
            ModelWriter.SaveWithPlainTextAnomaly(modelPath, cartesianModel);


            logger.WriteStatus("Writing vtr to disk...");
            var writer = new VtkExport.XmlVtkWriter { ScaleZ = 500 };

            var xdoc = writer.Write(cartesianModel);
            xdoc.Save(vtkPath);

        }

        private static void CreateDtm31(string dir)
        {
            var creater = new Dtm31ModelCreater();
            var model = creater.CreateAnalyticModel();
            var mp = new MeshParameters(108, 160, 40);
            var mb = new ManualBoundaries(-8000, -150000, 100000, 10000);

            var converter = new AnalyticToCartesianModelConverter(model, mb, new ConsoleLogger());
            converter.IntegrateStepsAlongX = 4;
            converter.IntegrateStepsAlongY = 4;
            converter.IntegrateStepsAlongZ = 4;
            var cartesianModel = converter.Convert(mp);

            var modelPath = Path.Combine(dir, "dtm31.xmodel");
            var vtkPath = Path.Combine(dir, "dtm31.vtr");

            ModelWriter.SaveWithPlainTextAnomaly(modelPath, cartesianModel);
            var writer = new VtkExport.XmlVtkWriter();
            writer.ScaleZ = 25;
            var xdoc = writer.Write(cartesianModel);
            xdoc.Save(vtkPath);

        }

        private static void CreateDtm30(string dir)
        {
            var creater = new Dtm30ModelCreater();
            var model = creater.CreateNonMeshedModel();
            var mp = new MeshParameters(48, 108, 39);
            var converter = new NonMeshedToCartesianModelConverter(model);
            var cartesianModel = converter.Convert(mp);


            var modelPath = Path.Combine(dir, "dtm30.xmodel");
            var vtkPath = Path.Combine(dir, "dtm30.vtr");


            ModelWriter.SaveWithPlainTextAnomaly(modelPath, cartesianModel);
            var writer = new VtkExport.XmlVtkWriter();
            var xdoc = writer.Write(cartesianModel);
            xdoc.Save(vtkPath);
        }
    }
}
