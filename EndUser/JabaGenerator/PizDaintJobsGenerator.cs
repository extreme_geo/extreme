﻿using System;
using System.Collections.Generic;
using System.IO;
using Extreme.Cartesian.Forward;
using Extreme.Model;
using Extreme.Model.SimpleCommemi3D;
using Extreme.Cartesian.Project;
using Extreme.Core;

namespace JabaGenerator
{
    public class PizDaintJobsGenerator
    {
        private readonly string _path;
        private readonly double[] _frequencies;
        private readonly string _lastDirectory;
        private readonly List<string> _jobs = new List<string>();

        private readonly double _residual = 1E-8;
        

        public PizDaintJobsGenerator(string path, params double[] frequencies)
        {
            _path = path;
            _frequencies = frequencies;

            if (!Directory.Exists(_path))
                Directory.CreateDirectory(_path);

            _lastDirectory = new DirectoryInfo(_path).Name;
        }

        public string[] GetAllJobs()
        {
            return _jobs.ToArray();
        }

        public void GenerateManyJobs(int nx, int ny, int nz)
        {
            var modeFileName = GenerateModelFile(nx, ny, nz);
            
            for (int mpiSize = 64; mpiSize <= 1024; mpiSize *= 2)
            {
                var projectFileName = GenerateProject(modeFileName, _frequencies[0], mpiSize);
                var jobFileName = GenerateJob(projectFileName, mpiSize);
                _jobs.Add(jobFileName);
            }
        }

        private string GenerateModelFile(int nx, int ny, int nz)
        {
            var mesh = new MeshParameters(nx, ny, nz)
            {
                UseGeometricStepAlongZ = false,
                GeometricRation = 1.06
            };

            var settings = new CommemiModelSettings(mesh)
                .WithConductivities(10, 0.001);
            var modelFileName = $"commemi_nx{nx}_ny{ny}_nz{nz}.xset";

            ModelSettingsSerializer.SaveToXml(Path.Combine(_path, modelFileName), settings);

            return modelFileName;
        }


        private string GenerateJob(string projectFileName, int totalMpi)
        {
            var template = new PizDaintJobTemplate(totalMpi)
            {
                JobName = Path.GetFileNameWithoutExtension(projectFileName),
                ProjectPath = projectFileName,
                MaxTime = new TimeSpan(0, 40, 00)
            };

            var jobFullPath = Path.Combine(_path, GenerateJobFileName(template));
            var job = template.TransformText();

            job = job.Replace("\r\n", "\n");

            File.WriteAllText(jobFullPath, job);

            return jobFullPath;
        }

        private string GenerateProject(string modelFileName, double frequency, int mpiSize)
        {
            var project = ForwardProject.NewWithFrequencies(frequency);

            project.ForwardSettings.Residual = _residual;
            project.ForwardSettings.NumberOfHankels = 1;
            project.Extreme
                .WithResultsPath($"results_{mpiSize:0000}mpi")
                .WithModelFile(modelFileName)
                .WithObservations(new ObservationLevel(0, 0, 0));

            var projectFileName = GenerateProjectName(mpiSize);

            ForwardProjectSerializer.Save(project, Path.Combine(_path, projectFileName));
            return projectFileName;
        }


        private string GenerateProjectName(int mpiSize)
            => $"start_{mpiSize:0000}mpi.xproj";

        private static string GenerateJobFileName(PizDaintJobTemplate template)
        {
            return
                $"job_mpi{template.TotalNumberOfMpiProcesses:0000}.job";
        }
    }
}
