﻿using System;
using System.Diagnostics;
using System.IO;

namespace JabaGenerator
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length < 2)
            {
                WriteHelp();
                return;
            }

            if (args[1] == "0")
            {
                var generator = new PizDaintJobsGenerator(args[0], frequencies:0.01);
                generator.GenerateManyJobs(512, 512, 128);
                RunAllJobs(generator.GetAllJobs());
            }

            if (args[1] == "1")
            {
                var generator = new PizDaintJobsGenerator(args[0], 0.01);
                generator.GenerateManyJobs(512, 512, 128);
                RunAllJobs(generator.GetAllJobs());
            }
        }

        private static void RunAllJobs(string[] allJobs)
        {
            for (int i = 0; i < allJobs.Length; i++)
            {
                Console.WriteLine("Run job {0}", allJobs[i]);
                RunJob(allJobs[i]);
            }
        }

        private static void RunJob(string job)
        {
            const string sbatch = @"sbatch";

            var psi = new ProcessStartInfo
            {
                FileName = sbatch,
                UseShellExecute = false,
                RedirectStandardOutput = true,
                Arguments =  Path.GetFileName(job),
                WorkingDirectory = Path.GetDirectoryName(Path.GetFullPath(job)),
            };

            Process p = Process.Start(psi);
            string strOutput = p.StandardOutput.ReadToEnd();
            p.WaitForExit();
            Console.WriteLine(strOutput);
        }


        private static void WriteHelp()
        {
            Console.WriteLine("JabaGenerator.exe path nummer, where 'path' is the output path, nummer is nummer");
        }
    }
}
