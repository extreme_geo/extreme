﻿using System;

namespace JabaGenerator
{
    public partial class PizDaintJobTemplate
    {
        public int TotalNumberOfMpiProcesses { get; }
        
        public PizDaintJobTemplate(int totalNumberOfMpiProcesses)
        {
            this.TotalNumberOfMpiProcesses = totalNumberOfMpiProcesses;
        }

        public string JobName { get; set; }
        public string ProjectPath { get; set; }
        public TimeSpan MaxTime { get; set; }
    }
}
