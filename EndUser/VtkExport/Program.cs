﻿using System;
using System.IO;
using Extreme.Model;

namespace VtkExport
{
    static class Program
    {
        static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                Console.WriteLine(@"Need to provide 1 or 2 arguments");
                Console.WriteLine(@"Argument 1: Model File *.xmodel (required)");
                Console.WriteLine(@"Argument 2: VTK File (if not provided, model file name will be used with corresponding extension)");
                return;
            }

            var modelFileName = args[0];

            var vtkFileName = args.Length > 1 ?
                args[1] :
                Path.Combine(Path.GetDirectoryName(modelFileName), Path.GetFileNameWithoutExtension(modelFileName) + ".vtr");

            var model = ModelGenUtils.LoadCartesianModel(modelFileName);

            int nx = model.Nx;
            int ny = model.Ny;
            int nz = model.Nz;

            var sigma = new double[nx, ny, nz];

            for (int i = 0; i < nx; i++)
                for (int j = 0; j < ny; j++)
                    for (int k = 0; k < nz; k++)
                        sigma[i, j, k] = model.Anomaly.Sigma[i, j, k];

            var writer = new XmlVtkWriter();
            var xdoc = writer.Write(model, sigma);
            xdoc.Save(vtkFileName);
        }
    }
}
