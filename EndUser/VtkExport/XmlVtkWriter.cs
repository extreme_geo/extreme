﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Markup;
using System.Xml.Linq;
using Extreme.Cartesian.Model;
using Extreme.Core;

namespace VtkExport
{
    public class XmlVtkWriter
    {
        private const string VtkFileElement = "VTKFile";
        private const string TypeAttr = "type";
        private const string VersionAttr = "version";

        private const string CurrentVersion = "1.0";

        private const string WholeExtentAttr = "WholeExtent";

        private const string PieceElement = "Piece";
        private const string PieceExtentAttr = "Extent";

        private const string PointDataElement = "PointData";
        private const string CellDataElement = "CellData";
        private const string CoordinatesElement = "Coordinates";


        private const string AppendedDataElement = "AppendedData";
        private const string AppendedDataEncodingAttr = "encoding";


        private const string DataArray = "DataArray";


        public float ScaleX { get; set; } = 1;
        public float ScaleY { get; set; } = 1;
        public float ScaleZ { get; set; } = 1;

        public XDocument Write(CartesianModel model)
        {
            var dataType = DataType.RectilinearGrid;

            var byteOrder = BitConverter.IsLittleEndian ? "LittleEndian" : "BigEndian";

            return new XDocument(new XElement(VtkFileElement,
                                    new XAttribute(TypeAttr, dataType.Name),
                                    new XAttribute(VersionAttr, CurrentVersion),
                                    new XAttribute("byte_order", byteOrder),
                                            ToDataTypeXElement(model, model.Anomaly.Sigma, dataType)));
            //ToAppendDataXElement(model)));
        }


        public XDocument Write(CartesianModel model, double[,,] sigma)
        {
            var dataType = DataType.RectilinearGrid;

            var byteOrder = BitConverter.IsLittleEndian ? "LittleEndian" : "BigEndian";

            return new XDocument(new XElement(VtkFileElement,
                                    new XAttribute(TypeAttr, dataType.Name),
                                    new XAttribute(VersionAttr, CurrentVersion),
                                    new XAttribute("byte_order", byteOrder),
                                            ToDataTypeXElement(model, sigma, dataType)));
            //ToAppendDataXElement(model)));
        }

        private static XElement ToAppendDataXElement(CartesianModel model)
        {

            int length = model.Nx * model.Ny * model.Nz;
            float[] sigmas = new float[length];


            int counter = 0;

            for (int k = 0; k < model.Nz; k++)
                for (int j = 0; j < model.Ny; j++)
                    for (int i = 0; i < model.Nx; i++)
                        sigmas[counter++] = (float)model.Anomaly.Sigma[i, j, k];



            var xelem = new XElement(AppendedDataElement, new XAttribute(AppendedDataEncodingAttr, "raw"), "_");

            var writer = xelem.CreateWriter();


            var bytesLength = BitConverter.GetBytes(sigmas.Length);
            writer.WriteValue(Convert.ToBase64String(bytesLength));

            for (int i = 0; i < sigmas.Length; i++)
            {
                var bytes = BitConverter.GetBytes(sigmas[i]);
                writer.WriteValue(Convert.ToBase64String(bytes));
                //writer.WriteBinHex(bytes, 0, bytes.Length);
            }

            writer.Close();


            return xelem;
        }



        private XElement ToDataTypeXElement(CartesianModel model, double[,,] sigma, DataType dataType)
        {
            var nx = model.Nx;
            var ny = model.Ny;
            var nz = model.Nz;

            return new XElement(dataType.Name,
                new XAttribute(WholeExtentAttr, $"0 {nx} 0 {ny} 0 {nz}"),
                new XElement(PieceElement,
                    new XAttribute(PieceExtentAttr, $"0 {nx} 0 {ny} 0 {nz}"),
                    //new XElement(PointDataElement, ""),
                    CoordinatesToXElements(model),
                    CellDataToXElement(model, sigma)));

        }

        private static XElement CellDataToXElement(CartesianModel model, double[,,] sigma)
        {
            int length = model.Nx * model.Ny * model.Nz;
            float[] sigmas = new float[length];

            int counter = 0;

            for (int k = 0; k < model.Nz; k++)
                for (int j = 0; j < model.Ny; j++)
                    for (int i = 0; i < model.Nx; i++)
                        sigmas[counter++] = (float)sigma[i, j, k];


            var xdata = new XElement(DataArray, new XAttribute("type", "Float32"),
                    new XAttribute("Name", nameof(sigmas)),
                    new XAttribute("format", "binary"));


            var writer = xdata.CreateWriter();


            var bytes = new List<byte>(sigmas.Length * 4 + 4);

            int lengthInBytes = sigmas.Length * 4;
            var bytes222 = BitConverter.GetBytes(lengthInBytes);
            bytes.AddRange(bytes222);

            for (int i = 0; i < sigmas.Length; i++)
                bytes.AddRange(BitConverter.GetBytes(sigmas[i]));

            writer.WriteValue(Convert.ToBase64String(bytes.ToArray()));

            writer.Close();


            //var xdata = new XElement(DataArray, new XAttribute("type", "Float32"),
            //    new XAttribute("Name", "sigmas"),
            //    new XAttribute("format", "ascii"),
            //    XmlUtils.ToString(sigmas));


            //var xdata = new XElement(DataArray, new XAttribute("type", "Float32"),
            //    new XAttribute("Name", "sigmas"),
            //    new XAttribute("format", "appended"),
            //    new XAttribute("offset", "0"));

            return new XElement(CellDataElement, xdata);
        }

        private XElement CoordinatesToXElements(CartesianModel model)
        {
            var xCoords = GetXCoords(model);
            var yCoords = GetYCoords(model);
            var zCoords = GetZCoords(model);

            return new XElement(CoordinatesElement,
                ToDataArray(xCoords, nameof(xCoords)),
                ToDataArray(yCoords, nameof(yCoords)),
                ToDataArray(zCoords, nameof(zCoords)));
        }


        private static XElement ToDataArray(float[] values, string name)
            => new XElement(DataArray,
                    new XAttribute("type", "Float32"),
                    new XAttribute("Name", name),
                    new XAttribute("format", "ascii"),
                    new XAttribute("RangeMin", values.Min()),
                    new XAttribute("RangeMax", values.Max()),
                    XmlUtils.ToString(values));

        private float[] GetXCoords(CartesianModel model)
            => GetCoords(model.Nx, (float)model.LateralDimensions.CellSizeX, ScaleX);

        private float[] GetYCoords(CartesianModel model)
            => GetCoords(model.Ny, (float)model.LateralDimensions.CellSizeY, ScaleY);

        private static float[] GetCoords(int n, float size, float scale)
        {
            var coords = new float[n + 1];

            for (int i = 0; i < coords.Length; i++)
                coords[i] = size * i * scale;

            return coords;
        }

        private float[] GetZCoords(CartesianModel model)
        {
            var anomaly = model.Anomaly;
            var coords = new float[anomaly.Layers.Count + 1];

            for (int i = 0; i < coords.Length - 1; i++)
                coords[i] = (float)anomaly.Layers[i].Depth * ScaleZ;

            var lastLayer = anomaly.Layers.Last();
            coords[coords.Length - 1] = (float)(lastLayer.Depth + lastLayer.Thickness) * ScaleZ;

            return coords;
        }
    }
}
