﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VtkExport
{
    public class DataType
    {
        public string Name { get; }
        public string DefaultExtension { get; }

        private DataType(string name, string defaultExtension)
        {
            Name = name;
            DefaultExtension = defaultExtension;
        }

        /// <summary>
        /// Serial vtkImageData (structured)
        /// </summary>
        public static DataType ImageData = new DataType("ImageData", "vti");

        /// <summary>
        /// Serial vtkPolyData(unstructured)
        /// </summary>
        public static DataType PolyData = new DataType("PolyData", "vtp");

        /// <summary>
        /// Serial vtkRectilinearGrid(structured)
        /// </summary>
        public static DataType RectilinearGrid = new DataType("RectilinearGrid", "vtr");

        /// <summary>
        /// Serial vtkStructuredGrid(structured)
        /// </summary>
        public static DataType StructuredGrid = new DataType("StructuredGrid", "vts");

        /// <summary>
        /// Serial vtkUnstructuredGrid(unstructured)
        /// </summary>
        public static DataType UnstructuredGrid = new DataType("UnstructuredGrid", "vtu");

        /// <summary>
        /// Parallel vtkImageData(structured)
        /// </summary>
        public static DataType PImageData = new DataType("PImageData", "pvti");

        /// <summary>
        /// Parallel vtkPolyData(unstructured)
        /// </summary>
        public static DataType PPolyData = new DataType("PPolyData", "pvtp");

        /// <summary>
        /// Parallel vtkRectilinearGrid(structured)
        /// </summary>
        public static DataType PRectilinearGrid = new DataType("PRectilinearGrid", "pvtr");

        /// <summary>
        /// Parallel vtkStructuredGrid(structured)
        /// </summary>
        public static DataType PStructuredGrid = new DataType("PStructuredGrid", "pvts");

        /// <summary>
        /// Parallel vtkUnstructuredGrid(unstructured)
        /// </summary>
        public static DataType PUnstructuredGrid = new DataType("PUnstructuredGrid", "pvtu");

    }
}
