function convert2tvk_arr(xs,ys,sig2,fout)

fd=fopen('anomaly_shape.dat');

ww=textscan(fd,'%d %d %d',1);
nx=ww{1};
ny=ww{2};
nz=ww{3};
ww=textscan(fd,'%f %f ',1);
dx=ww{1};
dy=ww{2};
ww=textscan(fd,'%f');
z=ww{1};
fclose(fd);
xb=double(xs):dx:double(xs+nx*dx);
yb=double(ys):dy:double(ys+ny*dy);
zb=z;



fd=fopen([fout '.vtk'],'w');
fprintf(fd,'%s \n ','# vtk DataFile Version 3.0');
fprintf(fd,'%s \n','Anomalous conductivity');
fprintf(fd,'%s \n','BINARY');
fprintf(fd,'%s \n','DATASET RECTILINEAR_GRID');
fprintf(fd,'%s %d %d %d \n','DIMENSIONS',nx+1,ny+1,nz+1);
fprintf(fd,'%s %d %s \n', 'X_COORDINATES', nx+1, 'float');
fwrite(fd,xb,'real*4','ieee-be');
%fprintf(fd,'%f ',xb);
fprintf(fd,'\n');
fprintf(fd,'%s %d %s \n', 'Y_COORDINATES', ny+1, 'float');
fwrite(fd,yb,'real*4','ieee-be');
%fprintf(fd,'%f ',yb);
fprintf(fd,'\n');
fprintf(fd,'%s %d %s \n', 'Z_COORDINATES', nz+1, 'float');
%fprintf(fd,'%f ',zb);
fwrite(fd,zb,'real*4','ieee-be');
fprintf(fd,'\n');
fprintf(fd,'%s %d \n', 'CELL_DATA', nx*ny*nz);
fprintf(fd,'%s \n', 'FIELD FieldData 1');
fprintf(fd,'%s %d %d %s \n', 'sigma', 1, nx*ny*nz, 'float');
%fprintf(fd,'%s \n','SCALARS scalars float');
%fprintf(fd,'%s \n','LOOKUP_TABLE default');

%fprintf(fd,'%f \n',sig2);
fwrite(fd,sig2,'real*4','ieee-be');
fclose(fd);

end