﻿using System;
using Extreme.Cartesian.FftW;
using Extreme.Parallel;

namespace ExtremeEmjoyMt
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length < 1)
            {
                RunDemoMode();
            }
            else
            {
                RunFullVersion(args[0]);
            }
        }


        private static void RunDemoMode()
        {
            Console.WriteLine(@"I am extrEMjoy MT, please provide *.xproj file");
        }

        private static void RunFullVersion(string projectFileName)
        {
            using (var mpi = Mpi.Init())
            using (var memoryProvider = new FftWMemoryProvider())
            using (var runner = new ExtremeJoyMtSolverRunner(projectFileName, memoryProvider, mpi))
            {
                runner.Run();
            }
        }
    }
}
