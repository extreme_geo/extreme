﻿using System;
using System.Numerics;
using Extreme.Inversion.Observed;
using Porvem.Magnetotellurics;

namespace ExtremeEmjoyMt
{
    public class Noiser
    {
        private readonly double _percent;
        private readonly Random _rnd;

        public Noiser(double percent)
        {
            _percent = percent;
            _rnd = new Random();
        }


        public void AddRandomNoises(ImpedanceTensor[,] tensor)
        {
            var d1 = tensor.GetLength(0);
            var d2 = tensor.GetLength(1);

            for (int i = 0; i < d1; i++)
            {
                for (int j = 0; j < d2; j++)
                {
                    var xx = AddRandomNoises(tensor[i, j].Xx);
                    var xy = AddRandomNoises(tensor[i, j].Xy);
                    var yx = AddRandomNoises(tensor[i, j].Yx);
                    var yy = AddRandomNoises(tensor[i, j].Yy);

                    tensor[i, j] = new ImpedanceTensor(xx, xy, yx, yy);
                }
            }
        }

        public void AddRandomNoises(MagneticTipper[,] tippers)
        {
            var d1 = tippers.GetLength(0);
            var d2 = tippers.GetLength(1);

            for (int i = 0; i < d1; i++)
            {
                for (int j = 0; j < d2; j++)
                {
                    var zx = AddRandomNoises(tippers[i, j].Zx);
                    var zy = AddRandomNoises(tippers[i, j].Zy);

                    tippers[i, j] = new MagneticTipper(zx, zy);
                }
            }
        }

        private Complex AddRandomNoises(Complex value)
        {
            var rndRe = 2 * (_rnd.NextDouble() - 0.5);
            var rndIm = 2 * (_rnd.NextDouble() - 0.5);
            var changeRe = value.Magnitude * (_percent / 100) * rndRe;
            var changeIm = value.Magnitude * (_percent / 100) * rndIm;
            return new Complex(value.Real + changeRe, value.Imaginary + changeIm);
        }

        public static void ChangeWeights(TensorWeights[,] weights, double epsilon)
        {
            var d1 = weights.GetLength(0);
            var d2 = weights.GetLength(1);

            for (int i = 0; i < d1; i++)
            {
                for (int j = 0; j < d2; j++)
                {
                    var xx = weights[i, j].Xx / epsilon;
                    var xy = weights[i, j].Xy / epsilon;
                    var yx = weights[i, j].Yx / epsilon;
                    var yy = weights[i, j].Yy / epsilon;

                    weights[i, j] = new TensorWeights(xx, xy, yx, yy);
                }
            }
        }

        public static void ChangeWeights(TipperWeights[,] weights, double epsilon)
        {
            var d1 = weights.GetLength(0);
            var d2 = weights.GetLength(1);

            for (int i = 0; i < d1; i++)
            {
                for (int j = 0; j < d2; j++)
                {
                    var zx = weights[i, j].Zx / epsilon;
                    var zy = weights[i, j].Zy / epsilon;

                    weights[i, j] = new TipperWeights(zx, zy);
                }
            }
        }
    }
}
