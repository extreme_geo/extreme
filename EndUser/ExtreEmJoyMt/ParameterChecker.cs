﻿using Extreme.Cartesian.Model;
using Extreme.Core;
using Extreme.Inversion;
using Extreme.Inversion.Logger;
using Extreme.Inversion.Observed;
using Extreme.Inversion.Project;

namespace ExtremeEmjoyMt
{
    public class ParameterChecker
    {
        private readonly ILogger _logger;
        private readonly InversionProject _project;

        public ParameterChecker(ILogger logger, InversionProject project)
        {
            _logger = logger;
            _project = project;
        }


        public bool CheckOverallConsistency(CartesianModel startModel, ObservedContainer observed)
        {
            var inversionSettings = _project.InversionSettings;
            var result = true;

            result &= CheckInversionDomain(inversionSettings.Domain, startModel);
            result &= CheckResponseFunctions(inversionSettings.Responses);

            return result;
        }

        private bool CheckResponseFunctions(Responses responses)
        {
            int counter = 0;

            counter += responses.Impedance ? 1 : 0;
            counter += responses.Tipper ? 1 : 0;

            if (counter == 0)
                _logger.WriteError($"No response functions selected for inverstion");

            return counter != 0;
        }

        private bool CheckInversionDomain(InversionDomainParameters domain, CartesianModel model)
        {
            var result = true;

            if (model.Nx % domain.Nx != 0)
            {
                _logger.WriteError($"domain.Nx = {domain.Nx} is incorrect for model with model.Nx = {model.Nx}");
                result = false;
            }

            if (model.Ny % domain.Ny != 0)
            {
                _logger.WriteError($"domain.Ny = {domain.Ny} is incorrect for model with model.Ny = {model.Ny}");
                result = false;
            }

            if (model.Nz % domain.Nz != 0)
            {
                _logger.WriteError($"domain.Nz = {domain.Nz} is incorrect for model with model.Nz = {model.Nz}");
                result = false;
            }

            return result;
        }
    }
}
