﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using Extreme.Cartesian.Core;
using Extreme.Cartesian.Fft;
using Extreme.Cartesian.Forward;
using Extreme.Cartesian.Model;
using Extreme.Core;
using Extreme.Inversion;
using Extreme.Inversion.Logger;
using Extreme.Inversion.Observed;
using Extreme.Inversion.Project;
using Extreme.Model;
using Extreme.Parallel;
using Extreme.Parallel.Logger;
using Porvem.Magnetotellurics;
using VtkExport;

namespace ExtremeEmjoyMt
{
    public class ExtremeJoyMtSolverRunner : IDisposable
    {
        private readonly string _workingPath;
        private readonly InversionProject _project;
        private readonly INativeMemoryProvider _memoryProvider;
        private readonly Mpi _mpi;
        private readonly ILogger _logger;

        private readonly string _startTimePrefix;

        private InversionDomain _inversionDomain;
        private CartesianModel _startModel;

        public ExtremeJoyMtSolverRunner(string projectFileName, INativeMemoryProvider memoryProvider, Mpi mpi)
        {
            if (projectFileName == null) throw new ArgumentNullException(nameof(projectFileName));
            if (memoryProvider == null) throw new ArgumentNullException(nameof(memoryProvider));
            if (mpi == null) throw new ArgumentNullException(nameof(mpi));

            _memoryProvider = memoryProvider;
            _mpi = mpi;
            _workingPath = Path.GetDirectoryName(projectFileName);
            _startTimePrefix = DateTimeToFileName(DateTime.Now);

            _logger = InitLogger(_mpi, _workingPath, _startTimePrefix);

            _project = InversionProjectSerializer.Load(projectFileName);
        }

        public void Run()
        {
            var startModel = LoadStartModel(_project, _mpi);
            var observed = LoadObserved(_workingPath, _project, startModel.LateralDimensions, startModel.Anomaly.LocalSize, _mpi);
            AddNoises(observed, _project.NoiseSettings);

            NormalizeWeights(observed);
            ChangeWeights(observed, _project.NoiseSettings);

            if (_mpi.IsParallel && !_mpi.IsMaster)
                RunSlave(startModel, observed);
            else
                Run(startModel, observed);
        }

        private static ObservedContainer LoadObserved(string workingPath, InversionProject project, LateralDimensions lateral, Size2D localSize, Mpi mpi)
        {
            var observedContainer = new ObservedContainer(lateral, localSize, project.Extreme.ObservationLevels);
            var reader = new ResponsesWithWeightsPlainTextReader(lateral, project.Extreme.ObservationLevels);

            var shift = mpi.CalcLocalHalfNxStart(lateral.Nx);

            foreach (var frequency in project.Extreme.Frequencies)
            {
                var file = GetResponseFile(workingPath, frequency, project);
                var list = reader.Load(file);
                var list2 = CutForLocalSizes(list, localSize, shift);

                observedContainer.AddRange(frequency, list2);
            }


            return observedContainer;
        }

        private static List<ResponsesWithWeightsAtLevel> CutForLocalSizes(List<ResponsesWithWeightsAtLevel> list, Size2D localSize, int shift)
        {
            var result = new List<ResponsesWithWeightsAtLevel>();

            foreach (var rrr in list)
            {
                var resp = new ResponsesWithWeightsAtLevel(rrr.Level, rrr.Lateral);

                if (rrr.ImpedanceTensors != null)
                {
                    resp.ImpedanceTensors = new ImpedanceTensor[localSize.Nx, localSize.Ny];
                    resp.ImpedanceWeights = new TensorWeights[localSize.Nx, localSize.Ny];
                }

                if (rrr.Tippers != null)
                {
                    resp.Tippers = new MagneticTipper[localSize.Nx, localSize.Ny];
                    resp.TipperWeights = new TipperWeights[localSize.Nx, localSize.Ny];
                }

                for (int i = 0; i < localSize.Nx; i++)
                {
                    for (int j = 0; j < localSize.Ny; j++)
                    {
                        if (rrr.ImpedanceTensors != null)
                        {
                            resp.ImpedanceTensors[i, j] = rrr.ImpedanceTensors[i + shift, j];
                            resp.ImpedanceWeights[i, j] = rrr.ImpedanceWeights[i + shift, j];
                        }

                        if (rrr.Tippers != null)
                        {
                            resp.Tippers[i, j] = rrr.Tippers[i + shift, j];
                            resp.TipperWeights[i, j] = rrr.TipperWeights[i + shift, j];
                        }
                    }
                }

                result.Add(resp);
            }

            return result;
        }

        private static string GetResponseFile(string workingPath, double frequency, InversionProject project)
            => Path.Combine(
                workingPath,
                project.InversionSettings.ObservedData.Path,
                ResponsesWithWeightsPlainTextReader.GetInvertionObservedDataFileName(frequency));


        private void NormalizeWeights(ObservedContainer observed)
        {
            var numberOfObserved = CalculateNumberOfObserved(observed);

            foreach (var responses in observed.AllResponses.Values)
            {
                foreach (var resp in responses)
                {
                    if (_project.InversionSettings.Responses.Impedance)
                        ChangeWeights(resp.ImpedanceWeights, resp.ImpedanceTensors, numberOfObserved);

                    if (_project.InversionSettings.Responses.Tipper)
                        ChangeWeights(resp.TipperWeights, resp.Tippers, numberOfObserved);
                }
            }
        }

        private int CalculateNumberOfObserved(ObservedContainer observed)
        {
            var nx = observed.LocalSize.Nx;
            var ny = observed.LocalSize.Ny;

            int counter = 0;

            foreach (var freq in observed.AllResponses.Keys)
            {
                foreach (var resps in observed.AllResponses[freq])
                {
                    if (resps.ImpedanceWeights != null)
                        for (int i = 0; i < nx; i++)
                            for (int j = 0; j < ny; j++)
                            {
                                if (!resps.ImpedanceWeights[i, j].Equals(TensorWeights.EqualZero))
                                    counter++;
                            }

                    if (resps.TipperWeights != null)
                        for (int i = 0; i < nx; i++)
                            for (int j = 0; j < ny; j++)
                            {
                                if (!resps.TipperWeights[i, j].Equals(TipperWeights.EqualZero))
                                    counter++;
                            }
                }
            }

			if (_mpi.IsParallel) {
				counter=_mpi.AllReduce(Mpi.CommWorld, counter);
			}

				return counter;
        }

        private void ChangeWeights(TipperWeights[,] weights, MagneticTipper[,] tipper, int numberOfObserved)
        {
            var d1 = tipper.GetLength(0);
            var d2 = tipper.GetLength(1);

            for (int i = 0; i < d1; i++)
                for (int j = 0; j < d2; j++)
                {
                    var zx = weights[i, j].Zx;
                    var zy = weights[i, j].Zy;

                    weights[i, j] = new TipperWeights(zx / numberOfObserved, zy / numberOfObserved);
                }
        }

        private void ChangeWeights(TensorWeights[,] weights, ImpedanceTensor[,] tensor, int numberOfObserved)
        {
            var d1 = tensor.GetLength(0);
            var d2 = tensor.GetLength(1);

            for (int i = 0; i < d1; i++)
            {
                for (int j = 0; j < d2; j++)
                {
                    var xx = tensor[i, j].Xx.Magnitude;
                    var xy = tensor[i, j].Yx.Magnitude;
                    var yx = tensor[i, j].Yx.Magnitude;
                    var yy = tensor[i, j].Yy.Magnitude;

                    var tmp = (xx * xx + xy * xy + yx * yx * yy) * numberOfObserved;

                    var w = weights[i, j];

                    weights[i, j] = new TensorWeights(w.Xx / tmp, w.Xy / tmp, w.Yx / tmp, w.Yy / tmp);
                }
            }
        }

        private void ChangeWeights(ObservedContainer observed, NoiseSettings settings)
        {
            if (settings.Epsilon == 1)
                return;

            foreach (var responses in observed.AllResponses.Values)
            {
                foreach (var resp in responses)
                {
                    if (resp.ImpedanceWeights != null)
                        Noiser.ChangeWeights(resp.ImpedanceWeights, settings.Epsilon);

                    if (resp.TipperWeights != null)
                        Noiser.ChangeWeights(resp.TipperWeights, settings.Epsilon);
                }
            }
        }

        private void AddNoises(ObservedContainer observed, NoiseSettings settings)
        {
            if (settings.Percent == 0)
                return;

            var noiser = new Noiser(settings.Percent);

            foreach (var responses in observed.AllResponses.Values)
            {
                foreach (var resp in responses)
                {
                    if (resp.ImpedanceTensors != null)
                        noiser.AddRandomNoises(resp.ImpedanceTensors);

                    if (resp.Tippers != null)
                        noiser.AddRandomNoises(resp.Tippers);
                }
            }
        }

        private static ILogger InitLogger(Mpi mpi, string workingPath, string startTimePrefix)
		{
			ILogger logger;

			var logPath = Path.Combine (workingPath, "Logs");
			if (!Directory.Exists (logPath))
				Directory.CreateDirectory (logPath);

			var fullFileLoggerFileName = Path.Combine (logPath, $"full_log_{startTimePrefix }");
			var warningFileLoggerFileName = Path.Combine (logPath, $"inv_log_{startTimePrefix }");

			if (!(mpi == null || mpi.Size == 1)){
                logger = new ParallelConsoleLogger(mpi);
				//logger = new MultiLogger () 
				//{
				//	new ParallelConsoleLogger (mpi),
					//new InversionFileLogger (warningFileLoggerFileName, rewrite: true),
				//};
			}
            else
            {
                logger = new MultiLogger()
                {
                    new InversionConsoleLogger(),
                    new InversionFileLogger(warningFileLoggerFileName, rewrite:true),
                    new FileLogger(fullFileLoggerFileName, rewrite:true),
                };
            }
            return logger;
        }


        private CartesianModel LoadStartModel(InversionProject project, Mpi mpi)
        {
            var initModelPath = GetFullPath(project.Extreme.ModelFile);
            var initialModel = ModelGenUtils.LoadCartesianModel(initModelPath, mpi);

            _logger.WriteStatus($"Initial model: {initModelPath}");

            var maskFile = project.InversionSettings.StartModel.MaskFile;
            var invDomFile = project.InversionSettings.StartModel.InversionDomainFile;


            if (!string.IsNullOrEmpty(maskFile) &&
                !string.IsNullOrEmpty(invDomFile))
            {
                if (_mpi.IsParallel)
                    throw new NotImplementedException("Mask is not supported in parallel mode");

                var fullMaskFile = GetFullPath(maskFile);
                var fullInvDomFile = GetFullPath(invDomFile);

                _logger.WriteStatus($"Sigma from: {invDomFile}");
                _logger.WriteStatus($"Using mask: {maskFile}");

                var mask = InversionDomainWriter.LoadInversionDomainMask(fullMaskFile);
                var invDom = InversionDomainWriter.LoadInversionDomain(fullInvDomFile);

                UpdateModel(initialModel, mask, invDom);
            }

            return initialModel;
        }

        private static void UpdateModel(CartesianModel initialModel, int[,,] mask, double[] invDom)
        {
            for (int i = 0; i < initialModel.Nx; i++)
                for (int j = 0; j < initialModel.Ny; j++)
                    for (int k = 0; k < initialModel.Nz; k++)
                    {
                        var index = mask[i, j, k];
                        initialModel.Anomaly.Sigma[i, j, k] = invDom[index];
                    }
        }

        private string GetFullPath(string path)
            => Path.Combine(_workingPath, path);


        private static string DateTimeToFileName(DateTime dt)
        {
            return dt.ToString(CultureInfo.InvariantCulture)
                .Replace(@"/", "-")
                .Replace(@":", "-")
                .Replace(@" ", "_");
        }

        private void RunSlave(CartesianModel startModel, ObservedContainer observedContainer)
        {
            using (var solver = new MtInversionSolver(_logger, _memoryProvider, _project, _mpi))
            {
                FftBuffersPool.PrepareBuffersForModel(startModel, _memoryProvider, _mpi);
                solver.SolveSlave(startModel, observedContainer);
            }
        }


        private void Run(CartesianModel startModel, ObservedContainer observedContainer)
        {
            if (!CheckOverallConsistency(startModel, observedContainer))
                return;

            _startModel = startModel;

            LogSettingsInfo();

            var resultsPath = Path.Combine(_workingPath, _project.Extreme.ResultsPath);
            if (!Directory.Exists(resultsPath))
                Directory.CreateDirectory(resultsPath);

            using (var solver = new MtInversionSolver(_logger, _memoryProvider, _project, _mpi))
            {
                solver.InversionDomainCreated += Solver_InversionDomainCreated;
                solver.InversionIterationComplete += Solver_InversionIterationComplete;


                FftBuffersPool.PrepareBuffersForModel(startModel, _memoryProvider, _mpi);
                solver.Solve(startModel, observedContainer);
            }
        }

        private bool CheckOverallConsistency(CartesianModel model, ObservedContainer observed)
        {
            var checker = new ParameterChecker(_logger, _project);

            if (!checker.CheckOverallConsistency(model, observed))
            {
                _logger.WriteError("Input data is not correct, abort Inversion");
                return false;
            }

            return true;
        }

        private void LogSettingsInfo()
        {
            var stab = _project.InversionSettings.Stabilizer;
            var lbfgs = _project.InversionSettings.Lbfgs;
            var noise = _project.NoiseSettings;
            var resps = _project.InversionSettings.Responses;

            _logger.WriteStatus($"Stabilizer, lambda:{stab.Lambda}, alpha:{stab.Alpha}, beta:{stab.Beta}");

            _logger.WriteStatus($"Lbfgs," +
                                $"\n\tMemory:{lbfgs.Memory}, " +
                                $"\n\tTolerance:{lbfgs.Tolerance}" +
                                $"\n\tLine search length:{lbfgs.LineSearchLength}" +
                                $"\n\tLine search type:{lbfgs.LineSearchType}");


            _logger.WriteStatus($"Noise, percent: {noise.Percent}, epsilon: {noise.Epsilon}");

            _logger.WriteStatus($"Number of threads: {MultiThreadUtils.MaxDegreeOfParallelism}");
            _logger.WriteStatus($"Number of hankels: {_project.ForwardSettings.NumberOfHankels}");
            _logger.WriteStatus($"Target forward residual  : {_project.ForwardSettings.Residual}");


            _logger.WriteStatus($"Impedance: {resps.Impedance}");
            _logger.WriteStatus($"Tipper: {resps.Tipper}");
        }

        private unsafe void Solver_InversionIterationComplete(object sender, InversionIterationCompleteEventArgs e)
        {
            var fileName = Path.Combine(_workingPath, _project.Extreme.ResultsPath, $"inv_dom_{e.Progress.IterationCount:00000}_{_startTimePrefix}.bin");
            var vtkFileName = Path.Combine(_workingPath, _project.Extreme.ResultsPath, $"fwd_dom_{e.Progress.IterationCount:00000}_{_startTimePrefix}.vtr");
            InversionDomainWriter.SaveInversionDomain(fileName, e.Progress.NumberOfVariables, e.Progress.X);

            var sigma = RiseFromAshes2(e.Progress.X);

            var writer = new XmlVtkWriter();
            var xdoc = writer.Write(_startModel, sigma);

            xdoc.Save(vtkFileName);
        }

        private void Solver_InversionDomainCreated(object sender, InversionDomainCreatedEventArgs e)
        {
            var maskFileName = Path.Combine(_workingPath, _project.Extreme.ResultsPath, $"mask_{_startTimePrefix}.bin");
            _inversionDomain = e.InversionDomain;
            InversionDomainWriter.SaveInversionDomainMask(maskFileName, e.InversionDomain);
        }

        private unsafe double[,,] RiseFromAshes2(double* d)
        {
            var nx = _inversionDomain.Mask.GetLength(0);
            var ny = _inversionDomain.Mask.GetLength(1);
            var nz = _inversionDomain.Mask.GetLength(2);

            var sigmas = new double[nx, ny, nz];

            for (int i = 0; i < nx; i++)
                for (int j = 0; j < ny; j++)
                    for (int k = 0; k < nz; k++)
                    {
                        var index = _inversionDomain.Mask[i, j, k];
                        sigmas[i, j, k] = Math.Exp(d[index]);
                    }

            return sigmas;
        }

        public void Dispose()
        {
        }
    }
}
