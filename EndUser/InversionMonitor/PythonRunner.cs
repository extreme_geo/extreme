﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InversionMonitor
{
    public class PythonRunner
    {

        private void run_cmd(string cmd, string args)
        {
            var start = new ProcessStartInfo();
            start.FileName = "my/full/path/to/python.exe";
            start.Arguments = $"{cmd} {args}";
            start.UseShellExecute = false;
            start.RedirectStandardOutput = true;
            using (var process = Process.Start(start))
            {
                using (var reader = process.StandardOutput)
                {
                    string result = reader.ReadToEnd();
                    Console.Write(result);
                }
            }
        }
    }
}
