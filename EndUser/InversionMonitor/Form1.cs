﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InversionMonitor
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
           SelectFolder();
        }
        private void openDirectoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SelectFolder();
        }


        private void SelectFolder()
        {
            using (var fbd = new FolderBrowserDialog())
            {
                if (Directory.Exists(Properties.Settings.Default.Path))
                    fbd.SelectedPath = Properties.Settings.Default.Path;
                else
                    fbd.SelectedPath = @"Z:\ETH\Inversion";

                
                if (fbd.ShowDialog() == DialogResult.OK)
                {
                    Properties.Settings.Default.Path = fbd.SelectedPath;
                    Properties.Settings.Default.Save();
                    SetNewPath(fbd.SelectedPath);
                }
            }
        }

        private void SetNewPath(string path)
        {
            label1.Text = path;

            ScanDirectory(path);
        }

        private void ScanDirectory(string path)
        {
            var files = Directory.GetFiles(path, "fwd_dom*.vtr");

            files = files.Select(Path.GetFileName).ToArray();


            //var dates = files.Select(f => f.Substring(14, 20)).Distinct().ToArray();



            listBox1.Items.AddRange(files);



        }

    }
}
