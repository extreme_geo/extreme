﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Extreme.Cartesian.Forward;
using Extreme.Cartesian.Magnetotellurics;
using Extreme.Core;
using Extreme.Inversion;
using Extreme.Inversion.Observed;
using Extreme.Inversion.Project;
using Extreme.Model;
using Porvem.Magnetotellurics;

namespace ForwardToInversion
{
    public class ForwadToInverseConverter
    {
        private readonly string _workingPath;
        private readonly ForwardProject _forwardProject;
        private readonly InversionProject _inversionProject;
        private readonly decimal _xShift;
        private readonly decimal _yShift;

        public ForwadToInverseConverter(string workingPath,
            ForwardProject forwardProject, InversionProject inversionProject,
            decimal xShift, decimal yShift)
        {
            _workingPath = workingPath;
            _forwardProject = forwardProject;
            _inversionProject = inversionProject;
            _xShift = xShift;
            _yShift = yShift;
        }

        public ObservedContainer Convert()
        {
            var fwdModel = ModelGenUtils.LoadCartesianModelWithoutAnomalyData(Path.Combine(_workingPath, _forwardProject.Extreme.ModelFile));
            var invModel = ModelGenUtils.LoadCartesianModelWithoutAnomalyData(Path.Combine(_workingPath, _inversionProject.Extreme.ModelFile));

            var fwdResponseFiles = GetResponseFiles(_workingPath, _forwardProject);

            var reader = new ObservedPlainTextReader(fwdModel.LateralDimensions, _forwardProject.ObservationLevels);
            var observed = new Dictionary<double, List<AllResponsesAtLevel>>();

            foreach (var freq in fwdResponseFiles.Keys)
                observed.Add(freq, reader.Load(fwdResponseFiles[freq]));

            var oc = new ObservedContainer(invModel.LateralDimensions, _inversionProject.Extreme.ObservationLevels);

            foreach (var frequency in _inversionProject.Extreme.Frequencies)
            {
                Convert(oc, frequency, observed);
            }

            return oc;
        }

        private void Convert(ObservedContainer oc, double frequency, Dictionary<double, List<AllResponsesAtLevel>> observed)
        {
            var obs = observed.ContainsKey(frequency) ? observed[frequency] : null;

            foreach (var level in oc.Levels)
            {
                var rww = CreateResponsesWithWeights(level, oc.Lateral);
                var zCorrLevels = obs?.FindAll(l => l.Level.Z == level.Z);

                if (zCorrLevels != null && zCorrLevels.Count != 0)
                {
                    for (int i = 0; i < rww.Lateral.Nx; i++)
                    {
                        for (int j = 0; j < rww.Lateral.Ny; j++)
                        {
                            var x = rww.Level.ShiftAlongX + rww.Lateral.CellSizeX * (i + 0.5m) + _xShift;
                            var y = rww.Level.ShiftAlongY + rww.Lateral.CellSizeY * (j + 0.5m) + _yShift;

                            var indecies = FindCorrespondingIndecies(x, y, zCorrLevels);
                            if (indecies != null)
                                FillResponsesWithWeights(rww, i, j, indecies.Item1, indecies.Item2, indecies.Item3);
                            else
                                FillResponsesWithWeightsWithZero(rww, i, j);
                        }
                    }
                }

                oc.Add(frequency, rww);
            }
        }

        private void FillResponsesWithWeightsWithZero(ResponsesWithWeightsAtLevel rww, int dstI, int dstJ)
        {
            var resps = _inversionProject.InversionSettings.Responses;

            if (resps.Impedance)
            {
                rww.ImpedanceTensors[dstI, dstJ] = ImpedanceTensor.EqualOne;
                rww.ImpedanceWeights[dstI, dstJ] = TensorWeights.EqualZero;
            }

            if (resps.Tipper)
            {
                rww.Tippers[dstI, dstJ] = MagneticTipper.EqualOne;
                rww.TipperWeights[dstI, dstJ] = TipperWeights.EqualZero;
            }
        }

        private void FillResponsesWithWeights(ResponsesWithWeightsAtLevel rww, int dstI, int dstJ, int srcI, int srcJ, AllResponsesAtLevel responses)
        {
            var resps = _inversionProject.InversionSettings.Responses;

            if (resps.Impedance)
            {
                rww.ImpedanceTensors[dstI, dstJ] = responses.ImpedanceTensors[srcI, srcJ];
                rww.ImpedanceWeights[dstI, dstJ] = TensorWeights.EqualOne;
            }

            if (resps.Tipper)
            {
                rww.Tippers[dstI, dstJ] = responses.Tippers[srcI, srcJ];
                rww.TipperWeights[dstI, dstJ] = TipperWeights.EqualOne;
            }
        }

        private Tuple<int, int, AllResponsesAtLevel> FindCorrespondingIndecies(decimal x, decimal y, List<AllResponsesAtLevel> zCorrLevels)
        {
            foreach (var allResponsesAtLevel in zCorrLevels)
            {
                var lev = allResponsesAtLevel.Level;
                var lat = allResponsesAtLevel.Lateral;

                var xIndexDec = (x - lev.ShiftAlongX - lat.CellSizeX / 2) / lat.CellSizeX;
                var yIndexDec = (y - lev.ShiftAlongY - lat.CellSizeY / 2) / lat.CellSizeY;

                var xIndex = (int)xIndexDec;
                var yIndex = (int)yIndexDec;

                if (xIndex == xIndexDec && yIndex == yIndexDec)
                {
                    if (xIndex < lat.Nx &&
                        yIndex < lat.Ny &&
                        xIndex >= 0 &&
                        yIndex >= 0)
                    {

                        return new Tuple<int, int, AllResponsesAtLevel>(xIndex, yIndex, allResponsesAtLevel);
                    }
                }
            }

            return null;
        }

        private ResponsesWithWeightsAtLevel CreateResponsesWithWeights(ObservationLevel level, LateralDimensions lateral)
        {
            var rwwal = new ResponsesWithWeightsAtLevel(level, lateral);
            var resps = _inversionProject.InversionSettings.Responses;

            if (resps.Impedance)
            {
                rwwal.ImpedanceTensors = new ImpedanceTensor[lateral.Nx, lateral.Ny];
                rwwal.ImpedanceWeights = new TensorWeights[lateral.Nx, lateral.Ny];
            }

            if (resps.Tipper)
            {
                rwwal.Tippers = new MagneticTipper[lateral.Nx, lateral.Ny];
                rwwal.TipperWeights = new TipperWeights[lateral.Nx, lateral.Ny];
            }

            return rwwal;
        }


        private static Dictionary<double, string> GetResponseFiles(string workingPath, ForwardProject project)
        {
            return project.Extreme.Frequencies
                .ToDictionary(f => f,
                f => Path.Combine(workingPath, project.Extreme.ResultsPath, PlainTextExporter.GetResponsesFileName(f)));
        }
    }
}
