﻿using System;
using System.Collections.Generic;
using System.IO;
using Extreme.Cartesian.Forward;
using Extreme.Cartesian.Magnetotellurics;
using Extreme.Inversion;
using Extreme.Inversion.Observed;
using Extreme.Inversion.Project;

namespace ForwardToInversion
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length < 4)
            {
                RunDemoMode();
            }
            else
            {
                var maskFiles = new List<string>();

                var xShift = decimal.Parse(args[2]);
                var yShift = decimal.Parse(args[3]);

                for (int i = 4; i < args.Length; i++)
                    maskFiles.Add(args[i]);

                ConvertForwardToInverse(args[0], args[1], xShift, yShift, maskFiles.ToArray());
            }
        }

        private static void RunDemoMode()
        {
            Console.WriteLine(@"Forward to Inversion converter, please provide: fwd.xproj inv.xproj xShift yShift [mask1.dat mask2.dat...]");
        }

        private static void ConvertForwardToInverse(
            string fwdProjectFile, string invProjectFile, 
            decimal xShift, decimal yShift,
            params string[] maskFiles)
        {
            var path = "";//Path.GetDirectoryName(invProjectFile);

            var fwd = ForwardProjectSerializer.Load(fwdProjectFile);
            var inv = InversionProjectSerializer.Load(invProjectFile);

            var converter = new ForwadToInverseConverter(path, fwd, inv, xShift, yShift);
            var oc = converter.Convert();

            ApplyMasksIfApplicable(oc, maskFiles);

            var dirName = Path.Combine(path, inv.InversionSettings.ObservedData.Path);
            if (!Directory.Exists(dirName))
                Directory.CreateDirectory(dirName);

            foreach (var frequency in oc.AllResponses.Keys)
            {
                var fileName = Path.Combine(dirName, ResponsesWithWeightsPlainTextReader
                    .GetInvertionObservedDataFileName(frequency));

                var resps = oc.AllResponses[frequency];
                Console.WriteLine($"file created: {fileName}");
                ObservedExporter.SaveInversionInputFile(fileName, resps, frequency);
            }
        }
        
        private static void ApplyMasksIfApplicable(ObservedContainer oc, string[] maskFiles)
        {
            if (maskFiles.Length == 0)
                return;
            
            var masks = ReadAllMasks(oc, maskFiles);

            foreach (var resps in oc.AllResponses.Values)
            {
                ApplyMask(resps, masks);
            }
        }

        private static bool[][,] ReadAllMasks(ObservedContainer oc, string[] maskFiles)
        {
            try
            {
                var masks = new bool[maskFiles.Length][,];

                for (int i = 0; i < maskFiles.Length; i++)
                    masks[i] = ReadWeightMask(maskFiles[i], oc.Lateral.Nx, oc.Lateral.Ny);

                return masks;
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Can't load masks. \n" +
                                  $"{ex.Message}\n" +
                                  $"{ex.StackTrace}");
                throw;
            }

        }

        private static void ApplyMask(List<ResponsesWithWeightsAtLevel> resps, bool[][,] masks)
        {
            if (masks.Length != 1)
                if (masks.Length != resps.Count)
                    throw new ArgumentException(@"Number of masks should be either 1 or equal to the number of observation levels");

            if (masks.Length == 1)
                foreach (var resp in resps)
                    ApplyMask(resp, masks[0]);
            else
                for (int i = 0; i < resps.Count; i++)
                    ApplyMask(resps[i], masks[i]);
        }

        private static void ApplyMask(ResponsesWithWeightsAtLevel resp, bool[,] mask)
        {
            if (resp.ImpedanceWeights != null)
                for (int i = 0; i < mask.GetLength(0); i++)
                    for (int j = 0; j < mask.GetLength(1); j++)
                        resp.ImpedanceWeights[i, j] = mask[i, j]
                            ? resp.ImpedanceWeights[i, j]
                            : TensorWeights.EqualZero;

            if (resp.TipperWeights != null)
                for (int i = 0; i < mask.GetLength(0); i++)
                    for (int j = 0; j < mask.GetLength(1); j++)
                        resp.TipperWeights[i, j] = mask[i, j]
                            ? resp.TipperWeights[i, j]
                            : TipperWeights.EqualZero;
        }

        private static bool[,] ReadWeightMask(string fileName, int nx, int ny)
        {
            using (var sr = new StreamReader(fileName))
                return ReadWeightMask(sr, nx, ny);
        }

        private static bool[,] ReadWeightMask(StreamReader sr, int nx, int ny)
        {
            var result = new bool[nx, ny];

            for (int i = 0; i < nx; i++)
            {
                var line = sr.ReadLine();

                for (int j = 0; j < ny; j++)
                    result[i, j] = line[j] != '0';
            }

            return result;
        }


    }
}
