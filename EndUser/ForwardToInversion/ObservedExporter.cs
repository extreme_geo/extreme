﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Numerics;
using Extreme.Inversion.Observed;
using Porvem.Magnetotellurics;

namespace ForwardToInversion
{
    public class ObservedExporter
    {

        public static void ExportMask(string fileName, ResponsesWithWeightsAtLevel rww)
        {
            using (var sw = new StreamWriter(fileName))
            {
                var lat = rww.Lateral;

                for (int i = 0; i < lat.Nx; i++)
                {
                    for (int j = 0; j < lat.Ny; j++)
                    {
                        bool x = rww.ImpedanceTensors[i, j] != null;

                        sw.Write(x ? "1" : "0");
                    }
                    sw.WriteLine();
                }
            }
        }


        public static void SaveInversionInputFile(string fileName, List<ResponsesWithWeightsAtLevel> rwws, double frequency)
        {
            using (var sw = new StreamWriter(fileName))
            {
                PrintHeader(sw, rwws);
                SaveInversionInputFile(sw, rwws, frequency);
            }
        }

        private static void PrintHeader(StreamWriter sw, List<ResponsesWithWeightsAtLevel> rwws)
        {
            Action<string, int> writeGen = (text, pad) => sw.Write($"{text.PadLeft(pad, ' ')} ");
            Action<string> write = t => writeGen(t, 14);

            writeGen("%", 0);
            writeGen("x", 8);
            writeGen("y", 10);
            writeGen("z", 10);
            write("freq");

            if (rwws.First().ImpedanceTensors != null)
            {
                write("Z_XX_re");
                write("Z_XX_im");
                write("Z_XX_w");

                write("Z_XY_re");
                write("Z_XY_im");
                write("Z_XY_w");

                write("Z_YX_re");
                write("Z_YX_im");
                write("Z_YX_w");

                write("Z_YY_re");
                write("Z_YY_im");
                write("Z_YY_w");
            }

            if (rwws.First().Tippers != null)
            {
                write("W_ZX_re");
                write("W_ZX_im");
                write("W_ZX_w");

                write("W_ZY_re");
                write("W_ZY_im");
                write("W_ZY_w");
            }

            sw.WriteLine();
        }

        private static void SaveInversionInputFile(StreamWriter sw, List<ResponsesWithWeightsAtLevel> rwws, double frequency)
        {
            foreach (var rww in rwws)
            {
                var lat = rww.Lateral;

                for (int i = 0; i < lat.Nx; i++)
                    for (int j = 0; j < lat.Ny; j++)
                    {
                        var x = rww.Level.ShiftAlongX + lat.CellSizeX * (i + 0.5m);
                        var y = rww.Level.ShiftAlongY + lat.CellSizeY * (j + 0.5m);
                        var z = rww.Level.Z;

                        WriteFreqAndCoord(sw, x, y, z, frequency);

                        if (rww.ImpedanceTensors != null)
                            WriteImpedance(sw, rww.ImpedanceTensors[i, j], rww.ImpedanceWeights[i, j]);

                        if (rww.Tippers != null)
                            WriteTippers(sw, rww.Tippers[i, j], rww.TipperWeights[i, j]);

                        sw.WriteLine();
                    }
            }
        }

        private static void WriteFreqAndCoord(StreamWriter sw, decimal x, decimal y, decimal z, double frequency)
        {
            var xStr = x.ToString("F2").PadLeft(10, ' ');
            var yStr = y.ToString("F2").PadLeft(10, ' ');
            var zStr = z.ToString("F2").PadLeft(10, ' ');

            sw.Write("{0} {1} {2} ", xStr, yStr, zStr);
            sw.Write("{0} ", frequency.ToString("E").PadLeft(14, ' '));
        }

        private static void WriteTippers(StreamWriter sw, MagneticTipper tipper, TipperWeights weight)
        {
            Action<Complex> writeV = (c)
                              => sw.Write($"{c.Real.ToString("E").PadLeft(14, ' ')} " +
                                          $"{c.Imaginary.ToString("E").PadLeft(14, ' ')} ");

            Action<double> writeW = (c)
               => sw.Write($"{c.ToString("E").PadLeft(14, ' ')} ");

            writeV(tipper.Zx);
            writeW(weight.Zx);

            writeV(tipper.Zy);
            writeW(weight.Zy);
        }

        private static void WriteImpedance(StreamWriter sw, ImpedanceTensor impedance, TensorWeights weights)
        {
            Action<Complex> writeV = (c)
                   => sw.Write($"{c.Real.ToString("E").PadLeft(14, ' ')} " +
                               $"{c.Imaginary.ToString("E").PadLeft(14, ' ')} ");

            Action<double> writeW = (c)
               => sw.Write($"{c.ToString("E").PadLeft(14, ' ')} ");

            writeV(impedance.Xx);
            writeW(weights.Xx);

            writeV(impedance.Xy);
            writeW(weights.Xy);

            writeV(impedance.Yx);
            writeW(weights.Yx);

            writeV(impedance.Yy);
            writeW(weights.Yy);
        }
    }
}
