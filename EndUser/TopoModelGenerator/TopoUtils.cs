﻿using System;
using System.IO;
using Extreme.Model;
using Extreme.Model.Topography;
using Extreme.Cartesian.Model;
using Extreme.Core;
using Extreme.Core.Logger;

namespace TopoModelGenerator
{
    public class TopoUtils
    {
        private readonly string _topographyDataPath;

        public TopoUtils(ILogger logger, string topographyDataPath)
        {
            Logger = logger;
            _topographyDataPath = topographyDataPath;
        }
        private ILogger Logger { get; }

        public CartesianModel CreateModel(int nx, int ny, int nz)
        {
            var mesh = new MeshParameters(nx, ny, nz)
            {
                UseGeometricStepAlongZ = false,
            };

            var shiftXInKm = -1000;
            var shiftYInKm = -500;

            var totalXInKm = 1500m;
            var totalYInKm = 1500m;

            var shift = new Point(shiftXInKm * 1000.0, shiftYInKm * 1000);
            var totalXSize = totalXInKm * 1000m;
            var totalYSize = totalYInKm * 1000m;

            Logger.WriteStatus("preLoad topography data");


            using (var topography = DiscretePhilippineTopographyProvider.CreateProviderWithPreloadedData(_topographyDataPath))
            {
                Logger.WriteStatus("Looking or zMin and zMax");

                var minZ = topography.GetMinZ();
                var maxZ = topography.GetMaxZ();

                Logger.WriteStatus($"\tzMin = {minZ}");
                Logger.WriteStatus($"\tzMax = {maxZ}");

                Logger.WriteStatus("Creating creator");
                var creater = new TopographyModelCreater(Logger, topography, shift, 0, maxZ);

                Logger.WriteStatus("Creating model");
                var model = creater.CreateModel(mesh, totalXSize / mesh.Nx, totalYSize / mesh.Ny);

                return model;

            }
        }
    }
}
