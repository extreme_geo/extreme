﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoPlot
{
    public class ScriptContainer
    {
        public const string ScriptsDir = "Scripts";
        public const string ImagesDir = "Images";

        private readonly string _basePath;
        private readonly double _frequency;

        private readonly string _name;

        private readonly string _eXtremeSourcePath;
        private readonly string _grayverSourcePath;

        public ScriptContainer(string basePath, string dirName, double frequency)
        {
            _basePath = basePath;
            _frequency = frequency;

            _name = dirName.Replace("model_", "");
            
            // always in Linux style
            _eXtremeSourcePath = String.Format("../{0}/result/selection.dat", dirName);
            _grayverSourcePath = String.Format("../Grayver/grayver_selected_{0}.dat", frequency.ToString("0.00000000"));
        }

        public string GenerateForComponent(string title, int columnIndex, bool logScaleY)
        {
            var fullName = String.Format("{1}_{0}Hz_{2}", _frequency.ToString("0.0000"), _name, title.Replace(" ", "_"));
            Console.WriteLine(fullName);

            var imagePath = Path.Combine(ImagesDir, fullName + ".png");
            var scriptPath = Path.Combine(_basePath, ScriptsDir, fullName + ".plt");

            var script = new GnuplotScript(_eXtremeSourcePath, _grayverSourcePath)
            {
                Freq = _frequency,
                Title = title,
                ColumnIndex = columnIndex,
                Output = imagePath,
                LogscaleY = logScaleY,
            };

            File.WriteAllText(scriptPath, script.TransformText());

            return scriptPath;
        }

        public string GenerateWithKryglykovForComponent(string kruglyakovName, string title, int columnIndex, bool logScaleY)
        {
            var fullName = String.Format("mk200_{1}_{0}Hz_{2}", _frequency.ToString("0.0000"), _name, title.Replace(" ", "_"));
            Console.WriteLine(fullName);

            var imagePath = Path.Combine(ImagesDir, fullName + ".png");
            var scriptPath = Path.Combine(_basePath, ScriptsDir, fullName + ".plt");

            var kruglykovSourcePath = String.Format("../kruglyakov/{0}", kruglyakovName);

            var script = new GnuplotScriptWithKruglyakov(_eXtremeSourcePath, _grayverSourcePath, kruglykovSourcePath)
            {
                Freq = _frequency,
                Title = title,
                ColumnIndex = columnIndex,
                Output = imagePath,
                LogscaleY = logScaleY,
            };

            File.WriteAllText(scriptPath, script.TransformText());

            return scriptPath;
        }
    }
}
