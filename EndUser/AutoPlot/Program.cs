﻿using System;
using System.Collections.Generic;
using System.IO;

namespace AutoPlot
{
    public static class Program
    {
        public const string BasePath = @"Z:\PorvemWorking\ModelForPaper_2015_04_23_geomZ_2\residual_E_10";
        //public const string BasePath = @"Z:\PorvemWorking\ModelForPaper_2015_04_23\residual_E_10";
        //public const string BasePath = @"Z:\PorvemWorking\ModelForPaper_2015_04_21\residual_E_10";

        static void Main(string[] args)
        {
            var str = @"Z:\PorvemWorking\ModelForPaper_2015\2015_07_08\giem2g\KruglyakovResults\tr_func_F00001.00000C_3nx1024_ny256_nz200_geomlin.dat";
            AutoPlotGenerator.ConvertOneKruglyakovResult(str, 20078.125m);
            
            
            //var str = @"Z:\PorvemWorking\ModelForPaper_2015\2015_07_08";
            //AutoPlotGenerator.ConvertAllGrayverResults(str);

            //var str = @"Z:\PorvemWorking\ModelForPaper_2015\2015_07_08\contrast10\fields_freq1.xml";
            //AutoPlotGenerator.ConvertAllExtremeMtResultsForFile(str, 20039.0625m);
            //AutoPlotGenerator.ConvertAllExtremeMtResultsForFile(str, 19960.9375m);
            
            //ClearAll();
            //ConvertAndCopyGrayver();
            //ConvertKruglyakov();

            

            //ConvertExtremeMt();
            //GenerateAll();
            //GenerateWithKruglyakov();
        }

        private static void ConvertExtremeMt()
        {
            //AutoPlotGenerator.ConvertAllExtremeMtResults(BasePath, "nx50ny50", 20400);
            //AutoPlotGenerator.ConvertAllExtremeMtResults(BasePath, "nx100ny100", 20200);
        }

        private static void ConvertAndCopyGrayver()
        {
            AutoPlotGenerator.ConvertAllGrayverResults(BasePath);
            AutoPlotGenerator.CopyAllGrayverResults(BasePath);
        }

        private static void ConvertKruglyakov()
        {
            AutoPlotGenerator.ConvertAllKruglykovResults(BasePath, 20400);
        }

        private static void GenerateAll()
        {
            var allScripts = AutoPlotGenerator.GenerateAllScripts(BasePath);
            AutoPlotGenerator.RunAllScripts(allScripts);

            var dstPath = Path.Combine(BasePath, "TablesAndPictures", "AllImages");

            if (!Directory.Exists(dstPath))
                Directory.CreateDirectory(dstPath);

            AutoPlotGenerator.CopyAllImagesToSingleDirectory(BasePath, dstPath);
        }


        private static void GenerateWithKruglyakov()
        {
            var allScripts = new List<string>();

            //allScripts.AddRange(AutoPlotGenerator.GenerateScriptsWithKruglykov(BasePath, "nx50ny50", "contrast1", "selected_tr_func_giem2g_c1_nz80.dat"));
            allScripts.AddRange(AutoPlotGenerator.GenerateScriptsWithKruglykov(BasePath, "nx50ny50", "contrast10", "selected_tr_func_giem2g_c3_nz200.dat"));
            allScripts.AddRange(AutoPlotGenerator.GenerateScriptsWithKruglykov(BasePath, "nx100ny100", "contrast10", "selected_tr_func_giem2g_c3_nz200.dat"));
            //allScripts.AddRange(AutoPlotGenerator.GenerateScriptsWithKruglykov(BasePath, "nx100ny100", "contrast1", "selected_tr_func_giem2g_c1_nz80.dat"));

            AutoPlotGenerator.RunAllScripts(allScripts);
        }


        private static void ClearAll()
        {
            var finalPathImg = Path.Combine(BasePath, "TablesAndPictures", "AllImages");

            if (!Directory.Exists(finalPathImg))
                Directory.CreateDirectory(finalPathImg);

            var files = Directory.GetFiles(finalPathImg, "*.png", SearchOption.TopDirectoryOnly);
            foreach (var file in files)
                File.Delete(file);

            AutoPlotGenerator.ClearAllImages(BasePath);
            AutoPlotGenerator.ClearAllScripts(BasePath);
        }
    }
}
