﻿namespace AutoPlot
{
    partial class GnuplotScriptWithKruglyakov
    {
        private readonly string _path;
        private readonly string _grPath;
        private readonly string _krPath;

        private bool _logscaleY;

        private string _output;
        private string _title;
        private double _freq;
        private double _columnIndex;

        public GnuplotScriptWithKruglyakov(string path, string grPath, string krPath)
        {
            _path = path;
            _grPath = grPath;
            _krPath = krPath;
        }

        public double Freq
        {
            get { return _freq; }
            set { _freq = value; }
        }

        public string Title
        {
            get { return _title; }
            set { _title = value; }
        }

        public double ColumnIndex
        {
            get { return _columnIndex; }
            set { _columnIndex = value; }
        }

        public string Output
        {
            get { return _output; }
            set { _output = value; }
        }

        public bool LogscaleY
        {
            get { return _logscaleY; }
            set { _logscaleY = value; }
        }
    }
}
