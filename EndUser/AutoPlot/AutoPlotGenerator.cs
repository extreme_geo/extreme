﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Threading.Tasks;
using Extreme.Cartesian.Magnetotellurics;

namespace AutoPlot
{
    public class AutoPlotGenerator
    {
        #region Scripts And Images

        public static void RunAllScripts(List<string> scriptFiles)
        {
            Parallel.ForEach(scriptFiles, RunScript);
        }

        private static void RunScript(string scriptFile)
        {
            Console.WriteLine(scriptFile);

            var psi = new ProcessStartInfo()
            {
                CreateNoWindow = true,
                FileName = "gnuplot.exe",
                Arguments = scriptFile,
                WorkingDirectory = Path.GetDirectoryName(scriptFile)
            };

            Process.Start(psi);
        }

        public static List<string> GenerateAllScripts(string basePath)
        {
            var laterals = new[] { "nx50ny50", "nx100ny100" };
            var contrasts = new[] { "contrast1", "contrast3", "contrast10" };
            //var contrasts = new[] { "contrast10" };

            var scripts = new List<string>();

            foreach (var lateral in laterals)
                foreach (var contrast in contrasts)
                {
                    var path = Path.Combine(basePath, lateral, contrast);

                    if (!Directory.Exists(path))
                        continue;

                    scripts.AddRange(AutoPlotGenerator.Generate(path));
                }

            return scripts;
        }


        public static List<string> GenerateScriptsWithKruglykov(string basePath, string lateral, string contrast, string name)
        {
            var scripts = new List<string>();

            var path = Path.Combine(basePath, lateral, contrast);

            scripts.AddRange(AutoPlotGenerator.GenerateWithKruglykov(path, name));

            return scripts;
        }

        public static void CopyAllImagesToSingleDirectory(string basePath, string dstPath)
        {
            var laterals = new[] { "nx100ny100", "nx200ny200", "nx50ny50" };

            foreach (var lateral in laterals)
            {
                var path = Path.Combine(basePath, lateral);

                if (!Directory.Exists(path))
                    continue;

                var files = Directory.GetFiles(path, "*.png", SearchOption.AllDirectories);

                foreach (var file in files)
                    File.Copy(file, Path.Combine(dstPath, Path.GetFileName(file)));
            }
        }

        public static void ClearAllImages(string basePath)
        {
            var laterals = new[] { "nx100ny100", "nx200ny200", "nx50ny50" };

            foreach (var lateral in laterals)
            {
                var path = Path.Combine(basePath, lateral);

                if (!Directory.Exists(path))
                    continue;

                var files = Directory.GetFiles(path, "*.png", SearchOption.AllDirectories);

                foreach (var file in files)
                    File.Delete(file);
            }
        }
        public static void ClearAllScripts(string basePath)
        {
            var laterals = new[] { "nx100ny100", "nx200ny200", "nx50ny50" };
            var contrasts = new[] { "contrast1", "contrast3", "contrast10" };

            foreach (var lateral in laterals)
                foreach (var contrast in contrasts)
                {
                    var path = Path.Combine(basePath, lateral, contrast);

                    if (!Directory.Exists(path))
                        continue;

                    var files = Directory.GetFiles(path, "*.plt", SearchOption.AllDirectories);

                    foreach (var file in files)
                        File.Delete(file);
                }
        }

        private static List<string> Generate(string basePath)
        {
            CheckAllDirs(basePath);

            var directories = Directory.GetDirectories(basePath, "model_*");

            var scripts = new List<string>();

            foreach (var dir in directories)
            {
                Console.WriteLine(dir);

                var dirName = new DirectoryInfo(dir).Name;

                scripts.AddRange(GenerateForFrequency(1, basePath, dirName));
                scripts.AddRange(GenerateForFrequency(0.1, basePath, dirName));
                scripts.AddRange(GenerateForFrequency(0.01, basePath, dirName));
                scripts.AddRange(GenerateForFrequency(0.001, basePath, dirName));
            }

            return scripts;
        }

        private static List<string> GenerateWithKruglykov(string basePath, string kruglykovName)
        {
            CheckAllDirs(basePath);

            var directories = Directory.GetDirectories(basePath, "model_*");

            var scripts = new List<string>();

            foreach (var dir in directories)
            {
                Console.WriteLine(dir);

                var dirName = new DirectoryInfo(dir).Name;

                scripts.AddRange(GenerateWithKruglykovForFrequency(1, basePath, dirName, kruglykovName));
                scripts.AddRange(GenerateWithKruglykovForFrequency(0.1, basePath, dirName, kruglykovName));
                scripts.AddRange(GenerateWithKruglykovForFrequency(0.01, basePath, dirName, kruglykovName));
                scripts.AddRange(GenerateWithKruglykovForFrequency(0.001, basePath, dirName, kruglykovName));
            }

            return scripts;
        }


        private static List<string> GenerateForFrequency(double frequency, string basePath, string dirName)
        {
            var scriptContainer = new ScriptContainer(basePath, dirName, frequency);

            var scripts = new List<string>
            {
                scriptContainer.GenerateForComponent("Rho XY", columnIndex: 5, logScaleY: true),
                scriptContainer.GenerateForComponent("Rho YX", columnIndex: 6, logScaleY: true),
                scriptContainer.GenerateForComponent("Phs XY", columnIndex: 7, logScaleY: false),
                scriptContainer.GenerateForComponent("Phs YX", columnIndex: 8, logScaleY: false),
                scriptContainer.GenerateForComponent("W ZX", columnIndex: 9, logScaleY: false),
                scriptContainer.GenerateForComponent("W ZY", columnIndex: 10, logScaleY: false),
            };

            return scripts;
        }


        private static List<string> GenerateWithKruglykovForFrequency(double frequency, string basePath, string dirName, string kruglykovName)
        {
            var scriptContainer = new ScriptContainer(basePath, dirName, frequency);

            var scripts = new List<string>
            {
                scriptContainer.GenerateWithKryglykovForComponent(kruglykovName, "Rho XY", columnIndex: 5, logScaleY: true),
                scriptContainer.GenerateWithKryglykovForComponent(kruglykovName, "Rho YX", columnIndex: 6, logScaleY: true),
                scriptContainer.GenerateWithKryglykovForComponent(kruglykovName, "Phs XY", columnIndex: 7, logScaleY: false),
                scriptContainer.GenerateWithKryglykovForComponent(kruglykovName, "Phs YX", columnIndex: 8, logScaleY: false),
            };

            return scripts;
        }


        private static void CheckAllDirs(string basePath)
        {
            var scriptPath = Path.Combine(basePath, ScriptContainer.ScriptsDir);

            if (!Directory.Exists(scriptPath))
                Directory.CreateDirectory(scriptPath);

            var imagesPath = Path.Combine(basePath, ScriptContainer.ImagesDir);

            if (!Directory.Exists(imagesPath))
                Directory.CreateDirectory(imagesPath);
        }

        #endregion

        #region Kruglykov

        public static void ConvertAllKruglykovResults(string basePath, decimal y)
        {
            var krPath = Path.Combine(basePath, "KruglyakovResults");

            if (!Directory.Exists(krPath))
                return;

            var files = Directory.GetFiles(krPath, "tr_*.dat", SearchOption.AllDirectories);

            foreach (var file in files)
                ConvertOneKruglyakovResult(file, y);
        }


        public static void ConvertOneKruglyakovResult(string file, decimal y)
        {
            Console.WriteLine(file);

            var records = KruglyakovResultsReader.ReadResults(file);

            var dir = Path.GetDirectoryName(file);
            var name = string.Format("selected_{0}", Path.GetFileName(file));

            var dst = Path.Combine(dir, name);

            KruglyakovResultsReader.SaveForGnuplotPlotting(dst, records, y);

        }



        #endregion

        #region Porvem

        public static void ConvertAllExtremeMtResults(string basePath, string lateral, decimal y)
        {
            var files = Path.Combine(basePath, lateral);

            ConvertAllExtremeMtResults(files, y);
        }


        private static void ConvertAllExtremeMtResults(string path, decimal y)
        {
            var files = Directory.GetFiles(path, "fields.xml", SearchOption.AllDirectories);

            foreach (var file in files)
                ConvertAllExtremeMtResultsForFile(file, y);
        }


        public static void ConvertAllExtremeMtResultsForFile(string file, decimal y)
        {
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine(file);

            Console.WriteLine("load...");
            var rc = ResultsContainer.Load(file);

            var exporter = new PlainTextExporter(rc,  frequency:0.01)
            {
                PhaseInDegree = true,
                ReversePhaseSign = true,
            };

            var exportPath = Path.Combine(Path.GetDirectoryName(file), "selection.dat");

            Console.WriteLine("export...");

            exporter.ExportRhoPhaseAndTipper(exportPath);
        }

        #endregion

        #region Grayver





        public static void ConvertAllGrayverResults(string basePath)
        {
            string coordinates = Path.Combine(basePath, "GrayverResults", "receivers");

            string contrast1 = Path.Combine(basePath, "GrayverResults", "results", "1_0.01");
            string contrast10 = Path.Combine(basePath, "GrayverResults", "results", "10_0.001");
            string contrast3 = Path.Combine(basePath, "GrayverResults", "results", "3_0.00333");


            var coodinates = GrayverResultsReader.ReadCoordinates(coordinates);

            ReadAllFrequencies(coodinates, contrast1);
            ReadAllFrequencies(coodinates, contrast10);
            ReadAllFrequencies(coodinates, contrast3);
        }

        public static void CopyAllGrayverResults(string basePath)
        {
            //var laterals = new[] { "nx100ny100", "nx200ny200", "nx50ny50" };
            var laterals = new[] { "nx100ny100", "nx50ny50" };

            foreach (var lateral in laterals)
                CopyAllGrayverResults(basePath, lateral);
        }

        public static void CopyAllGrayverResults(string basePath, string lateral)
        {
            string contrast1 = Path.Combine(basePath, "GrayverResults", "results", "1_0.01");
            string contrast10 = Path.Combine(basePath, "GrayverResults", "results", "10_0.001");
            string contrast3 = Path.Combine(basePath, "GrayverResults", "results", "3_0.00333");

            string dstContrast1 = Path.Combine(basePath, lateral, "contrast1", "Grayver");
            string dstContrast3 = Path.Combine(basePath, lateral, "contrast3", "Grayver");
            string dstContrast10 = Path.Combine(basePath, lateral, "contrast10", "Grayver");

            CopyAllGrayverResultsLastStep(contrast1, dstContrast1);
            CopyAllGrayverResultsLastStep(contrast3, dstContrast3);
            CopyAllGrayverResultsLastStep(contrast10, dstContrast10);
        }

        private static void CopyAllGrayverResultsLastStep(string srcDir, string dstDir)
        {
            if (!Directory.Exists(srcDir))
                return;

            if (!Directory.Exists(dstDir))
                Directory.CreateDirectory(dstDir);

            var files = Directory.GetFiles(srcDir, "grayver_selected*.dat", SearchOption.TopDirectoryOnly);

            foreach (var file in files)
                File.Copy(file, Path.Combine(dstDir, Path.GetFileName(file)));
        }


        private static void ReadAllFrequencies(Coordinate[] coodinates, string basePath)
        {
            if (!Directory.Exists(basePath))
                return;

            var files = Directory.GetFiles(basePath, "*output_data*");

            foreach (var file in files)
            {
                Console.WriteLine(file);
                var freqStr = Path.GetFileName(file).Substring(0, 7);


                var freq = double.Parse(freqStr, NumberStyles.Float, CultureInfo.InvariantCulture);
                var records = GrayverResultsReader.ReadResults(Path.Combine(basePath, file), freq);

                string newFile = Path.Combine(basePath, string.Format("grayver_selected_{0}.dat", freqStr));

                GrayverResultsReader.SaveForGnuplotPlotting(newFile, coodinates, records, freq);
            }
        }

        #endregion
    }
}
