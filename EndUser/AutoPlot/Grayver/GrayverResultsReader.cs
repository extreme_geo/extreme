﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Reflection.Emit;
using Extreme.Cartesian.Model;

namespace AutoPlot
{
    public class GrayverResultsReader
    {
        public static void SaveForGnuplotPlotting(string path, Coordinate[] coordinates,
            GrayverRecord[] records, double frequency)
        {
            using (var sw = new StreamWriter(path))
            {
                sw.WriteLine("%         x          y          z           freq         Rho_XX         Rho_XY         Rho_YX         Rho_YY         Phs_XX         Phs_XY         Phs_YX         Phs_YY       W_ZX_Mag       W_ZY_Mag");

                for (int i = 0; i < records.Length; i++)
                {
                    var rec = records[i].Record;
                    var coord = coordinates[i];


                    sw.Write("{0} ", coord.X.ToString().PadLeft(11));
                    sw.Write("{0} ", coord.Y.ToString().PadLeft(10));
                    sw.Write("{0} ", coord.Z.ToString().PadLeft(10));

                    sw.Write("{0} ", frequency.ToString().PadLeft(14));

                    sw.Write("{0} ", rec.RhoXX.ToString().PadLeft(14));
                    sw.Write("{0} ", rec.RhoXY.ToString().PadLeft(14));
                    sw.Write("{0} ", rec.RhoYX.ToString().PadLeft(14));
                    sw.Write("{0} ", rec.RhoYY.ToString().PadLeft(14));


                    sw.Write("{0} ", rec.PhsXX.ToString().PadLeft(14));
                    sw.Write("{0} ", rec.PhsXY.ToString().PadLeft(14));
                    sw.Write("{0} ", rec.PhsYX.ToString().PadLeft(14));
                    sw.Write("{0} ", rec.PhsYY.ToString().PadLeft(14));

                    sw.Write("{0} ", rec.Wzx.Real.ToString().PadLeft(14));
                    sw.Write("{0} ", rec.Wzx.Imaginary.ToString().PadLeft(14));
                    sw.Write("{0} ", rec.Wzy.Real.ToString().PadLeft(14));
                    sw.Write("{0} ", rec.Wzy.Imaginary.ToString().PadLeft(14));

                    sw.WriteLine();
                }
            }
        }

        public static GrayverRecord[] ReadResults(string path, double frequency)
        {
            var strings = File.ReadAllLines(path).Skip(1);

            return strings
                .Select(s => SplitAndParseResults(s, frequency))
                .Where(gr => gr != null)
                .ToArray();
        }

        private static GrayverRecord SplitAndParseResults(string line, double frequency)
        {
            var splitted = line.Split(' ', '\t');

            if (splitted[0] != "NS" && splitted[0] != "PlaneWave_NS")
                return null;

            var gr = new GrayverRecord(receiver: splitted[1]);


            var omega = OmegaModelUtils.FrequencyToOmega(frequency);

            var zxx = ParseFromCppComplexToComplex(splitted[2]);
            var zyy = ParseFromCppComplexToComplex(splitted[5]);

            gr.Record.RhoXX = CalculateApparentResistivity(zxx, omega);
            gr.Record.RhoYY = CalculateApparentResistivity(zyy, omega);

            gr.Record.PhsXX = ProcessPhase(zxx);
            gr.Record.PhsYY = ProcessPhase(zyy);

            gr.Record.RhoXY = ParseFromCppComplexToDouble(splitted[8]);
            gr.Record.RhoYX = ParseFromCppComplexToDouble(splitted[9]);

            gr.Record.PhsXY = ParseFromCppComplexToDouble(splitted[10]);
            gr.Record.PhsYX = ParseFromCppComplexToDouble(splitted[11]);

            gr.Record.Wzx = ParseFromCppComplexToComplex(splitted[6]);
            gr.Record.Wzy = ParseFromCppComplexToComplex(splitted[7]);

            return gr;
        }


        private const double Mu0 = (4.0 * System.Math.PI * 1.0E-07);

        private static double CalculateApparentResistivity(Complex z, double omega)
        {
            return (z.Magnitude * z.Magnitude) / (omega * Mu0);
        }

        private static double ProcessPhase(Complex value)
        {
            var result = value.Phase;

            result *= (180 / System.Math.PI);

            result = -result;

            return result;
        }

        private static Complex ParseFromCppComplexToComplex(string str)
        {
            var splitted = str.Trim('(', ')').Split(',');

            return new Complex(
                double.Parse(splitted[0], NumberStyles.Float, CultureInfo.InvariantCulture),
                double.Parse(splitted[1], NumberStyles.Float, CultureInfo.InvariantCulture));
        }

        private static double ParseFromCppComplexToDouble(string str)
        {
            var trimmed = str.Trim('(', ')', '0', ',');

            return double.Parse(trimmed, NumberStyles.Float, CultureInfo.InvariantCulture);
        }


        public static Coordinate[] ReadCoordinates(string path)
        {
            var strings = File.ReadAllLines(path).Skip(1);

            return strings.Select(SplitAndParseCoordinate).ToArray();
        }

        private static Coordinate SplitAndParseCoordinate(string line)
        {
            var splitted = line.Split(' ', '\t');

            var coodinate = new Coordinate(
                x: decimal.Parse(splitted[1]),
                y: decimal.Parse(splitted[2]),
                z: decimal.Parse(splitted[3]));

            return coodinate; ;
        }
    }
}
