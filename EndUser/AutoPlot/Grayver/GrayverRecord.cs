﻿using System.Numerics;

namespace AutoPlot
{
    public class GrayverRecord
    {
        public string Receiver { get; private set; }
        public Record Record { get; private set; }

        public GrayverRecord(string receiver)
        {
            Receiver = receiver;
            Record = new Record();
        }
    }
}
