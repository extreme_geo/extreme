﻿namespace AutoPlot
{
    public class Coordinate
    {
        public decimal X { get; private set; }
        public decimal Y { get; private set; }
        public decimal Z { get; private set; }

        public Coordinate(decimal x, decimal y, decimal z)
        {
            X = x;
            Y = y;
            Z = z;
        }
    }
}
