﻿using System.Numerics;

namespace AutoPlot
{
    public class Record
    {
        public double RhoXX { get; set; }
        public double RhoXY { get; set; }
        public double RhoYX { get; set; }
        public double RhoYY { get; set; }
        public double PhsXX { get; set; }
        public double PhsXY { get; set; }
        public double PhsYX { get; set; }
        public double PhsYY { get; set; }
        public Complex Wzx { get; set; }
        public Complex Wzy { get; set; }

        public Record()
        {
           
        }
    }
}
