﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Numerics;

namespace AutoPlot
{
    public class KruglyakovResultsReader
    {
        public static void SaveForGnuplotPlotting(string path, KruglyakovRecord[] records, decimal y)
        {
            using (var sw = new StreamWriter(path))
            {
                sw.WriteLine("%         x          y          z           freq         Rho_XX         Rho_XY         Rho_YX         Rho_YY         Phs_XX         Phs_XY         Phs_YX         Phs_YY       W_ZX_Mag       W_ZY_Mag");

                for (int i = 0; i < records.Length; i++)
                {
                    var rec = records[i].Record;
                    var coord = records[i].Coordinate;
                    var freq = records[i].Frequency;

                    if (coord.Y != y)
                        continue;


                    sw.Write("{0} ", coord.X.ToString().PadLeft(11));
                    sw.Write("{0} ", coord.Y.ToString().PadLeft(10));
                    sw.Write("{0} ", coord.Z.ToString().PadLeft(10));

                    sw.Write("{0} ", freq.ToString().PadLeft(14));

                    sw.Write("{0} ", rec.RhoXX.ToString().PadLeft(14));
                    sw.Write("{0} ", rec.RhoXY.ToString().PadLeft(14));
                    sw.Write("{0} ", rec.RhoYX.ToString().PadLeft(14));
                    sw.Write("{0} ", rec.RhoYY.ToString().PadLeft(14));

                    sw.Write("{0} ", rec.PhsXX.ToString().PadLeft(14));
                    sw.Write("{0} ", rec.PhsXY.ToString().PadLeft(14));
                    sw.Write("{0} ", rec.PhsYX.ToString().PadLeft(14));
                    sw.Write("{0} ", rec.PhsYY.ToString().PadLeft(14));

                    sw.Write("{0} ", rec.Wzx.Real.ToString().PadLeft(14));
                    sw.Write("{0} ", rec.Wzx.Imaginary.ToString().PadLeft(14));
                    sw.Write("{0} ", rec.Wzy.Real.ToString().PadLeft(14));
                    sw.Write("{0} ", rec.Wzy.Imaginary.ToString().PadLeft(14));

                    sw.WriteLine();
                }
            }
        }

        public static KruglyakovRecord[] ReadResults(string path)
        {
            var strings = File.ReadAllLines(path).Skip(2);

            return strings
                .Select(SplitAndParseResults)
                .Where(gr => gr != null)
                .ToArray();
        }

        private static KruglyakovRecord SplitAndParseResults(string line)
        {
            var splitted = line.Split(' ', '\t').Where(s => !string.IsNullOrEmpty(s)).ToArray();

            var wzx = new Complex(ParseDoubleToDouble(splitted[20]), ParseDoubleToDouble(splitted[21]));
            var wzy = new Complex(ParseDoubleToDouble(splitted[22]), ParseDoubleToDouble(splitted[23]));

            var kr = new KruglyakovRecord(
                coordinate: ParseCoordinate(splitted),
                frequency: ParseDoubleToDouble(splitted[3]));
            
            kr.Record.RhoXX = ParseDoubleToDouble(splitted[12]);
            kr.Record.RhoXY = ParseDoubleToDouble(splitted[13]);
            kr.Record.RhoYX = ParseDoubleToDouble(splitted[14]);
            kr.Record.RhoYY = ParseDoubleToDouble(splitted[15]);

            kr.Record.PhsXX = ParseDoubleToDouble(splitted[16]);
            kr.Record.PhsXY = ParseDoubleToDouble(splitted[17]);
            kr.Record.PhsYX = ParseDoubleToDouble(splitted[18]);
            kr.Record.PhsYY = ParseDoubleToDouble(splitted[19]);

            kr.Record.Wzx = wzx;
            kr.Record.Wzy = wzy;
            
            return kr;
        }

        private static double ParseDoubleToDouble(string str)
        {
            return double.Parse(str, NumberStyles.Float, CultureInfo.InvariantCulture);
        }

        private static Coordinate ParseCoordinate(string[] splitted)
        {
            var coodinate = new Coordinate(
                x: (decimal)double.Parse(splitted[0], NumberStyles.Float, CultureInfo.InvariantCulture),
                y: (decimal)double.Parse(splitted[1], NumberStyles.Float, CultureInfo.InvariantCulture),
                z: (decimal)double.Parse(splitted[2], NumberStyles.Float, CultureInfo.InvariantCulture));

            return coodinate; ;
        }
    }
}
