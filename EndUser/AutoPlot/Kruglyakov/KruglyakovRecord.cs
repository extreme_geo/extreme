﻿using System.Numerics;

namespace AutoPlot
{
    public class KruglyakovRecord
    {
        public Coordinate Coordinate { get; private set; }
        public double Frequency { get; private set; }
        public Record Record { get; private set; }

        public KruglyakovRecord(Coordinate coordinate, double frequency)
        {
            Frequency = frequency;
            Coordinate = coordinate;
            Record = new Record();
        }
    }
}
