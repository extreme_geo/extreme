﻿namespace AutoPlot
{
    partial class GnuplotScript
    {
        private readonly string _path;
        private readonly string _grPath;

        private bool _logscaleY;

        private string _output;
        private string _title;
        private double _freq;
        private double _columnIndex;

        public GnuplotScript(string path, string grPath)
        {
            _path = path;
            _grPath = grPath;
        }

        public double Freq
        {
            get { return _freq; }
            set { _freq = value; }
        }

        public string Title
        {
            get { return _title; }
            set { _title = value; }
        }

        public double ColumnIndex
        {
            get { return _columnIndex; }
            set { _columnIndex = value; }
        }

        public string Output
        {
            get { return _output; }
            set { _output = value; }
        }

        public bool LogscaleY
        {
            get { return _logscaleY; }
            set { _logscaleY = value; }
        }
    }
}
