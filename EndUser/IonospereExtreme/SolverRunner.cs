﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Numerics;
using Extreme.Cartesian.Core;
using Extreme.Cartesian.Forward;
using Extreme.Cartesian.Ionosphere;
using Extreme.Cartesian.Logger;
using Extreme.Cartesian.Magnetotellurics;
using Extreme.Cartesian.Model;
using Extreme.Cartesian.Project;
using Extreme.Core;
using Extreme.Core.Logger;
using Extreme.Parallel;
using Extreme.Parallel.Logger;

namespace IonospereExtreme
{

    public class SolverRunner : IDisposable
    {
        private readonly ForwardProject _project;
        private readonly INativeMemoryProvider _memoryProvider;
        private readonly Mpi _mpi;
        private readonly ILogger _logger;
        private readonly IonosphereForwardSolver _solver;

        public SolverRunner(ForwardProject project, INativeMemoryProvider memoryProvider, Mpi mpi)
        {
            if (project == null) throw new ArgumentNullException(nameof(project));
            if (memoryProvider == null) throw new ArgumentNullException(nameof(memoryProvider));
            if (mpi == null) throw new ArgumentNullException(nameof(mpi));

            _project = project;
            _memoryProvider = memoryProvider;
            _mpi = mpi;
            _logger = _mpi == null ? (ILogger)new ConsoleLogger() : new ParallelConsoleLogger(_mpi);

            _solver = new IonosphereForwardSolver(_logger, _memoryProvider, _project.ForwardSettings);
        }

        public void Run(CartesianModel model, double frequency)
        {
            ForwardLoggerHelper.WriteStatus(_logger, "Starting Cartesian Ionosphere Forward solver...");
            ForwardLoggerHelper.WriteStatus(_logger, _mpi != null && _mpi.Size > 1 ? $"MPI processes = {_mpi.Size}" : "MPI - NO");
            ForwardLoggerHelper.WriteStatus(_logger, $"Number of threads: {MultiThreadUtils.MaxDegreeOfParallelism}");
            ForwardLoggerHelper.WriteStatus(_logger, $"Number of hankels: {_project.ForwardSettings.NumberOfHankels}");
            ForwardLoggerHelper.WriteStatus(_logger, $"Target residual  : {_project.ForwardSettings.Residual}");

            var omegaModel = OmegaModelBuilder.BuildOmegaModel(model, frequency);

            var eFields = new List<FieldsAtLevelCalculatedEventArgs>();
            var hFields = new List<FieldsAtLevelCalculatedEventArgs>();

            _solver.EFieldsAtLevelCalculated += (s, e) => eFields.Add(e);
            _solver.HFieldsAtLevelCalculated += (s, e) => hFields.Add(e);

            var sources = ProvideSources(omegaModel);

            foreach (var s in sources)
            {
                var sourceField = CreateSourceFieldX(omegaModel, 1);
                _solver.AddSource(s, sourceField);
            }

            _solver
                .With(_project.ObservationLevels)
                .WithMpi(_mpi);

            _solver.Solve(omegaModel);

            ForwardLoggerHelper.WriteStatus(_logger, "Finish 1");


            _solver.ClearSources();
            foreach (var s in sources)
            {
                var sourceField = CreateSourceFieldY(omegaModel, -1);
                _solver.AddSource(s, sourceField);
            }

            _solver.Solve(omegaModel);
            ForwardLoggerHelper.WriteStatus(_logger, "Finish 2");

            if (!_solver.IsParallel || _mpi.Rank < _mpi.Size/2)
            {
                var rc = GatherSolution(_project, omegaModel, eFields, hFields);

                if (_mpi == null || _mpi.IsMaster)
                    Export(rc, frequency);

            }
        }


        private const int МногоИсточников = 1;
        private SourceLayer[] ProvideSources(OmegaModel model)
        {
            //return _project.SourceLayers.ToArray();

            const int number = МногоИсточников;
            const decimal depth = -90000m;

            var result = new List<SourceLayer>(number * number);

            decimal step = number / 2m - 0.5m;
            var xLength = model.Nx * model.LateralDimensions.CellSizeX;
            var yLength = model.Ny * model.LateralDimensions.CellSizeY;


            for (int i = 0; i < number; i++)
            {
                for (int j = 0; j < number; j++)
                {
                    var xShift = (-step + i) * xLength;
                    var yShift = (-step + j) * yLength;

                    result.Add(new SourceLayer(xShift, yShift, depth));
                }
            }

            return result.ToArray();
        }

        private AnomalyCurrent CreateSourceFieldX(OmegaModel model, Complex value)
        {
            var field = AnomalyCurrent.AllocateNew(_memoryProvider, model.Nx, model.Ny, 1);

            var accX = VerticalLayerAccessor.NewX(field, 0);
            var accY = VerticalLayerAccessor.NewY(field, 0);
            var accZ = VerticalLayerAccessor.NewZ(field, 0);

            for (int i = 0; i < accX.Nx; i++)
                for (int j = 0; j < accX.Ny; j++)
                {
                    accX[i, j] = value;
                    accY[i, j] = 0;
                    accZ[i, j] = 0;
                }

            return field;
        }

        private AnomalyCurrent CreateSourceFieldY(OmegaModel model, Complex value)
        {
            var field = AnomalyCurrent.AllocateNew(_memoryProvider, model.Nx, model.Ny, 1);
            var accX = VerticalLayerAccessor.NewX(field, 0);
            var accY = VerticalLayerAccessor.NewY(field, 0);
            var accZ = VerticalLayerAccessor.NewZ(field, 0);

            for (int i = 0; i < accX.Nx; i++)
                for (int j = 0; j < accX.Ny; j++)
                {
                    accX[i, j] = 0;
                    accY[i, j] = value;
                    accZ[i, j] = 0;
                }

            return field;
        }

        private ResultsContainer GatherSolution(ForwardProject project, OmegaModel model, List<FieldsAtLevelCalculatedEventArgs> eFields, List<FieldsAtLevelCalculatedEventArgs> hFields)
        {
            ForwardLoggerHelper.WriteStatus(_logger, "Gather results...");
            var rc = new ResultsContainer(model.LateralDimensions);
            
            foreach (var observationLevel in project.ObservationLevels)
            {
                var all = GatherAllFieldsAtLevel(observationLevel, eFields, hFields);

                if (_mpi == null || _mpi.IsMaster)
                    rc.Add(all);
            }

            return rc;
        }

        private void Export(ResultsContainer rc, double frequency)
        {
            ForwardLoggerHelper.WriteStatus(_logger, "Exporting results...");

            var resultPath = _project.ResultsPath;

            if (!Directory.Exists(resultPath))
                Directory.CreateDirectory(resultPath);

            var exporter = new PlainTextExporter(rc, frequency);

            ForwardLoggerHelper.WriteStatus(_logger, "\t Export Raw fields");
            exporter.ExportRawFields(Path.Combine(resultPath, "fields.dat"));

            ForwardLoggerHelper.WriteStatus(_logger, "\t Export MT responses");
            exporter.ExportMtResponses(Path.Combine(resultPath, "responses.dat"));
        }

        private AllFieldsAtLevel GatherAllFieldsAtLevel(ObservationLevel level, List<FieldsAtLevelCalculatedEventArgs> eFields, List<FieldsAtLevelCalculatedEventArgs> hFields)
        {
            var e1 = eFields.First(f => f.Level == level);
            var e2 = eFields.Last(f => f.Level == level);
            var h1 = hFields.First(f => f.Level == level);
            var h2 = hFields.Last(f => f.Level == level);

            return new AllFieldsAtLevel(level)
            {
                AnomalyE1 = DistibutedUtils.GatherFromAllProcesses(_solver, e1.AnomalyField),
                NormalE1 = DistibutedUtils.GatherFromAllProcesses(_solver, e1.NormalField),
                AnomalyE2 = DistibutedUtils.GatherFromAllProcesses(_solver, e2.AnomalyField),
                NormalE2 = DistibutedUtils.GatherFromAllProcesses(_solver, e2.NormalField),

                AnomalyH1 = DistibutedUtils.GatherFromAllProcesses(_solver, h1.AnomalyField),
                NormalH1 = DistibutedUtils.GatherFromAllProcesses(_solver, h1.NormalField),
                AnomalyH2 = DistibutedUtils.GatherFromAllProcesses(_solver, h2.AnomalyField),
                NormalH2 = DistibutedUtils.GatherFromAllProcesses(_solver, h2.NormalField),
            };
        }

        public void Dispose()
        {
            _solver.Dispose();
        }
    }
}
