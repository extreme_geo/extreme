﻿using System;
using Extreme.Cartesian.FftW;
using Extreme.Cartesian.Forward;
using Extreme.Cartesian.Model;
using Extreme.Cartesian.Project;
using Extreme.Core.Model;
using Extreme.Parallel;
using Extreme.Model;
using Extreme.Model.SimpleCommemi3D;

namespace IonospereExtreme
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length < 1)
            {
                RunDemoMode();
            }
            else
            {
                string projectFileName = args[0];

                var project = ForwardProjectSerializer.Load(projectFileName);
                RunFullVersion(project);
            }
        }

        private static void RunDemoMode()
        {
            Console.WriteLine("I am Ionosphere eXtrEMe, please provide parameters");
        }

        private static void RunFullVersion(ForwardProject project)
        {
            using (var mpi = Mpi.Init())
            using (var memoryProvider = new FftWMemoryProvider())
            {
                using (var runner = new SolverRunner(project, memoryProvider, mpi))
                {
                    foreach (var frequency in project.Frequencies)
                    {
                        var model = CreateCartesianModelCommemi(project, mpi);

                        runner.Run(model, frequency);

                        ParallelMemoryUtils.ExportMemoryUsage(project.ResultsPath, mpi, memoryProvider, frequency);
                    }
                }
            }
        }

        private static CartesianModel CreateCartesianModelCommemi(ForwardProject project, Mpi mpi)
        {
            var modelFile = project.ModelFile;

            if (ModelSettingsSerializer.IsModelCommemi(modelFile))
            {
                var settings = ModelSettingsSerializer.LoadCommemiFromXml(modelFile);
                return GenerateCommemiModel(mpi, settings);
            }
            else if (ModelSettingsSerializer.IsModelCommemi3D3(modelFile))
            {
                var settings = ModelSettingsSerializer.LoadCommemi3D3FromXml(modelFile);
                return GenerateCommemi3D3Model(mpi, settings);
            }

            throw new InvalidOperationException($"Unknow model type in the file {modelFile}");
        }

        private static CartesianModel GenerateCommemiModel(Mpi mpi, CommemiModelSettings settings)
        {
            var creater = new SimpleCommemi3DModelCreater(settings.AnomalySizeInMeters);

            return GenerateModel(mpi, settings.Mesh, () =>
                    creater.CreateNonMeshedModel(settings.LeftConductivity, settings.RightConductivity));
        }

        private static CartesianModel GenerateCommemi3D3Model(Mpi mpi, Commemi3D3ModelSettings settings)
        {
            return GenerateModel(mpi, settings.Mesh, Commemi3D3ModelCreater.CreateNonMeshedModel);
        }

        private static CartesianModel GenerateModel(Mpi mpi, MeshParameters mesh, Func<NonMeshedModel> genNonMeshed)
        {
            var nonMeshed = genNonMeshed();
            var converter = new NonMeshedToCartesianModelConverter(nonMeshed);

            var nxStart = 0;
            var nxLength = mesh.Nx;

            if (mpi != null && mpi.IsParallel)
            {
                nxStart = mpi.CalcLocalHalfNxStart(mesh.Nx);
                nxLength = mpi.CalcLocalHalfNxLength(mesh.Nx);
            }

            var cartesian = converter.Convert(mesh, nxStart, nxLength);
            return cartesian;
        }
    }
}
