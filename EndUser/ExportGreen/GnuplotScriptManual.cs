﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExportGreen
{
    partial class GnuplotScript
    {
        private readonly string _path;
        private readonly string _title;
        private readonly string _output;

        public GnuplotScript(string title, string path, string output)
        {
            _title = title;
            _path = path;
            _output = output;
        }
    }
}
