﻿using System;
using System.IO;
using System.Numerics;
using System.Runtime.InteropServices;
using Extreme.Cartesian.Green.Scalar;

namespace ExportGreen
{
    public static class ScalarSerializer
    {
        public static GreenScalars ImportFromFile(string fileName)
        {
            using (var br = new BinaryReader(File.OpenRead(fileName)))
                return ImportFromStream(br);
        }

        public static void ExportToFile(string fileName, GreenScalars scalars)
        {
            using (var bw = new BinaryWriter(File.OpenWrite(fileName)))
                ExportToStream(bw, scalars);
        }

        private static GreenScalars ImportFromStream(BinaryReader br)
        {
            throw new NotImplementedException();
        }

        public static void ExportToStream(BinaryWriter bw, GreenScalars scalars)
        {
            throw new NotImplementedException();
        }

        public static void Write(this BinaryWriter bw, Complex[] value)
        {
            for (int i = 0; i < value.Length; i++)
            {
                bw.Write(value[i].Real);
                bw.Write(value[i].Imaginary);
            }
        }

        public static void Write(this BinaryWriter bw, double[] value)
        {
            for (int i = 0; i < value.Length; i++)
                bw.Write(value[i]);
        }

        public static void ReadComplexArray(this BinaryReader br, Complex[] values)
        {
            for (int i = 0; i < values.Length; i++)
            {
                var real = br.ReadDouble();
                var imag = br.ReadDouble();
                values[i] = new Complex(real, imag);
            }
        }

        public static double[] ReadDoubleArray(this BinaryReader br, int length)
        {
            var values = new double[length];

            for (int i = 0; i < values.Length; i++)
                values[i] = br.ReadDouble();

            return values;
        }
    }
}
