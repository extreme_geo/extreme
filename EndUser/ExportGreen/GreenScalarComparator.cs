﻿using System;
using System.Numerics;
using Extreme.Cartesian.Green;
using Extreme.Cartesian.Green.Scalar;
using Extreme.Cartesian.Model;
using Extreme.Core;

namespace ExportGreen
{
    public class GreenScalarComparator
    {
        private readonly int _numberOfHank = 10;

        private readonly ILogger _logger;
        private readonly OmegaModel _model;

        public GreenScalarComparator(ILogger logger, OmegaModel model)
        {
            _logger = logger;
            _model = model;
        }

        public void CompareGreenScalars()
        {
            var startOld = DateTime.Now;
            var scalarsOld = CalculateScalarsAtoA();
            var timeOld = DateTime.Now - startOld;

            var startNew = DateTime.Now;
            var scalarsNew = CalculateScalarsAtoA();
            var timeNew = DateTime.Now - startNew;

            CompareScalars(scalarsOld, scalarsNew);

            Console.WriteLine("{0} {1} {2}", timeOld.TotalSeconds, timeNew.TotalSeconds, timeOld.TotalSeconds / timeNew.TotalSeconds);
            Console.WriteLine(scalarsOld);
            Console.WriteLine(scalarsNew);
        }

        private GreenScalars CalculateScalarsAtoA()
        {
            var plan = new ScalarPlansCreater(_model.LateralDimensions, HankelCoefficients.LoadN40(), _numberOfHank)
                .CreateForAnomalyToAnomaly();

            var scalarCalc = GreenScalarCalculator.NewAtoACalculator(_logger, _model);
            var trcs = TranceiverUtils.CreateAnomalyToAnomaly(_model.Anomaly);
            var scalars = scalarCalc.Calculate(plan, trcs);

            return scalars;
        }


        private static void CompareScalars(GreenScalars scalarsOld, GreenScalars scalarsNew)
        {
            int length = scalarsOld.SingleScalars.Length;

            for (int i = 0; i < length; i++)
            {
                var olds = scalarsOld.SingleScalars[i];
                var news = scalarsNew.SingleScalars[i];

                Console.WriteLine("[tr,rc]: {0} {1}", olds.Transceiver.Transmitter, olds.Transceiver.Receiver);
                CompareScalars(olds, news);
            }
        }

        private static void CompareScalars(SingleGreenScalar ss1, SingleGreenScalar ss2)
        {
            double eps = 1E-14;

            Action<Complex, Complex, string> cmp = (c1, c2, s) =>
            {
                if (double.IsNaN(c1.Magnitude))
                    Console.WriteLine("NaN old");

                if (double.IsNaN(c2.Magnitude))
                    Console.WriteLine("NaN new");

                if (Math.Abs(c1.Magnitude - c2.Magnitude) > eps)
                    Console.WriteLine("actung {2}: {0} {1}", c1.Magnitude, c2.Magnitude, s);
            };

            for (int i = 0; i < ss1.I1.Length; i++)
            {
                cmp(ss1.I1[i], ss2.I1[i], "1");
                cmp(ss1.I2[i], ss2.I2[i], "2");
                cmp(ss1.I3[i], ss2.I3[i], "3");
                cmp(ss1.I4[i], ss2.I4[i], "4");
                cmp(ss1.I5[i], ss2.I5[i], "5");
            }
        }

    }
}
