﻿using System;
using System.Collections.Generic;
using System.Data.Odbc;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExportGreen
{
    public class GreenTensorParameters
    {
        private readonly int _ndec = 40;
        private readonly int _numberOfHankels;
        private readonly bool _useOriginalInterpolationSplines;
        
        public static GreenTensorParameters CreateClassic()
        {
            return new GreenTensorParameters(numberOfHankels:1, useOriginalInterpolationSplines: true, ndec: 40);
        }

        public static GreenTensorParameters CreateNewVersion(int numberOfHankels)
        {
            return new GreenTensorParameters(numberOfHankels, useOriginalInterpolationSplines: false, ndec: 40);
        }
        
        private GreenTensorParameters(int numberOfHankels, bool useOriginalInterpolationSplines, int ndec)
        {
            _numberOfHankels = numberOfHankels;
            _useOriginalInterpolationSplines = useOriginalInterpolationSplines;
            _ndec = ndec;
        }
        
        public int NumberOfHankels
        {
            get { return _numberOfHankels; }
        }
       
        public bool UseOriginalInterpolationSplines
        {
            get { return _useOriginalInterpolationSplines; }
        }
      
        public int Ndec
        {
            get { return _ndec; }
        }
    }
}
