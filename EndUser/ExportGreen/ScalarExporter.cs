﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Numerics;
using Extreme.Cartesian.Green.Scalar;
using Extreme.Core;

namespace ExportGreen
{
    public static class ScalarExporter
    {
        public static void SaveAll(string path, SingleGreenScalar scalar, double[] rho)
        {
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            Do(Path.Combine(path, "t2t_t"), scalar.I1, rho);
            Do(Path.Combine(path, "t2t_p"), scalar.I2, rho);
            Do(Path.Combine(path, "t2z_"), scalar.I4, rho);
            Do(Path.Combine(path, "z2t_"), scalar.I3, rho);
            Do(Path.Combine(path, "z2z_"), scalar.I5, rho);
        }

        private static void Do(string path, Complex[] f, double[] rho)
        {
            var knots = GetKnotPoints(f, rho);

            ExportToFile(path + @"_knots.dat", knots);
        }
        private static IList<Point> GetKnotPoints(Complex[] f, double[] rad)
        {
            var results = new List<Point>();

            for (int i = 0; i < rad.Length; i++)
                results.Add(CreatePoint(i, rad, f));

            return results;
        }


        private static void ExportToFile(string fileName, IEnumerable<Point> points)
        {
            using (var sw = new StreamWriter(fileName))
            {
                foreach (var p in points)
                {
                    sw.WriteLine("{0} {1} {2}", p.X, p.F.Real, p.F.Imaginary);
                }
            }
        }

        private static Point CreatePoint(int i, double[] r, Complex[] f)
        {
            return new Point(r[i], f[i]);
        }
    }
}
