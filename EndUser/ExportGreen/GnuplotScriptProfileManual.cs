﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExportGreen
{
    partial class GnuplotScriptProfile
    {
        private readonly string _path;
        private readonly string _title;
        private readonly string _output;
        private readonly int _y;

        public GnuplotScriptProfile(string title, string path, string output, int y)
        {
            _title = title;
            _path = path;
            _output = output;
            _y = y;
        }
    }
}
