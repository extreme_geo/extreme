﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace ExportGreen
{
    public class Point
    {
        public int Test { get; set; }
        
        private readonly double _x;
        private readonly Complex _f;

        public Point(double x, Complex f)
        {
            _x = x;
            _f = f;
        }

        public double X
        {
            get { return _x; }
        }

        public Complex F
        {
            get { return _f; }
        }
    }
}
