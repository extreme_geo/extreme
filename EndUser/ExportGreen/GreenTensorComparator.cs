﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Numerics;
//using Porvem.Cartesian;
//using Porvem.Cartesian.Core;
//using Porvem.Cartesian.FftW;
//using Porvem.Cartesian.Green;
//using Porvem.Cartesian.Green.Scalar;
//using Porvem.Cartesian.Green.Tensor;
//using Porvem.Cartesian.Green.Tensor.Impl;
//using Porvem.Cartesian.IntelMklFft;
//using Porvem.Cartesian.Memory;
//using Porvem.Cartesian.Model;
//using Porvem.Core;
//using static System.Math;

//namespace ExportGreen
//{
//    public class GreenTensorComparator
//    {
//        private readonly OmegaModel _model;

//        private readonly ILogger _logger;
//        private readonly IProfiler _profiler;
//        private readonly INativeMemoryProvider _memoryProvider;
//        private readonly IGreenTensorMemoryProvider _greenTensorMemoryProvider;
//        private readonly IFourierTransform _fourierTransform;
//        private readonly IGreenScalarCalculatorProvider _greenScalarCalculatorProvider;

//        private readonly int _numberOfhankels = 10;

//        private int Nz => _model.Anomaly.Layers.Count;

//        public GreenTensorComparator(ILogger logger, OmegaModel model)
//        {
//            _logger = logger;
//            _model = model;

//            _profiler = new Profiler();
//            _memoryProvider = new IntelMklMemoryProvider();
//            _greenTensorMemoryProvider = new GreenTensorMemoryProvider(_memoryProvider);
//            _fourierTransform = new IntelFourierTransform();
//            _greenScalarCalculatorProvider = new GreenScalarCalculatorProvider(_logger);
//        }

//        private void LogStart(string message)
//            => _logger.WriteWarning($"\n\n\t\t\t{message}:\n");

//        #region Anomaly

//        public void CompareGreenTensorsAtoA()
//        {
//            LogStart("Compare AtoA");
//            GreenScalarCalculator.UseFastCalculation = true;
//            var scalars = ScalarUtils.CalculateScalarsAtoA(_logger, _model, _numberOfhankels);
//            GreenScalarCalculator.UseFastCalculation = false;

//            var gt1 = CalculateOriginGreenTensor(scalars);
//            var gt2 = CalculateNewGreenTensor(scalars);

//            DumpGreenTensor(gt1, gt2);
//            CompareGreenTensors(gt1, gt2, Nz, Nz);
//        }

//        private IGreenTensor CalculateOriginGreenTensor(GreenScalars scalars)
//        {
//            var calc = new GreenTensorCalculatorAtoA(
//                _logger,
//                _profiler,
//                _model,
//                _memoryProvider,
//                _greenTensorMemoryProvider,
//                _fourierTransform,
//                _greenScalarCalculatorProvider,
//                _numberOfhankels)
//            {
//                PreCalculatedScalars = scalars,
//                _performFft = true
//            };

//            return calc.Calculate();
//        }

//        private GreenTensorAlongZ CalculateNewGreenTensor(GreenScalars scalars)
//        {
//            var calc = new GreenTensorCalculatorImpl(_logger, _model, _memoryProvider, _fourierTransform);
//            var gt = calc.CalculateAtoA(scalars, MemoryLayoutOrder.AlongLateral);
//            return gt;
//        }

//        #endregion

//        #region Electric

//        public void CompareGreenTensorsAtoOLevelElectric(ObservationLevel level)
//        {
//            LogStart("Compare AtoO Level Electric");
//            var scalars = CalculateScalarsAtoO(level);

//            var gt1 = CalculateOriginGreenTensorAtoOLevel(scalars, level);
//            var gt2 = CalculateNewGreenTensorAtoOLevel(scalars, level);

//            // DumpGreenTensor(gt1, gt2);
//            CompareGreenTensors(gt1, gt2, Nz, 1);
//        }

//        public void CompareGreenTensorsAtoOSiteElectric(ObservationSite site)
//        {
//            LogStart("Compare AtoO SITE Electric");
//            var scalars = CalculateScalarsAtoO(site);

//            var gt1 = CalculateOriginGreenTensorAtoOSite(scalars, site);
//            var gt2 = CalculateNewGreenTensorAtoOSite(scalars, site);

//            //DumpGreenTensor(gt1, gt2);
//            CompareGreenTensors(gt1, gt2, Nz, 1);
//        }

//        private GreenTensorAlongZ CalculateNewGreenTensorAtoOSite(GreenScalars scalars, ObservationSite site)
//        {
//            var calc = new GreenTensorCalculatorImpl(_logger, _model, _memoryProvider, _fourierTransform);
//            var gt = calc.CalculateAtoOElectric(scalars, site, MemoryLayoutOrder.AlongLateral);
//            return gt;
//        }

//        private GreenTensorAlongZ CalculateNewGreenTensorAtoOLevel(GreenScalars scalars, ObservationLevel level)
//        {
//            var calc = new GreenTensorCalculatorImpl(_logger, _model, _memoryProvider, _fourierTransform);
//            var gt = calc.CalculateAtoOElectric(scalars, level, MemoryLayoutOrder.AlongLateral);
//            return gt;
//        }

//        private GreenScalars CalculateScalarsAtoO(ObservationLevel level)
//        {
//            var hankel = HankelCoefficients.LoadN40();
//            var plan = ScalarPlansCreater.CreateForAnomalyToObservationLevels(_model.LateralDimensions, hankel, _numberOfhankels, level);
//            var transceiver = TranceiverUtils.CreateAnomalyToObservationLevels(_model.Anomaly, level);
//            var scalarCalculator = GreenScalarCalculator.NewAtoOElectricCalculator(_logger, _model, plan);

//            return scalarCalculator.Calculate2(transceiver);
//        }

//        private GreenScalars CalculateScalarsAtoO(ObservationSite site)
//        {
//            var hankel = HankelCoefficients.LoadN40();
//            var plan = ScalarPlansCreater.CreateForAnomalyToObservationSites(_model.LateralDimensions, hankel, _numberOfhankels, site);
//            var transceiver = TranceiverUtils.CreateAnomalyToObservationSites(_model.Anomaly, site);
//            var scalarCalculator = GreenScalarCalculator.NewAtoOElectricCalculator(_logger, _model, plan);

//            return scalarCalculator.Calculate2(transceiver);
//        }

//        private IGreenTensor CalculateOriginGreenTensorAtoOLevel(GreenScalars scalars, ObservationLevel level)
//        {
//            var calc = new GreenTensorCalculatorAtoOLevel(
//                _logger,
//                _profiler,
//                _model,
//                _memoryProvider,
//                _fourierTransform,
//                _numberOfhankels)
//            { PreCalculatedScalars = scalars };

//            return calc.Calculate(FieldToField.J2E, level);
//        }

//        private IGreenTensor CalculateOriginGreenTensorAtoOSite(GreenScalars scalars, ObservationSite site)
//        {
//            var calc = new GreenTensorCalculatorAtoOSite(
//                _logger,
//                _profiler,
//                _model,
//                _memoryProvider,
//                _numberOfhankels);

//            return calc.Calculate(site, FieldToField.J2E);
//        }

//        #endregion

//        #region Magnetic


//        public void CompareGreenTensorsAtoOSiteMagnetic(ObservationSite site)
//        {
//            LogStart("Compare AtoO SITE Magnetic");
//            var scalars = CalculateScalarsAtoOMagnetic(site);

//            var gt1 = CalculateOriginGreenTensorAtoOSiteMagnetic(scalars, site);
//            var gt2 = CalculateNewGreenTensorAtoOSiteMagnetic(scalars, site);

//            //DumpGreenTensor(gt1, gt2);
//            CompareGreenTensorsMagnetic(gt1, gt2, Nz, 1);
//        }

//        private GreenTensorAlongZ CalculateNewGreenTensorAtoOSiteMagnetic(GreenScalars scalars, ObservationSite site)
//        {
//            var calc = new GreenTensorCalculatorImpl(_logger, _model, _memoryProvider, _fourierTransform);
//            var gt = calc.CalculateAtoOMagnetic(scalars, site, MemoryLayoutOrder.AlongLateral);
//            return gt;
//        }

//        private IGreenTensor CalculateOriginGreenTensorAtoOSiteMagnetic(GreenScalars scalars, ObservationSite site)
//        {
//            var calc = new GreenTensorCalculatorAtoOSite(
//                _logger,
//                _profiler,
//                _model,
//                _memoryProvider,
//                _numberOfhankels);

//            return calc.Calculate(site, FieldToField.J2H);
//        }

//        public void CompareGreenTensorsAtoOLevelMagnetic(ObservationLevel level)
//        {
//            LogStart("Compare AtoO Level Magnetic");
//            var scalars = CalculateScalarsAtoOMagnetic(level);

//            var gt1 = CalculateOriginGreenTensorAtoOLevelMagnetic(scalars, level);
//            var gt2 = CalculateNewGreenTensorAtoOLevelMagnetic(scalars, level);

//            //DumpGreenTensor(gt1, gt2);
//            CompareGreenTensorsMagnetic(gt1, gt2, Nz, 1);
//        }


//        private GreenScalars CalculateScalarsAtoOMagnetic(ObservationSite site)
//        {
//            var hankel = HankelCoefficients.LoadN40();
//            var plan = ScalarPlansCreater.CreateForAnomalyToObservationSites(_model.LateralDimensions, hankel, _numberOfhankels, site);
//            var transceiver = TranceiverUtils.CreateAnomalyToObservationSites(_model.Anomaly, site);
//            var scalarCalculator = GreenScalarCalculator.NewAtoOMagneticCalculator(_logger, _model, plan);

//            return scalarCalculator.Calculate2(transceiver);
//        }

//        private GreenScalars CalculateScalarsAtoOMagnetic(ObservationLevel level)
//        {
//            var hankel = HankelCoefficients.LoadN40();
//            var plan = ScalarPlansCreater.CreateForAnomalyToObservationLevels(_model.LateralDimensions, hankel, _numberOfhankels, level);
//            var transceiver = TranceiverUtils.CreateAnomalyToObservationLevels(_model.Anomaly, level);
//            var scalarCalculator = GreenScalarCalculator.NewAtoOMagneticCalculator(_logger, _model, plan);

//            return scalarCalculator.Calculate2(transceiver);
//        }

//        private IGreenTensor CalculateOriginGreenTensorAtoOLevelMagnetic(GreenScalars scalars, ObservationLevel level)
//        {
//            var calc = new GreenTensorCalculatorAtoOLevel(
//                _logger,
//                _profiler,
//                _model,
//                _memoryProvider,
//                _fourierTransform,
//                _numberOfhankels)
//            { PreCalculatedScalars = scalars };

//            return calc.Calculate(FieldToField.J2H, level);
//        }


//        private GreenTensorAlongZ CalculateNewGreenTensorAtoOLevelMagnetic(GreenScalars scalars, ObservationLevel level)
//        {
//            var calc = new GreenTensorCalculatorImpl(_logger, _model, _memoryProvider, _fourierTransform);
//            return calc.CalculateAtoOMagnetic(scalars, level, MemoryLayoutOrder.AlongLateral);
//        }

//        #endregion

//        #region Support
//        private void WriteStatistics(List<DateTime> times, string message)
//        {
//            var spans = new List<TimeSpan>();

//            for (int i = 1; i < times.Count; i++)
//                spans.Add(times[i] - times[i - 1]);


//            var min = spans.Min().TotalSeconds;
//            var max = spans.Max().TotalSeconds;
//            var avg = spans.Average(ts => ts.TotalSeconds);

//            _logger.WriteStatus("{0}: min = [{1}], avg = [{2}], max = [{3}]", message.PadRight(9), min, avg, max);
//        }

//        private void DumpGreenTensor(IGreenTensor gt1, GreenTensorAlongZ gt2)
//        {
//            var tr = 0;
//            var rc = 1;

//            _logger.WriteStatus("[tr:{0},rc:{1}]", tr, rc);
//            DumpGreenTensor("XZ", gt1[tr, rc].XZ, gt2, tr, rc);
//        }

//        private void DumpGreenTensor(string component, IComponentAccessor ca1, GreenTensorAlongZ gtAlongZ, int tr, int rc)
//        {
//            var ca2 = gtAlongZ[component];

//            for (int i = 0; i < ca1.Nx; i++)
//                for (int j = 0; j < ca1.Ny; j++)
//                {
//                    var val1 = ca1[i, j];
//                    var val2 = ca2.GetAlongLateral(i, j, tr, rc);

//                    if (ValuesAreNotEqual(val1, val2))
//                        Console.ForegroundColor = ConsoleColor.Red;

//                    Console.WriteLine($"[{i},{j}]: {val1: 00.0000000000000;-00.0000000000000;                0} " +
//                                             $"--- {val2: 00.0000000000000;-00.0000000000000;                0}");

//                    Console.ResetColor();
//                }
//        }

//        private void DumpGreenTensor(string component, IComponentAccessor ca)
//        {
//            for (int i = 0; i < ca.Nx; i++)
//                for (int j = 0; j < ca.Ny; j++)
//                    _logger.WriteStatus("{2} \t[{0},{1}] = {3}", i, j, component, ca[i, j]);
//        }

//        private void DumpGreenTensor(string component, GreenTensorAlongZ gtAlongZ, int tr, int rc)
//        {
//            var ca = gtAlongZ[component];

//            for (int i = 0; i < _model.LateralDimensions.Nx * 2; i++)
//                for (int j = 0; j < _model.LateralDimensions.Ny * 2; j++)
//                    _logger.WriteStatus("{2} \t[{0},{1}] = {3}", i, j, component, ca.GetAlongLateral(i, j, tr, rc));
//            //_logger.WriteStatus("{2} \t[{0},{1}] = {3}", i, j, component, ca.GetAlongVertical(i, j, tr, rc));
//        }

//        private bool CompareGreenTensors(IGreenTensor gt1, GreenTensorAlongZ gt2, int nTr, int nRc)
//        {
//            _logger.WriteStatus("Compare:");

//            for (int tr = 0; tr < nTr; tr++)
//                for (int rc = 0; rc < nRc; rc++)
//                {
//                    var gt1c = gt1[tr, rc];

//                    _logger.WriteStatus($"[{tr},{rc}]:...");

//                    if (!CompareGreenTensors("XX", gt1c.XX, gt2, tr, rc))
//                        return false;
//                    if (!CompareGreenTensors("YY", gt1c.YY, gt2, tr, rc))
//                        return false;

//                    if (!CompareGreenTensors("ZZ", gt1c.ZZ, gt2, tr, rc))
//                        return false;

//                    if (!CompareGreenTensors("XY", gt1c.XY, gt2, tr, rc))
//                        return false;

//                    if (!CompareGreenTensors("XZ", gt1c.XZ, gt2, tr, rc))
//                        return false;
//                    if (!CompareGreenTensors("YZ", gt1c.YZ, gt2, tr, rc))
//                        return false;

//                    if (!CompareGreenTensors("ZX", gt1c.ZX, gt2, tr, rc))
//                        return false;
//                    if (!CompareGreenTensors("ZY", gt1c.ZY, gt2, tr, rc))
//                        return false;

//                    _logger.WriteStatus("\t\t\t GOOD");
//                }

//            return true;
//        }

//        private bool CompareGreenTensorsMagnetic(IGreenTensor gt1, GreenTensorAlongZ gt2, int nTr, int nRc)
//        {
//            _logger.WriteStatus("Compare:");

//            for (int tr = 0; tr < nTr; tr++)
//                for (int rc = 0; rc < nRc; rc++)
//                {
//                    var gt1c = gt1[tr, rc];

//                    _logger.WriteStatus($"[{tr},{rc}]:...");

//                    if (!CompareGreenTensors("XX", gt1c.XX, gt2, tr, rc))
//                        return false;
//                    if (!CompareGreenTensors("XY", gt1c.XY, gt2, tr, rc))
//                        return false;
//                    if (!CompareGreenTensors("YX", gt1c.YX, gt2, tr, rc))
//                        return false;
//                    if (!CompareGreenTensors("XZ", gt1c.XZ, gt2, tr, rc))
//                        return false;
//                    if (!CompareGreenTensors("YZ", gt1c.YZ, gt2, tr, rc))
//                        return false;
//                    if (!CompareGreenTensors("ZX", gt1c.ZX, gt2, tr, rc))
//                        return false;
//                    if (!CompareGreenTensors("ZY", gt1c.ZY, gt2, tr, rc))
//                        return false;

//                    _logger.WriteStatus("\t\t\t GOOD");
//                }

//            return true;
//        }

//        private bool CompareGreenTensors(string component, IComponentAccessor ca1, GreenTensorAlongZ ca2, int tr, int rc)
//        {

//            for (int i = 0; i < ca1.Nx; i++)
//                for (int j = 0; j < ca1.Ny; j++)
//                {
//                    var gt1 = ca1[i, j];
//                    var gt2 = ca2[component].GetAlongLateral(i, j, tr, rc);

//                    if (ValuesAreNotEqual(gt1, gt2))
//                    {
//                        _logger.WriteError($"{component}: i={i}, j={j} == {gt1} {gt2}");
//                        return false;
//                    }
//                }

//            return true;
//        }

//        private static bool ValuesAreNotEqual(Complex val1, Complex val2)
//        {
//            const double epsilon = 1E-9;


//            if (double.IsNaN(val1.Real))
//                return true;

//            if (double.IsNaN(val1.Imaginary))
//                return true;

//            if (double.IsNaN(val2.Real))
//                return true;

//            if (double.IsNaN(val2.Imaginary))
//                return true;

//            return Abs((val1.Real - val2.Real)) > epsilon ||
//                   Abs((val1.Imaginary - val2.Imaginary)) > epsilon;
//        }

//        #endregion
//    }
//}
