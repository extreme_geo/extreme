﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;

namespace ExportGreen
{
    public class GnuplotExporter
    {
        private const string ScriptsSubdir = @"scripts";
        private const string ImagesSubdir = @"images";

        public static void ExportAll(string basePath)
        {
            var dirs = Directory.GetDirectories(basePath);

            var scriptFiles = new List<string>();

            foreach (var dir in dirs)
                scriptFiles.AddRange(CreateScripts(dir));

            Parallel.ForEach(scriptFiles, RunScript);

            //foreach (var scriptFile in scriptFiles)
            //    RunScript(scriptFile);
        }

        private static void RunScript(string scriptFile)
        {
            Console.WriteLine(scriptFile);

            var psi = new ProcessStartInfo()
            {
                CreateNoWindow = true,
                FileName = "gnuplot.exe",
                Arguments = scriptFile,
                WorkingDirectory = Path.GetDirectoryName(scriptFile)
            };

            Process.Start(psi);
        }

        private static List<string> CreateScripts(string dir)
        {
            var scriptsDir = CreateScriptsSubdirectory(dir);
            CreateImagesSubdirectory(dir);

            var files = GetAllResultFiles(dir);

            var scriptFiles = new List<string>();

            foreach (var file in files)
            {
                scriptFiles.Add(CreateScriptForFile(dir, scriptsDir, file));
                scriptFiles.Add(CreateProfileScriptForFile(dir, scriptsDir, file));
            }

            return scriptFiles;
        }

        private static string CreateProfileScriptForFile(string dir, string scriptsDir, string file)
        {
            int y = 50;

            var name = Path.GetFileNameWithoutExtension(file);
            var dirName = new DirectoryInfo(dir).Name;

            var title = string.Format("profileX{2}_{0}_{1}", dirName, name, y);
            var output = Path.Combine(@"..", ImagesSubdir, title + ".png");

            var script = new GnuplotScriptProfile(title, Path.GetFileName(file), output, y);

            var scriptPath = Path.Combine(scriptsDir, name + "_profileX.plt");

            File.WriteAllText(scriptPath, script.TransformText());

            return scriptPath;
        }

        private static string CreateScriptForFile(string dir, string scriptsDir, string file)
        {
            var name = Path.GetFileNameWithoutExtension(file);
            var dirName = new DirectoryInfo(dir).Name;

            var title = string.Format("{0}_{1}", dirName, name);
            var output = Path.Combine(@"..", ImagesSubdir, title + ".png");

            var script = new GnuplotScript(title, Path.GetFileName(file), output);

            var scriptPath = Path.Combine(scriptsDir, name + ".plt");

            File.WriteAllText(scriptPath, script.TransformText());

            return scriptPath;
        }

        private static string[] GetAllResultFiles(string dir)
        {
            return Directory.GetFiles(dir, "*.dat", SearchOption.TopDirectoryOnly);
        }

        private static string CreateImagesSubdirectory(string dir)
        {
            return Directory.CreateDirectory(Path.Combine(dir, ImagesSubdir))
                .FullName;
        }

        private static string CreateScriptsSubdirectory(string dir)
        {
            return Directory.CreateDirectory(Path.Combine(dir, ScriptsSubdir)).
                FullName;
        }
    }
}
