#include <mkl.h>

#ifdef WINDOWS
#define DllExport __declspec(dllexport) 
#else
#define DllExport 
#endif

namespace NativeMemory
{
	extern "C"
	{
		DllExport void* Malloc(size_t size, int alignment)
		{
			return mkl_malloc(size, alignment);
		}

		DllExport void Free(void* ptr)
		{
			return mkl_free(ptr);
		}
	}
}