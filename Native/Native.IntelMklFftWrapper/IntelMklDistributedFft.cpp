#ifdef CLUSTER_FFT

#include <iostream>
#include <complex>
#include <cstdlib>

#include <mkl_cdft.h>

#ifdef WINDOWS
	#define DllExport __declspec(dllexport) 
#else
	#define DllExport 
#endif

using namespace std;

namespace NativeDistributed
{
	void CheckStatus(MKL_LONG status)
	{
		if (status != 0)
		{
			cout << "status: " << status << "\n" << DftiErrorMessage(status);
			std::exit(status);
		}
	}

	extern "C"
	{
		DllExport DFTI_DESCRIPTOR_DM_HANDLE CreateDistributedDftiDescriptor(MPI_Comm comm, MKL_INT nx, MKL_INT ny)
		{
			MKL_LONG fftDimension[2];

			fftDimension[0] = (MKL_LONG)nx;
			fftDimension[1] = (MKL_LONG)ny;

			DFTI_DESCRIPTOR_DM_HANDLE handler = 0;

			// (MPI_Comm)MPI_Comm_c2f (comm) -- this is realy fucking hell with Open MPI and intel cluster FFT
			// It needs static linking on brutus to work (see makefile)
			CheckStatus(DftiCreateDescriptorDM((MPI_Comm)MPI_Comm_c2f (comm), &handler, DFTI_DOUBLE, DFTI_COMPLEX, 2, &fftDimension[0]));
			CheckStatus(DftiCommitDescriptorDM(handler));

			return handler;
		}

		DllExport void FreeDistributedDftiDescriptor(DFTI_DESCRIPTOR_DM_HANDLE  *handler)
		{
			MKL_LONG status = DftiFreeDescriptorDM(handler);

			CheckStatus(status);
		}

		DllExport void PerformDistributedForwardFft(DFTI_DESCRIPTOR_DM_HANDLE handler, void* srcAndDst)
		{
			MKL_LONG status = DftiComputeForwardDM(handler, srcAndDst);

			CheckStatus(status);
		}

		DllExport void PerformDistributedBackwardFft(DFTI_DESCRIPTOR_DM_HANDLE handler, void* srcAndDst)
		{
			MKL_LONG status = DftiComputeBackwardDM(handler, srcAndDst);

			CheckStatus(status);
		}

		DllExport int GetLocalSize(DFTI_DESCRIPTOR_DM_HANDLE handler)
		{
			int param;
			CheckStatus(DftiGetValueDM(handler, CDFT_LOCAL_SIZE, &param));
			return param;
		}

		DllExport int GetLocalNx(DFTI_DESCRIPTOR_DM_HANDLE handler)
		{
			int param;
			CheckStatus(DftiGetValueDM(handler, CDFT_LOCAL_NX, &param));
			return param;
		}

		DllExport int GetLocalXStart(DFTI_DESCRIPTOR_DM_HANDLE handler)
		{
			int param;
			CheckStatus(DftiGetValueDM(handler, CDFT_LOCAL_X_START, &param));
			return param;
		}
	}
}
#endif 