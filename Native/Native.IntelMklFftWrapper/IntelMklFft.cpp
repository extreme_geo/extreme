#include <iostream>
#include <cstdlib>

#include <mkl.h>

#ifdef WINDOWS
#define DllExport __declspec(dllexport) 
#else
#define DllExport 
#endif

using namespace std;

namespace Native
{
	void CheckStatus(MKL_LONG status)
	{
		if (status != 0)
		{
			cout << DftiErrorMessage(status);
			std::exit(status);
		}
	}

	extern "C"
	{
		DllExport void FreeDftiDescriptor(DFTI_DESCRIPTOR_HANDLE *handler)
		{
			MKL_LONG status = DftiFreeDescriptor(handler);

			CheckStatus(status);
		}

		DFTI_DESCRIPTOR_HANDLE CreateDftiDescriptor(DFTI_CONFIG_VALUE precision, MKL_INT nx, MKL_INT ny)
		{
			MKL_LONG fftDimension[2];

			fftDimension[0] = (MKL_LONG)nx;
			fftDimension[1] = (MKL_LONG)ny;

			DFTI_DESCRIPTOR_HANDLE handler = 0;

			CheckStatus(DftiCreateDescriptor(&handler, precision, DFTI_COMPLEX, 2, &fftDimension[0]));
			CheckStatus(DftiSetValue(handler, DFTI_PLACEMENT, DFTI_INPLACE));
			CheckStatus(DftiSetValue(handler, DFTI_FORWARD_SCALE, 1.0));
			//CheckStatus(DftiSetValue(handler, DFTI_BACKWARD_SCALE, 1.0 / (nx*ny)));

			CheckStatus(DftiCommitDescriptor(handler));

			return handler;
		}

		DllExport DFTI_DESCRIPTOR_HANDLE CreateDftiDescriptorDouble(MKL_INT nx, MKL_INT ny)
		{
			return CreateDftiDescriptor(DFTI_DOUBLE, nx, ny);
		}

		DllExport void PerformForwardFft(DFTI_DESCRIPTOR_HANDLE handler, void* srcAndDst)
		{
			MKL_LONG status = DftiComputeForward(handler, srcAndDst);

			CheckStatus(status);
		}

		DllExport void PerformBackwardFft(DFTI_DESCRIPTOR_HANDLE handler, void* srcAndDst)
		{
			MKL_LONG status = DftiComputeBackward(handler, srcAndDst);

			CheckStatus(status);
		}
	}
}