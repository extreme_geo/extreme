#include "lbfgs.h"

#ifdef WINDOWS
#define DllExport __declspec(dllexport) 
#else
#define DllExport 
#endif



DllExport void InitLbfgsParameters(lbfgs_parameter_t *param)
{
	lbfgs_parameter_init(param);
}

DllExport int RunLbfgs(
	int n,
	lbfgsfloatval_t *x,
	lbfgsfloatval_t *ptr_fx,
	lbfgs_evaluate_t proc_evaluate,
	lbfgs_progress_t proc_progress,
	void *instance,
	lbfgs_parameter_t *_param,
	lbfgs_vec_alloc vec_alloc,
	lbfgs_vec_free vec_free)
{
	return lbfgs(n, x, ptr_fx, proc_evaluate, proc_progress, instance, _param, vec_alloc, vec_free);
}

//DllExport void ClearBuffer(double* buffer, size_t length)
//{
//	memset(buffer, 0, length);
//}
