
DST=$(HOME)/bin/extrEMe
DEV_DST=$(HOME)/bin/dev/extrEMe

INV_PATH=./EndUser/ExtreEmJoyMt/bin/x64/Release
FWD_PATH=./EndUser/ExtremeMt/bin/x64/Release
FWD2INV_PATH=./EndUser/ForwardToInversion/bin/x64/Release

user: 

	mkdir -p ${DST}
	$(MAKE) -C Native  
	xbuild ExtremeReduced.sln  /p:Platform="x64" /p:Configuration="Release"

	cp $(INV_PATH)/*.exe $(DST)
	cp $(INV_PATH)/*.dll $(DST)
	cp $(INV_PATH)/*.so $(DST)


	cp $(FWD_PATH)/*.exe $(DST)
	cp $(FWD_PATH)/*.dll $(DST)
	cp $(FWD_PATH)/*.so $(DST)

	cp $(FWD2INV_PATH)/*.exe $(DST)
	cp $(FWD2INV_PATH)/*.dll $(DST)
develop: 

	mkdir -p ${DEV_DST}
	$(MAKE) -C Native  
	 xbuild Extreme.sln  /p:Platform="x64" /p:Configuration="Release"

	cp $(INV_PATH)/*.exe $(DEV_DST)
	cp $(INV_PATH)/*.dll $(DEV_DST)
	cp $(INV_PATH)/*.so $(DEV_DST)


	cp $(FWD_PATH)/*.exe $(DEV_DST)
	cp $(FWD_PATH)/*.dll $(DEV_DST)
	cp $(FWD_PATH)/*.so $(DEV_DST)

	cp $(FWD2INV_PATH)/*.exe $(DEV_DST)
	cp $(FWD2INV_PATH)/*.dll $(DEV_DST)
clean:

	$(MAKE) -C Native clean 
	 xbuild Extreme.sln  /t:Clean 
	rm $(FWD_PATH)/*.dll
	rm $(INV_PATH)/*.dll
	rm $(FWD_PATH)/*.exe
	rm $(INV_PATH)/*.exe
