﻿using System;
using System.Collections.Generic;
using System.Linq;

using Extreme.Core;
using Extreme.Cartesian.Core;
using Extreme.Cartesian.Forward;
using Extreme.Cartesian.Green.Tensor;
using Extreme.Cartesian.Model;
using Extreme.Cartesian.Magnetotellurics;

using Extreme.Inversion.Adjoint;
using Extreme.Inversion.Lbfgs;
using Extreme.Inversion.Observed;
using Extreme.Inversion.Project;
using Extreme.Inversion.Logger;


using Extreme.Parallel;

using static System.Math;
using UnsafeNativeMethods = Extreme.Cartesian.Forward.UnsafeNativeMethods;
using System.Xml.Schema;

namespace Extreme.Inversion
{
    public class MtInversionSolver : IDisposable
    {
        private readonly ILogger _logger;
        private readonly INativeMemoryProvider _memoryProvider;
        private readonly Mt3DForwardSolver _solver;
        private readonly AdjointSolver _adjointSolver;
        private readonly InversionProject _project;


        private AnomalyCurrent _fullFieldsAtAnomaly1;
        private AnomalyCurrent _fullFieldsAtAnomaly2;

        private GreenTensor _aToAGreenTensor;
        private AdjointCalculator _adjointCalculator;

        private Mpi _mpi;

        private ObservedContainer _observedContainer;
        private CartesianModel _startModel;
        private OmegaModel _currentModel;

        private StabilizerCalculator _stabilizerCalculator;
        private InversionDomain _inversionDomain;

        private AnomalyCurrent _sourceE;
        private AnomalyCurrent _sourceH;

        private ChiCache _firstFrequencyCache;
        private ChiCache _currentFrequencyCache;

        private Polarization _currentAdjointSource;

        private readonly LbfgsSolver _lbfgsSolver;

        private int _evaluationIterationCounter = 0;
        private double _currentFrequency = 0;
        private bool _skipSolvingWhenStabObjFuncGrows = false;
        private double _previousObjectiveFunction;


        private double Lambda => _project.InversionSettings.Stabilizer.Lambda;

        public MtInversionSolver(ILogger logger, INativeMemoryProvider memoryProvider, InversionProject project, Mpi mpi)
        {
            if (logger == null) throw new ArgumentNullException(nameof(logger));
            if (memoryProvider == null) throw new ArgumentNullException(nameof(memoryProvider));
            if (project == null) throw new ArgumentNullException(nameof(project));
            if (mpi == null) throw new ArgumentNullException(nameof(mpi));

            _logger = logger;
            _memoryProvider = memoryProvider;
            _project = project;
            _mpi = mpi;

            _solver = new Mt3DForwardSolver(logger, memoryProvider, project.ForwardSettings);
            _solver.AtoOGreenTensorECalculated += SolverAtoOGreenTensorECalculated;
            _solver.AtoOGreenTensorHCalculated += SolverAtoOGreenTensorHCalculated;
            _solver.AtoAGreenTensorCalculated += SolverAtoAGreenTensorCalculated;
            _solver.EScatteredCalculated += SolverEScatteredCalculated;
            _solver.CieSolverFinished += ForwardSolver_CieSolverFinished;
            _solver.CieSolverStarted += ForwardSolver_CieSolverStarted;

            _adjointSolver = new AdjointSolver(_logger, _memoryProvider, project.ForwardSettings);
            _adjointSolver.CieSolverStarted += AdjointSolver_CieSolverStarted;
            _adjointSolver.CieSolverFinished += AdjointSolver_CieSolverFinished;
            _adjointSolver.WithMpi(_mpi);

            _lbfgsSolver = new LbfgsSolver(_memoryProvider, _logger, _mpi);
            _lbfgsSolver.EvaluateRequest += LbfgsSolverEvaluateRequest;
            _lbfgsSolver.Progress += lbfgsSolver_Progress;
        }

        public event EventHandler<InversionDomainCreatedEventArgs> InversionDomainCreated;
        public event EventHandler<InversionIterationCompleteEventArgs> InversionIterationComplete;

        private void OnInversionDomainCreated(InversionDomainCreatedEventArgs e)
        {
            InversionDomainCreated?.Invoke(this, e);
        }

        protected virtual void OnInversionIterationComplete(InversionIterationCompleteEventArgs e)
        {
            InversionIterationComplete?.Invoke(this, e);
        }

        #region Event Handlers

        #region Chi

        private void AdjointSolver_CieSolverFinished(object sender, CieSolverFinishedEventArgs e)
        {
            var freqs = _project.Extreme.Frequencies.ToList();
            var index = freqs.IndexOf(_currentFrequency);

            if (index == 0)
            {
                if (_currentAdjointSource == Polarization.X)
                    e.Chi.CopyToAnomalyCurrent(_firstFrequencyCache.AdjointPol1);

                if (_currentAdjointSource == Polarization.Y)
                    e.Chi.CopyToAnomalyCurrent(_firstFrequencyCache.AdjointPol2);
            }

            if (_currentAdjointSource == Polarization.X)
                e.Chi.CopyToAnomalyCurrent(_currentFrequencyCache.AdjointPol1);

            if (_currentAdjointSource == Polarization.Y)
                e.Chi.CopyToAnomalyCurrent(_currentFrequencyCache.AdjointPol2);
        }


        private void AdjointSolver_CieSolverStarted(object sender, CieSolverStartedEventArgs e)
        {
            var freqs = _project.Extreme.Frequencies.ToList();
            var index = freqs.IndexOf(_currentFrequency);

            if (_evaluationIterationCounter == 0 && index == 0)
                return;

            if (index == 0)
            {
                if (_currentAdjointSource == Polarization.X)
                    _firstFrequencyCache.AdjointPol1.CopyToAnomalyCurrent(e.InitialGuess);

                if (_currentAdjointSource == Polarization.Y)
                    _firstFrequencyCache.AdjointPol2.CopyToAnomalyCurrent(e.InitialGuess);
            }
            else
            {
                if (_currentAdjointSource == Polarization.X)
                    _currentFrequencyCache.AdjointPol1.CopyToAnomalyCurrent(e.InitialGuess);

                if (_currentAdjointSource == Polarization.Y)
                    _currentFrequencyCache.AdjointPol2.CopyToAnomalyCurrent(e.InitialGuess);
            }
        }

        private void ForwardSolver_CieSolverFinished(object sender, CieSolverFinishedEventArgs e)
        {
            var freqs = _project.Extreme.Frequencies.ToList();
            var index = freqs.IndexOf(_currentFrequency);

            if (index == 0)
            {
                if (_solver.CurrentSource == Polarization.X)
                    e.Chi.CopyToAnomalyCurrent(_firstFrequencyCache.ForwardPol1);

                if (_solver.CurrentSource == Polarization.Y)
                    e.Chi.CopyToAnomalyCurrent(_firstFrequencyCache.ForwardPol2);
            }

            if (_solver.CurrentSource == Polarization.X)
                e.Chi.CopyToAnomalyCurrent(_currentFrequencyCache.ForwardPol1);

            if (_solver.CurrentSource == Polarization.Y)
                e.Chi.CopyToAnomalyCurrent(_currentFrequencyCache.ForwardPol2);
        }


        private void ForwardSolver_CieSolverStarted(object sender, CieSolverStartedEventArgs e)
        {
            var freqs = _project.Extreme.Frequencies.ToList();
            var index = freqs.IndexOf(_currentFrequency);

            if (_evaluationIterationCounter == 0 && index == 0)
                return;

            if (index == 0)
            {
                if (_solver.CurrentSource == Polarization.X)
                    _firstFrequencyCache.ForwardPol1.CopyToAnomalyCurrent(e.InitialGuess);

                if (_solver.CurrentSource == Polarization.Y)
                    _firstFrequencyCache.ForwardPol2.CopyToAnomalyCurrent(e.InitialGuess);
            }
            else
            {
                if (_solver.CurrentSource == Polarization.X)
                    _currentFrequencyCache.ForwardPol1.CopyToAnomalyCurrent(e.InitialGuess);

                if (_solver.CurrentSource == Polarization.Y)
                    _currentFrequencyCache.ForwardPol2.CopyToAnomalyCurrent(e.InitialGuess);
            }
        }

        #endregion

        private void lbfgsSolver_Progress(object sender, ProgressEventArgs e)
        {
            _logger.WriteStatus($"Inversion iteration {e.IterationCount} complete. gnorm / xnorm = {e.GNorm / e.XNorm}");

            OnInversionIterationComplete(new InversionIterationCompleteEventArgs(e));
        }

        private unsafe void SolverEScatteredCalculated(object sender, EScatteredCalculatedEventArgs e)
        {
            var fullFieldAtAnomaly = AnomalyCurrent.AllocateNewLocalSize(_memoryProvider, _currentModel);
            long length = fullFieldAtAnomaly.GetFullLength();
            UnsafeNativeMethods.AddElementwise(length, e.NormalFieldAtAnomaly.Ptr, e.EScattered.Ptr,
                fullFieldAtAnomaly.Ptr);

            if (_solver.CurrentSource == Polarization.X)
                _fullFieldsAtAnomaly1 = fullFieldAtAnomaly;

            if (_solver.CurrentSource == Polarization.Y)
                _fullFieldsAtAnomaly2 = fullFieldAtAnomaly;
        }

        private void SolverAtoAGreenTensorCalculated(object sender, AtoAGreenTensorCalculatedEventArgs e)
        {
            _aToAGreenTensor = e.GreenTensor;
        }

        private void SolverAtoOGreenTensorECalculated(object sender, GreenTensorCalculatedEventArgs e)
        {
            _adjointSolver.AddGreenTensorE(e.Level, e.GreenTensor);
            e.SupressGreenTensorDisposal = true;
        }

        private void SolverAtoOGreenTensorHCalculated(object sender, GreenTensorCalculatedEventArgs e)
        {
            _adjointSolver.AddGreenTensorH(e.Level, e.GreenTensor);
            e.SupressGreenTensorDisposal = true;
        }

        #endregion


        public unsafe void SolveSlave(CartesianModel startModel, ObservedContainer observedContainer)
        {
            if (startModel == null) throw new ArgumentNullException(nameof(startModel));
            if (observedContainer == null) throw new ArgumentNullException(nameof(observedContainer));
            _startModel = startModel;
            _observedContainer = observedContainer;

            _adjointCalculator = new AdjointCalculator(startModel.Anomaly.LocalSize, startModel.LateralDimensions);
            _stabilizerCalculator = new StabilizerCalculator(_startModel, _project.InversionSettings.Stabilizer);

            CreateInversionDomain(_project.InversionSettings.Domain);
            AllocateBuffers();
            SetUpForwardSolver();

            BuildInversionDomainModel(startModel, _inversionDomain);

            WaitForCommandFromMaster();
        }


        private const int InversionProcessFinished = 0;
        private const int NextInverseIteration = 1;

        private unsafe void WaitForCommandFromMaster()
        {
            var currentM = _memoryProvider.AllocateDouble(_inversionDomain.Length);
            var gradient = _memoryProvider.AllocateDouble(_inversionDomain.Length);

			for(;;)
            {
                int command = _mpi.BroadCast(Mpi.CommWorld, 0, 0);

				if (command == InversionProcessFinished)
                    break;

                _mpi.BroadCast(Mpi.CommWorld, 0, currentM, _inversionDomain.Length);
                var sigmaM = RiseFromAshes(currentM, _inversionDomain);


                double stabOmega = 0;

                if (_startModel.Anomaly.LocalSize.Nx != 0)
                {

                    _stabilizerCalculator.CalculateDerivatives(sigmaM, GetShiftX(), currentM, _inversionDomain);
                    stabOmega = _stabilizerCalculator.CalculateDefaultStabilizer(sigmaM);
                }

                _mpi.Reduce(Mpi.CommWorld, &stabOmega, &stabOmega, 1);

                var stabGradient = _stabilizerCalculator.CalculateDefaultStabilizerGradient(sigmaM);

                ClearFullGradient(gradient);
                AppendStabGradientToInversionGradient(stabGradient, gradient);

                var sigma = ConvertSigmaMToSigma(sigmaM);

                var residualSqr = CalculateModelGradientAndResidualSqr(sigma, gradient);


                _mpi.Reduce(Mpi.CommWorld, gradient, gradient, _inversionDomain.Length);

                _evaluationIterationCounter ++;
            }


            _memoryProvider.Release(gradient);
            _memoryProvider.Release(currentM);
        }

        public void Solve(CartesianModel startModel, ObservedContainer observedContainer)
        {
            if (startModel == null) throw new ArgumentNullException(nameof(startModel));
            if (observedContainer == null) throw new ArgumentNullException(nameof(observedContainer));

            _logger.WriteStatus("\n\n\t\t\tStarting MT Inversion solver");

            _evaluationIterationCounter = 0;

            _startModel = startModel;
            _observedContainer = observedContainer;

            _adjointCalculator = new AdjointCalculator(startModel.Anomaly.LocalSize, startModel.LateralDimensions);
            _stabilizerCalculator = new StabilizerCalculator(_startModel, _project.InversionSettings.Stabilizer);

            CreateInversionDomain(_project.InversionSettings.Domain);
            AllocateBuffers();
            SetUpForwardSolver();

            var result = SetUpAndRunLbfgs(_project.InversionSettings.Lbfgs);

            if (_mpi.IsParallel)
				_mpi.BroadCast(Mpi.CommWorld, 0, InversionProcessFinished);

            _logger.WriteStatus($"Lbfgs result = {result}");
        }

        private void SetUpForwardSolver()
        {
            _solver.WithMpi(_mpi)
                   .With(_project.Extreme.ObservationLevels);
        }

        private void AllocateBuffers()
        {
            _firstFrequencyCache = ChiCache.Allocate(_memoryProvider, _startModel);
            _currentFrequencyCache = ChiCache.Allocate(_memoryProvider, _startModel);

            _sourceE = AnomalyCurrent.AllocateNewOneLayer(_memoryProvider, _startModel);
            _sourceH = AnomalyCurrent.AllocateNewOneLayer(_memoryProvider, _startModel);
        }

        private unsafe LbfgsResult SetUpAndRunLbfgs(LbfgsExternalParameters lbfgs)
        {
            _lbfgsSolver.Parameters.M = lbfgs.Memory;
            _lbfgsSolver.Parameters.Epsilon = lbfgs.Tolerance;
            _lbfgsSolver.Parameters.LineSearch = lbfgs.LineSearchType;
            _lbfgsSolver.Parameters.MaxLinesearch = lbfgs.LineSearchLength;

            _previousObjectiveFunction = double.MaxValue;
            _skipSolvingWhenStabObjFuncGrows = false;//_lbfgsSolver.Parameters.LineSearch != LineSearchType.MoreThuente;

            var currentM = BuildInversionDomainModel(_startModel, _inversionDomain);
            return _lbfgsSolver.Run(currentM, _inversionDomain.Length);
        }


        private unsafe double* BuildInversionDomainModel(CartesianModel startModel, InversionDomain domain)
        {
            var currentM = _memoryProvider.AllocateDouble(domain.Length);

            for (int i = 0; i < domain.Length; i++)
                currentM[i] = -1;

            var nx = startModel.Anomaly.LocalSize.Nx;
            var ny = startModel.Anomaly.LocalSize.Ny;
            var nz = startModel.Nz;
			int shift = GetShiftX();
            for (int i = 0; i < nx; i++)
                for (int j = 0; j < ny; j++)
                    for (int k = 0; k < nz; k++)
                    {
                        var index = domain.Mask[i+shift, j, k];
						currentM[index] = (startModel.Anomaly.Sigma[i, j, k]);
                    }

            if (_mpi.IsParallel)
            {
                if (_mpi.IsMaster)
                {
                    var tmp = _memoryProvider.AllocateDouble(domain.Length);
					_mpi.LogicalReduce(Mpi.CommWorld, currentM, tmp, domain.Length);
					for (int k = 0; k < domain.Length; k++)
						currentM [k] = Math.Log (tmp [k]);
                    _memoryProvider.Release(tmp);
                    return currentM;
                }

                _mpi.LogicalReduce(Mpi.CommWorld, currentM, currentM, domain.Length);
				_memoryProvider.Release(currentM);
                return null;
            }
			for (int k = 0; k < domain.Length; k++)
				currentM [k] = Math.Log (currentM [k]);
            return currentM;
        }

        private void CreateInversionDomain(InversionDomainParameters domain)
        {
            _inversionDomain = new InversionDomain(_startModel);
            _inversionDomain.GroupBy(domain.Nx, domain.Ny, domain.Nz);

            OnInversionDomainCreated(new InversionDomainCreatedEventArgs(_inversionDomain));
        }


        private unsafe void LbfgsSolverEvaluateRequest(object sender, EvaluateRequestEventArgs e)
        {
            _logger.WriteStatus($"        Evaluate request {_evaluationIterationCounter}");
			var gradient = _memoryProvider.AllocateDouble(_inversionDomain.Length);
            if (_mpi.IsParallel)
				_mpi.BroadCast(Mpi.CommWorld, 0, NextInverseIteration);

            var sigmaM = BuildSigmaM(e);
            _stabilizerCalculator.CalculateDerivatives(sigmaM, GetShiftX(), e.M, _inversionDomain);
            var stabOmega = _stabilizerCalculator.CalculateDefaultStabilizer(sigmaM);

            if (_mpi.IsParallel)
            {
                double tmp = 0;
                _mpi.Reduce(Mpi.CommWorld, &stabOmega, &tmp, 1);
                stabOmega = tmp;
            }

            e.ObjectiveFunction = Lambda * stabOmega;

            // if (stabOmega > prevobj , то не надо ничего считать, а надо просто вернуть
            // stabOmega . В противном случае надо посчитать всё и запомнить текущий e.Objective в prevobj
            if (_skipSolvingWhenStabObjFuncGrows && e.ObjectiveFunction >= _previousObjectiveFunction)
            {
                _logger.WriteWarning($"Lambda * stabOmega [{e.ObjectiveFunction}] >= " +
                                     $"previous Objective Function [{_previousObjectiveFunction}],  " +
                                     $"skip model gradient calculation");
                _evaluationIterationCounter++;
                return;
            }

            var stabGradient = _stabilizerCalculator.CalculateDefaultStabilizerGradient(sigmaM);

			ClearFullGradient(gradient);
            AppendStabGradientToInversionGradient(stabGradient, gradient);


            var sigma = ConvertSigmaMToSigma(sigmaM);
            var residualSqr = CalculateModelGradientAndResidualSqr(sigma, gradient);

						


			if (_mpi.IsParallel){
				_mpi.Reduce(Mpi.CommWorld, gradient, e.Gradient, _inversionDomain.Length);
			}else{
				for(int i=0;i<_inversionDomain.Length;i++) e.Gradient[i]=gradient[i];
			}
			

            e.ObjectiveFunction += residualSqr;
			var gNorm = CalculateGNorm(e.Gradient, _inversionDomain.Length);

            _logger.WriteWarning($"gnorm = {gNorm}, residualSqr = {residualSqr}, _lambda * stabOmega = {Lambda * stabOmega}");

            _evaluationIterationCounter++;
            _previousObjectiveFunction = e.ObjectiveFunction;
			_memoryProvider.Release(gradient);
        }

        private unsafe double[,,] BuildSigmaM(EvaluateRequestEventArgs e)
        {
            var values = e.M;
            var sigmaM = RiseFromAshes(values, _inversionDomain);

            if (_mpi.IsParallel)
                _mpi.BroadCast(Mpi.CommWorld, 0, values, _inversionDomain.Length);

            return sigmaM;
        }

        private unsafe double CalculateGNorm(double* gradient, int length)
        {
            var norm = 0.0;

            for (int i = 0; i < length; i++)
            {
                var g = gradient[i];
                norm += g * g;
            }

            norm = Sqrt(norm);

            return norm;
        }

        private unsafe double CalculateModelGradientAndResidualSqr(double[,,] sigma, double* gradient)
        {
            double residualSqr = 0;
            var freqs = _project.Extreme.Frequencies.ToArray();

            for (int i = 0; i < freqs.Length; i++)
            {
                _currentFrequency = freqs[i];
                _logger.WriteStatus($"\t\tFrequency {_currentFrequency }, {i + 1} of {freqs.Length}");

                _logger.WriteStatus($"Forward MT");

               
				_currentModel = OmegaModelBuilder.BuildOmegaModel(_startModel, sigma, _currentFrequency);

                using (var rc = _solver.SolveWithoutGather(_currentModel))
                {

                    
                        residualSqr += CalculateResidualSqr(_currentFrequency, _observedContainer, rc);
                    ///


                    CalculateGradientThroughAdjoint(sigma, gradient, _currentFrequency, rc);
                }
            }

            return residualSqr;
        }

        private unsafe void CalculateGradientThroughAdjoint(double[,,] sigma, double* gradient, double frequency, ResultsContainer rc)
        {
            _logger.WriteStatus($"Adjoint calculation");
            _adjointSolver.SetAtoAGreenTensorAndModel(_aToAGreenTensor, _currentModel);

            var responses = _observedContainer.AllResponses[frequency];
            var fields = rc.LevelFields;

            _currentAdjointSource = Polarization.X;
            var ffaa1 = CalculateAdjoint(PdcComponent.E1, PdcComponent.H1, responses, fields);
            _currentAdjointSource = Polarization.Y;
            var ffaa2 = CalculateAdjoint(PdcComponent.E2, PdcComponent.H2, responses, fields);

            _adjointSolver.ClearAllGreenTensors();

            CalculateToInversionDomain(ffaa1, _fullFieldsAtAnomaly1, sigma, gradient);
            CalculateToInversionDomain(ffaa2, _fullFieldsAtAnomaly2, sigma, gradient);

            ffaa1.Dispose();
            ffaa2.Dispose();

            _fullFieldsAtAnomaly1.Dispose();
            _fullFieldsAtAnomaly2.Dispose();
        }

        private double[,,] ConvertSigmaMToSigma(double[,,] sigmaM)
        {
            var nx = _startModel.Anomaly.LocalSize.Nx;
            var ny = _startModel.Anomaly.LocalSize.Ny;
            var nz = _startModel.Nz;

            for (int i = 0; i < nx; i++)
                for (int j = 0; j < ny; j++)
                    for (int k = 0; k < nz; k++)
                        sigmaM[i, j, k] = Exp(sigmaM[i, j, k]);

            return sigmaM;
        }

        private unsafe void AppendStabGradientToInversionGradient(double[,,] stabGradient, double* gradient)
        {
            var nx = _startModel.Anomaly.LocalSize.Nx;
            var ny = _startModel.Anomaly.LocalSize.Ny;
            var nz = _startModel.Nz;

            int shift = GetShiftX();

            for (int i = 0; i < nx; i++)
                for (int j = 0; j < ny; j++)
                    for (int k = 0; k < nz; k++)
                    {
                        var index = _inversionDomain.Mask[i + shift, j, k];
                        gradient[index] += Lambda * stabGradient[i, j, k];
                    }
        }

        private unsafe void ClearFullGradient(double* gradient)
        {
            for (int i = 0; i < _inversionDomain.Length; i++)
                gradient[i] = 0;
        }

        private int GetShiftX()
        {
            if (_mpi.IsParallel)
                return _mpi.CalcLocalHalfNxStart(_startModel.Nx);

            return 0;
        }

        private unsafe double[,,] RiseFromAshes(double* d, InversionDomain inversionDomain)
        {
            var nx = _startModel.Anomaly.LocalSize.Nx;
            var ny = _startModel.Anomaly.LocalSize.Ny;
            var nz = _startModel.Nz;



            //var sigmas = new double[_startModel.Nx, _startModel.Ny, _startModel.Nz];
			var sigmas = new double[nx, ny, nz];
            int shift = GetShiftX();
	
            for (int i = 0; i < nx; i++)
                for (int j = 0; j < ny; j++)
                    for (int k = 0; k < nz; k++)
                    {
                        var index = inversionDomain.Mask[i + shift, j, k];
                        sigmas[i, j, k] = d[index];
                    }
			
            return sigmas;
        }

        private unsafe void CalculateToInversionDomain(AnomalyCurrent ac1, AnomalyCurrent ac2, double[,,] sigma, double* gradient)
        {
            var thicknesses = _currentModel.Anomaly.Layers.Select(l => (double)l.Thickness).ToArray();

            int shift = GetShiftX();

            for (int i = 0; i < ac1.Nx; i++)
            {
                for (int j = 0; j < ac1.Ny; j++)
                {
                    for (int k = 0; k < ac1.Nz; k++)
                    {
                        var indexX = (i * ac1.Ny + j) * (3 * ac1.Nz) + k;
                        var indexY = (i * ac1.Ny + j) * (3 * ac1.Nz) + k + ac1.Nz;
                        var indexZ = (i * ac1.Ny + j) * (3 * ac1.Nz) + k + ac1.Nz + ac1.Nz;

                        int dtsIndex = _inversionDomain.Mask[i + shift, j, k];

                        gradient[dtsIndex] += (ac1.Ptr[indexX] * ac2.Ptr[indexX]).Real * thicknesses[k] * sigma[i, j, k];
                        gradient[dtsIndex] += (ac1.Ptr[indexY] * ac2.Ptr[indexY]).Real * thicknesses[k] * sigma[i, j, k];
                        gradient[dtsIndex] += (ac1.Ptr[indexZ] * ac2.Ptr[indexZ]).Real * thicknesses[k] * sigma[i, j, k];
                    }
                }
            }
        }

        private AnomalyCurrent CalculateAdjoint(PdcComponent typeE, PdcComponent typeH, List<ResponsesWithWeightsAtLevel> responses,
            List<AllFieldsAtLevel> fields)
        {
            foreach (var level in _project.Extreme.ObservationLevels)
            {
                var sourceE = AnomalyCurrent.AllocateNewOneLayer(_memoryProvider, _currentModel);
                var sourceH = AnomalyCurrent.AllocateNewOneLayer(_memoryProvider, _currentModel);

                Clear(sourceE);
                Clear(sourceH);

                var resp = responses.First(r => r.Level == level);
                var allFields = fields.First(f => f.Level == level);

                if (_project.InversionSettings.Responses.Impedance)
                    _adjointCalculator.With(resp.ImpedanceTensors, resp.ImpedanceWeights);

                if (_project.InversionSettings.Responses.Tipper)
                    _adjointCalculator.With(resp.Tippers, resp.TipperWeights);

                _adjointCalculator.Calculate(allFields, sourceE, typeE);
                _adjointCalculator.Calculate(allFields, sourceH, typeH);

                _adjointSolver.AddSourceE(level, sourceE);
                _adjointSolver.AddSourceH(level, sourceH);
            }


            var fullFieldAtAnomaly = _adjointSolver.Solve(_currentModel);

            _adjointSolver.ClearAllSources();

            return fullFieldAtAnomaly;
        }

        private double CalculateResidualSqr(double frequency, ObservedContainer oc, ResultsContainer rc)
        {
         

            var obs = oc.AllResponses[frequency];
            var calc = rc.LevelFields;
            var resps = _project.InversionSettings.Responses;

            var impRsdSqr = 0.0;
            var tipRsdSqr = 0.0;

            foreach (var level in _project.Extreme.ObservationLevels)
            {
                var resp = obs.First(r => r.Level == level);
                var allFields = calc.First(f => f.Level == level);

                var rsdCalc = new ResidualCalculator(_startModel.LateralDimensions, _startModel.Anomaly.LocalSize);

                if (resps.Impedance)
                    impRsdSqr += rsdCalc.CalculateResidualSqr(resp.ImpedanceTensors, resp.ImpedanceWeights, allFields);

                if (resps.Tipper)
                    tipRsdSqr += rsdCalc.CalculateResidualSqr(resp.Tippers, resp.TipperWeights, allFields);
            }




            impRsdSqr = _mpi.AllReduce(Mpi.CommWorld, impRsdSqr);
            tipRsdSqr = _mpi.AllReduce(Mpi.CommWorld, tipRsdSqr);

            if (_evaluationIterationCounter == 0)
                RebalanceWeights(obs, ref impRsdSqr, ref tipRsdSqr);

            var residual = (impRsdSqr + tipRsdSqr);

            if (_mpi.IsMaster)
                _logger.WriteWarning($"impRsdSqr {impRsdSqr}, tipRsdSqr = {tipRsdSqr}");

            return residual;
        }

        private void RebalanceWeights(List<ResponsesWithWeightsAtLevel> obs, ref double impRsdSqr, ref double tipRsdSqr)
        {
            var resps = _project.InversionSettings.Responses;

            if (resps.Impedance && resps.Tipper)
            {
                var alpha = impRsdSqr / tipRsdSqr;
                tipRsdSqr *= alpha;

                foreach (var level in _project.Extreme.ObservationLevels)
                {
                    var resp = obs.First(r => r.Level == level);

                    var weights = resp.TipperWeights;

                    var d1 = weights.GetLength(0);
                    var d2 = weights.GetLength(1);

                    for (int i = 0; i < d1; i++)
                    {
                        for (int j = 0; j < d2; j++)
                        {
                            var zx = weights[i, j].Zx * alpha;
                            var zy = weights[i, j].Zy * alpha;

                            weights[i, j] = new TipperWeights(zx, zy);
                        }
                    }
                }
            }
        }

        private unsafe void Clear(AnomalyCurrent ac)
        {
            var length = ac.Nx * ac.Ny * ac.Nz * 3;
            UnsafeNativeMethods.ClearBuffer(ac.Ptr, length);
        }

        public void Dispose()
        {
            _solver.Dispose();
            _adjointSolver.Dispose();
            _sourceE.Dispose();
            _sourceH.Dispose();

            _firstFrequencyCache.Dispose();
            _currentFrequencyCache.Dispose();
        }

    }
}
