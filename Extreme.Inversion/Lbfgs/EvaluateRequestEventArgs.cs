﻿using System;

namespace Extreme.Inversion.Lbfgs
{
    public unsafe class EvaluateRequestEventArgs : EventArgs
    {
        public double* M { get; }
        public double* Gradient { get; }

        public double ObjectiveFunction { get; set; }

        public EvaluateRequestEventArgs(double* m, double* gradient)
        {
            M = m;
            Gradient = gradient;
        }
    }
}
