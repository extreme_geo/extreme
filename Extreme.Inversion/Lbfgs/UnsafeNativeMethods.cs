using System;
using System.Runtime.InteropServices;
using System.Security;
namespace Extreme.Inversion.Lbfgs
{
	[SuppressUnmanagedCodeSecurity]
	internal static class UnsafeNativeMethods
	{
		public unsafe delegate double Evaluate(IntPtr instance, double* x, double* g, int n, double step);
		public unsafe delegate int Progress(IntPtr instance, double* x, double* g, double fx, double xnorm, double gnorm, double step, int n, int k, int ls);
        public unsafe delegate IntPtr VecAlloc(IntPtr numberOfBytes);
        public unsafe delegate void VecFree(IntPtr ptr);

        private const string DllName = "lbfgs";
		[DllImport(DllName)]
		public static extern void InitLbfgsParameters(out LbfgsParameters parameters);

        [DllImport(DllName)]
		public unsafe static extern int RunLbfgs(int n, double* x, out double fx, 
            Evaluate ev, Progress prog, IntPtr instance, ref LbfgsParameters parameters,
            VecAlloc alloc, VecFree free);

	    [DllImport(DllName)]
	    public static unsafe extern void ClearBuffer(IntPtr buffer, IntPtr length);
	}
}
