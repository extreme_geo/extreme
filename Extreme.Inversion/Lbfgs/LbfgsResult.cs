using System;
namespace Extreme.Inversion.Lbfgs
{
	public enum LbfgsResult
	{
		Success,
		Convergence = 0,
		Stop,
		AlreadyMinimized,
		UnknownError = -1024,
		LogicError,
		OutOfMemory,
		Canceled,
		InvalidN,
		InvalidNSse,
		InvalidXSse,
		InvalidEpsilon,
		InvalidTestperiod,
		InvalidDelta,
		InvalidLinesearch,
		InvalidMinstep,
		InvalidMaxstep,
		InvalidFtol,
		InvalidWolfe,
		InvalidGtol,
		InvalidXtol,
		InvalidMaxlinesearch,
		InvalidOrthantwise,
		InvalidOrthantwiseStart,
		InvalidOrthantwiseEnd,
		Outofinterval,
		IncorrectTminmax,
		RoundingError,
		Minimumstep,
		Maximumstep,
		Maximumlinesearch,
		Maximumiteration,
		Widthtoosmall,
		Invalidparameters,
		Increasegradient
	}
}
