using System;
using System.Runtime.InteropServices;

namespace Extreme.Inversion.Lbfgs
{
    public class Example
    {
        public const int N = 100;
        public unsafe static void Run()
        {
            double[] array = new double[100];
            for (int i = 0; i < 100; i += 2)
            {
                array[i] = -1.2;
                array[i + 1] = 1.0;
            }
            LbfgsParameters lbfgsParameters;
            UnsafeNativeMethods.InitLbfgsParameters(out lbfgsParameters);
            lbfgsParameters.LineSearch = LineSearchType.MoreThuente;
            fixed (double* ptr = &array[0])
            {
                double num;
                int num2 = UnsafeNativeMethods.RunLbfgs(100, ptr, out num, 
                    EvaluateImpl, ProgressImpl, IntPtr.Zero, ref lbfgsParameters,
                    Alloc, Free);
                Console.WriteLine($"L-BFGS optimization terminated with status code = {num2}");
                Console.WriteLine($"  fx = {num:0:00000}, x[0] = {array[0]:0:00000}, x[1] = {array[1]:0:00000}");
            }
        }

        private static IntPtr Alloc(IntPtr size)
        {
            Console.WriteLine($"Allocate {size} bytes");
            return Marshal.AllocHGlobal(size);
        }



        private static void Free(IntPtr ptr)
        {
            Console.WriteLine($"Free {ptr.ToInt64()} ptr");
            Marshal.FreeHGlobal(ptr);
        }

        private unsafe static double EvaluateImpl(IntPtr instance, double* x, double* g, int n, double step)
        {
            Console.WriteLine("======================= evaluate");
            double num = 0.0;
            for (int i = 0; i < n; i += 2)
            {
                double num2 = 1.0 - x[i];
                double num3 = 10.0 * (x[(i + 1)] - x[i] * x[i]);
                g[(i + 1)] = 20.0 * num3;
                g[i] = -2.0 * (x[i] * g[i + 1] + num2);
                num += num2 * num2 + num3 * num3;
            }
            return num;
        }
        private unsafe static int ProgressImpl(IntPtr instance, double* x, double* g, double fx, double xnorm, double gnorm, double step, int n, int k, int ls)
        {
            Console.WriteLine($"Iteration {k}:");
            Console.WriteLine($"  fx = {fx:###0.000000}, x[0] = {*x:###0.000000}, x[1] = {x[1]:###0.000000}");
            Console.WriteLine($"  xnorm = {xnorm:###0.000000}, gnorm = {gnorm:###0.000000}, step = {step:###0.000000}", xnorm, gnorm, step);
            Console.WriteLine();
            return 0;
        }
    }
}
