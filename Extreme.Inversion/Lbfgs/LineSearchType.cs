using System;
namespace Extreme.Inversion.Lbfgs
{
	public enum LineSearchType : int
	{
		MoreThuente = 0,
		BacktrackingArmijo = 1,
		BacktrackingWolfe = 2,
		BacktrackingStrongWolfe = 3
	}
}
