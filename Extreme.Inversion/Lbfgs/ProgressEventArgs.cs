﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Extreme.Inversion.Lbfgs
{
    public unsafe class ProgressEventArgs:EventArgs
    {
        public double XNorm { get; }
        public double GNorm { get; }
        public int ReturnCode { get; set; } = 0;
        public double* X { get; }

        public int NumberOfVariables { get; }
        public int IterationCount { get; }

        public ProgressEventArgs(double* x, double xNorm, double gNorm, int numberOfVariables, int iterationCount)
        {
            XNorm = xNorm;
            GNorm = gNorm;
            X = x;

            NumberOfVariables = numberOfVariables;
            IterationCount = iterationCount;
        }
    }
}
