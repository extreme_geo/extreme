﻿using System;
using Extreme.Cartesian.Model;
using Extreme.Core;
using Extreme.Inversion;
using Extreme.Parallel;


namespace Extreme.Inversion.Lbfgs
{
    public unsafe class LbfgsSolver
    {
        private readonly INativeMemoryProvider _memoryProvider;
        private readonly ILogger _logger;
        private readonly Mpi _mpi;
        public LbfgsParameters Parameters;

        public LbfgsSolver(INativeMemoryProvider memoryProvider, ILogger logger, Mpi mpi)
        {
            if (memoryProvider == null) throw new ArgumentNullException(nameof(memoryProvider));
            if (logger == null) throw new ArgumentNullException(nameof(logger));
            if (mpi == null) throw new ArgumentNullException(nameof(mpi));

            _memoryProvider = memoryProvider;
            _logger = logger;
            _mpi = mpi;
            Parameters = new LbfgsParameters();

            UnsafeNativeMethods.InitLbfgsParameters(out Parameters);
        }

        public event EventHandler<EvaluateRequestEventArgs> EvaluateRequest;
        public event EventHandler<ProgressEventArgs> Progress;

        protected virtual void OnEvaluateRequest(EvaluateRequestEventArgs e)
        {
            EvaluateRequest?.Invoke(this, e);
        }

        protected virtual void OnProgress(ProgressEventArgs e)
        {
            Progress?.Invoke(this, e);
        }

        public LbfgsResult Run(double* currentM, int length)
        {
            double fx;

            var result = UnsafeNativeMethods.RunLbfgs(length, currentM, out fx,
                EvaluateImpl, ProgressImpl, IntPtr.Zero, ref Parameters,
                Alloc, Free);

            return (LbfgsResult)result;
        }

        private IntPtr Alloc(IntPtr size)
        {
            var ptr = _memoryProvider.AllocateBytes(size.ToInt64());

            byte* ptru = (byte*)ptr.ToPointer();
            for (long i = 0; i < size.ToInt64(); i++)
                ptru[i] = 0;

            //UnsafeNativeMethods.ClearBuffer(ptr, size);
            return ptr;
        }

        private void Free(IntPtr ptr)
        {
            if (ptr != IntPtr.Zero)
                _memoryProvider.ReleaseMemory(ptr);
        }

        private double EvaluateImpl(IntPtr instance, double* x, double* g, int n, double step)
        {
            var e = new EvaluateRequestEventArgs(x, g);
            OnEvaluateRequest(e);
            return e.ObjectiveFunction;
        }

        private int ProgressImpl(IntPtr instance, double* x, double* g, double fx, double xnorm, double gnorm, double step, int n, int k, int ls)
        {
            var e = new ProgressEventArgs(x, xnorm, gnorm, n, k);
            OnProgress(e);
            return e.ReturnCode;
        }
    }
}
