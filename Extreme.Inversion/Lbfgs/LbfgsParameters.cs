using System;
using System.Runtime.InteropServices;

namespace Extreme.Inversion.Lbfgs
{
    [StructLayout(LayoutKind.Sequential)]
    public struct LbfgsParameters
	{
		public int M;
		public double Epsilon;
		public int Past;
		public double Delta;
		public int MaxIterations;
		public LineSearchType LineSearch;
		public int MaxLinesearch;
		public double MinStep;
		public double MaxStep;
		public double Ftol;
		public double Wolfe;
		public double Gtol;
		public double Xtol;
		public double OrthantWiseC;
		public int OrthantWiseStart;
		public int OrthantWiseEnd;
	}
}
