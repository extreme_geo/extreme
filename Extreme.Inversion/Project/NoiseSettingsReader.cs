﻿using System.Xml.Linq;
using Extreme.Cartesian.Project;
using Extreme.Core;

namespace Extreme.Inversion.Project
{
    public class NoiseSettingsReader : IProjectSettingsReader
    {
        public ProjectSettings FromXElement(XElement xelem)
        {
            return new NoiseSettings()
            {
                Percent = xelem?.ElementAsDoubleOrNull("Percent") ?? 0.0,
                Epsilon = xelem?.ElementAsDoubleOrNull("Epsilon") ?? 1.0,
            };
        }
    }
}
