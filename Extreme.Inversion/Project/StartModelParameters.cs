﻿namespace Extreme.Inversion.Project
{
    public class StartModelParameters
    {
        public string MaskFile { get; set; } = "";
        public string InversionDomainFile { get; set; } = "";
    }
}
