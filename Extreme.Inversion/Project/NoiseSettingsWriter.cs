﻿using System;
using System.Xml.Linq;
using Extreme.Cartesian.Project;

namespace Extreme.Inversion.Project
{
    public class NoiseSettingsWriter : IProjectSettingsWriter
    {
        public XElement ToXElement(ProjectSettings settings)
        {
            var noise = settings as NoiseSettings;

            return new XElement(settings.Name,
                new XElement("Percent", noise?.Percent),
                new XElement("Epsilon", noise?.Percent));
        }
    }
}
