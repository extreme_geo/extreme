﻿using System.IO;
using System.Linq;
using System.Xml.Linq;
using Extreme.Cartesian.Project;
using Extreme.Core;
using Extreme.Inversion.Lbfgs;

namespace Extreme.Inversion.Project
{
    public class InversionSettingsReader : IProjectSettingsReader
    {
        public ProjectSettings FromXElement(XElement xelem)
        {
            return new InversionSettings()
            {
                Responses = ReadResponses(xelem),
                Stabilizer = ReadStabilizerParameters(xelem),
                Lbfgs = ReadLbfgsParameters(xelem),
                ObservedData = ReadObservedData(xelem),
                Domain = ReadDomain(xelem),
                StartModel = ReadStartModel(xelem),
            };
        }

        private Responses ReadResponses(XElement xelem)
        {
            var xresp = xelem.Element("Responses");

            var rs = xresp?.Elements("R").Select(r => r.Value.ToLower()).ToList();

            return new Responses()
            {
                Impedance = rs?.Contains("impedance") ?? false,
                Tipper = rs?.Contains("tipper") ?? false,
            };
        }

        private StartModelParameters ReadStartModel(XElement xelem)
        {
            var xmod = xelem?.Element("StartModel");

            return new StartModelParameters()
            {
                MaskFile = xmod?.Element("MaskFile")?.Value ?? "",
                InversionDomainFile = xmod?.Element("InversionDomainFile")?.Value ?? "",
            };
        }

        private InversionDomainParameters ReadDomain(XElement xelem)
        {
            var xdom = xelem?.Element("Domain");
            var type = xdom?.Attribute("type")?.Value ?? "";

            if (type != "simple")
                throw new InvalidDataException($"only simple type supported for inversion domain, not {type}");

            var xcells = xdom?.Element("Cells");

            return new InversionDomainParameters()
            {
                Type = type,
                Nx = xcells?.AttributeAsInt("nx") ?? 0,
                Ny = xcells?.AttributeAsInt("ny") ?? 0,
                Nz = xcells?.AttributeAsInt("nz") ?? 0,
            };
        }

        private StabilizerParameters ReadStabilizerParameters(XElement xelem)
        {
            var xstab = xelem?.Element("Stabilizer");

            return new StabilizerParameters()
            {
                Alpha = xstab?.Element("Alpha")?.ValueAsDouble() ?? 0,
                Beta = xstab?.Element("Beta")?.ValueAsDouble() ?? 0,
                Lambda = xstab?.Element("Lambda")?.ValueAsDouble() ?? 0,
            };
        }

        private LbfgsExternalParameters ReadLbfgsParameters(XElement xelem)
        {
            var xlbfgs = xelem?.Element("Lbfgs");

            return new LbfgsExternalParameters()
            {
                Memory = xlbfgs?.Element("Memory")?.ValueAsInt32() ?? 12,
                Tolerance = xlbfgs?.Element("Tolerance")?.ValueAsDouble() ?? 1E-7,
                LineSearchLength = xlbfgs?.Element("LineSearchLength")?.ValueAsInt32() ?? 40,
                LineSearchType = ToLineSearchType(xlbfgs?.Element("LineSearchType"))
            };
        }

        private LineSearchType ToLineSearchType(XElement xelem)
        {
            if (xelem == null)
                return LineSearchType.MoreThuente;
            
            if (xelem.Value.ToLower() == "Armijo".ToLower())
                return LineSearchType.BacktrackingArmijo;

            if (xelem.Value.ToLower() == "MoreThuente".ToLower())
                return LineSearchType.MoreThuente;

            if (xelem.Value.ToLower() == "Wolfe".ToLower())
                return LineSearchType.BacktrackingWolfe;

            if (xelem.Value.ToLower() == "StrongWolfe".ToLower())
                return LineSearchType.BacktrackingStrongWolfe;
            
            return LineSearchType.MoreThuente;
        }

        private ObservedData ReadObservedData(XElement xelem)
        {
            var xobs = xelem?.Element("ObservedData");

            return new ObservedData()
            {
                Path = xobs?.Element("Path")?.Value ?? ""
            };
        }
    }
}
