﻿using Extreme.Inversion.Lbfgs;

namespace Extreme.Inversion.Project
{
    public class LbfgsExternalParameters
    {
        public int Memory { get; set; } = 12;
        public double Tolerance { get; set; } = 1E-7;
        public LineSearchType LineSearchType { get; set; } = LineSearchType.MoreThuente;
        public int LineSearchLength { get; set; } = 40;
    }
}
