﻿namespace Extreme.Inversion.Project
{
    public class InversionDomainParameters
    {
        public string Type { get; set; } = "simple";
        public int Nx { get; set; }
        public int Ny { get; set; }
        public int Nz { get; set; }
    }
}
