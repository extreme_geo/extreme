﻿using System;
using System.Collections.Generic;
using Extreme.Cartesian.Forward;
using Extreme.Cartesian.Project;

namespace Extreme.Inversion.Project
{
    public class InversionProject
    {
        public ExtremeProject Extreme { get; }
        public ForwardSettings ForwardSettings => Extreme.Settings["Forward"] as ForwardSettings;
        public InversionSettings InversionSettings => Extreme.Settings["Inversion"] as InversionSettings;
        public NoiseSettings NoiseSettings => Extreme.Settings["Noise"] as NoiseSettings;

        private InversionProject(double[] frequencies)
        {
            Extreme = new ExtremeProject(frequencies,
                        new Dictionary<string, ProjectSettings>
                        {
                            ["Forward"] = new ForwardSettings(),
                            ["Inversion"] = new InversionSettings(),
                        });
        }

        private InversionProject(ExtremeProject extremeProject)
        {
            if (extremeProject == null) throw new ArgumentNullException(nameof(extremeProject));
            Extreme = extremeProject;
        }

        public static InversionProject NewWithFrequencies(params double[] frequencies)
                    => new InversionProject(frequencies);

        public static InversionProject NewFrom(ExtremeProject extremeProject)
                  => new InversionProject(extremeProject);
    }
}
