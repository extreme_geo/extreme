﻿using System.Xml.Linq;
using System.Collections.Generic;

using Extreme.Cartesian.Forward;
using Extreme.Cartesian.Project;

namespace Extreme.Inversion.Project
{
    public class InversionProjectSerializer
    {
        public static void Save(InversionProject project, string fileName)
            => ToXDocument(project).Save(fileName);

        public static InversionProject Load(string fileName)
            => FromXDocument(XDocument.Load(fileName));

        public static XDocument ToXDocument(InversionProject project)
        {
            var serializer = new ProjectWriter(
                project: project.Extreme,
                settingsWriters: new Dictionary<string, IProjectSettingsWriter>
                {
                    ["Forward"] = new ForwardSettingsWriter(),
                    ["Inversion"] = new InversionSettingsWriter(),
                    ["Noise"] = new NoiseSettingsWriter(),
                });

            return serializer.ToXDocument();
        }

        public static InversionProject FromXDocument(XDocument xdoc)
        {
            var serializer = new ProjectReader(
                settingsReaders: new Dictionary<string, IProjectSettingsReader>
                {
                    ["Forward"] = new ForwardSettingsReader(),
                    ["Inversion"] = new InversionSettingsReader(),
                    ["Noise"] = new NoiseSettingsReader(),
                });

            return InversionProject.NewFrom(serializer.FromXDocument(xdoc));
        }
    }
}