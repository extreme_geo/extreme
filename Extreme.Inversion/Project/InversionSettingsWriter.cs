﻿using System;
using System.Xml.Linq;
using Extreme.Cartesian.Project;
using Extreme.Inversion.Lbfgs;

namespace Extreme.Inversion.Project
{
    public class InversionSettingsWriter : IProjectSettingsWriter
    {
        public XElement ToXElement(ProjectSettings settings)
        {
            var inversionSettings = settings as InversionSettings;

            return new XElement(settings.Name,
                ToXElement(inversionSettings?.Responses),
                ToXElement(inversionSettings?.Domain),
                ToXElement(inversionSettings?.StartModel),
                ToXElement(inversionSettings?.ObservedData),
                ToXElement(inversionSettings?.Stabilizer),
                ToXElement(inversionSettings?.Lbfgs));
        }

        private XElement ToXElement(Responses responses)
        {
            return new XElement("Responses",
                responses.Impedance ? new XElement("R", "Impedance") : null,
                responses.Tipper ? new XElement("R", "Tipper") : null);
        }

        private XElement ToXElement(StartModelParameters startModel)
        {
            return new XElement("StartModel",
                    string.IsNullOrEmpty(startModel.MaskFile) ? null : new XElement("MaskFile", startModel.MaskFile),
                    string.IsNullOrEmpty(startModel.InversionDomainFile) ? null : new XElement("InversionDomainFile", startModel.InversionDomainFile));
        }

        private XElement ToXElement(InversionDomainParameters domain)
        {
            return new XElement("Domain",
                new XAttribute("type", domain.Type),
                new XElement("Cells",
                    new XAttribute("nx", domain.Nx),
                    new XAttribute("ny", domain.Ny),
                    new XAttribute("nz", domain.Nz)));
        }

        private XElement ToXElement(LbfgsExternalParameters lbfgs)
        {
            return new XElement("Lbfgs",
                new XElement("Memory", lbfgs.Memory),
                new XElement("Tolerance", lbfgs.Tolerance),
                new XElement("LineSearchLength", lbfgs.LineSearchLength),
                new XElement("LineSearchType", ToLineSearchTypeString(lbfgs.LineSearchType)));
        }

        private string ToLineSearchTypeString(LineSearchType lineSearchType)
        {
            if (lineSearchType == LineSearchType.MoreThuente)
                return "MoreThuente";

            if (lineSearchType == LineSearchType.BacktrackingArmijo)
                return "Armijo";

            if (lineSearchType == LineSearchType.BacktrackingWolfe  )
                return "Wolfe";

            if (lineSearchType == LineSearchType.BacktrackingStrongWolfe)
                return "StrongWolfe";

            return "MoreThuente";
        }

        private XElement ToXElement(StabilizerParameters stabilizer)
        {
            return new XElement("Stabilizer",
                new XElement("Lambda", stabilizer?.Lambda),
                new XElement("Alpha", stabilizer?.Alpha),
                new XElement("Beta", stabilizer?.Beta));
        }

        private XElement ToXElement(ObservedData observedData)
        {
            return new XElement("ObservedData",
                new XElement("Path", observedData?.Path));
        }
    }
}
