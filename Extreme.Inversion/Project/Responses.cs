﻿namespace Extreme.Inversion.Project
{
    public class Responses
    {
        public bool Impedance { get; set; } = true;
        public bool Tipper { get; set; } = false;
    }
}
