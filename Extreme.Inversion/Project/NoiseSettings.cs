﻿using Extreme.Cartesian.Project;

namespace Extreme.Inversion.Project
{
    public class NoiseSettings : ProjectSettings
    {
        public double Percent { get; set; } = 0;
        public double Epsilon { get; set; } = 1;
        
        public NoiseSettings() : base("Noise")
        {
        }
    }
}
