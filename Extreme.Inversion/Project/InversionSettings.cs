﻿using Extreme.Cartesian.Project;

namespace Extreme.Inversion.Project
{
    public class InversionSettings : ProjectSettings
    {
        public StabilizerParameters Stabilizer { get; set; } = new StabilizerParameters();
        public LbfgsExternalParameters Lbfgs { get; set; } = new LbfgsExternalParameters();
        public ObservedData ObservedData { get; set; } = new ObservedData();
        public InversionDomainParameters Domain { get; set; } = new InversionDomainParameters();
        public StartModelParameters StartModel { get; set; } = new StartModelParameters();
        public Responses Responses { get; set; } = new Responses();

        public InversionSettings()
            : base("Inversion")
        {
        }
    }
}
