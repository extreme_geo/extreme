﻿using System;
using Extreme.Cartesian.Core;
using Extreme.Cartesian.Model;
using Extreme.Core;

namespace Extreme.Inversion
{
    public class ChiCache : IDisposable
    {
        public AnomalyCurrent ForwardPol1 { get; }
        public AnomalyCurrent ForwardPol2 { get; }
        public AnomalyCurrent AdjointPol1 { get; }
        public AnomalyCurrent AdjointPol2 { get; }

        private ChiCache(AnomalyCurrent forwardPol1, AnomalyCurrent forwardPol2, AnomalyCurrent adjointPol1, AnomalyCurrent adjointPol2)
        {
            ForwardPol1 = forwardPol1;
            ForwardPol2 = forwardPol2;
            AdjointPol1 = adjointPol1;
            AdjointPol2 = adjointPol2;
        }

        public static ChiCache Allocate(INativeMemoryProvider memoryProvider, CartesianModel template)
        {
            int nx = template.Anomaly.LocalSize.Nx;
            int ny = template.Anomaly.LocalSize.Ny;
            int nz = template.Nz;

            return new ChiCache(
                AnomalyCurrent.AllocateNew(memoryProvider, nx, ny, nz),
                AnomalyCurrent.AllocateNew(memoryProvider, nx, ny, nz),
                AnomalyCurrent.AllocateNew(memoryProvider, nx, ny, nz),
                AnomalyCurrent.AllocateNew(memoryProvider, nx, ny, nz));
        }

        public void Dispose()
        {
            ForwardPol1.Dispose();
            ForwardPol2.Dispose();

            AdjointPol1.Dispose();
            AdjointPol2.Dispose();
        }
    }
}
