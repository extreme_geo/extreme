﻿using System;
using Extreme.Inversion.Lbfgs;

namespace Extreme.Inversion
{
    public class InversionIterationCompleteEventArgs : EventArgs
    {
        public ProgressEventArgs Progress { get; }

        public InversionIterationCompleteEventArgs(ProgressEventArgs progress)
        {
            Progress = progress;
        }
    }
}
