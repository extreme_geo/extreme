﻿using System;

namespace Extreme.Inversion
{
    public class InversionDomainCreatedEventArgs : EventArgs
    {
        public  InversionDomain InversionDomain { get; }

        public InversionDomainCreatedEventArgs(InversionDomain inversionDomain)
        {
            InversionDomain = inversionDomain;
        }
    }
}
