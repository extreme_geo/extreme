﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FftWrap;
using Extreme.Cartesian.Fft;
using Extreme.Cartesian.FftW;
using Extreme.Cartesian.Model;
using Extreme.Core;

namespace Extreme.Inversion
{
    public class GradientCalculatorWithFilter
    {
        private readonly INativeMemoryProvider _memoryProvider;

        private readonly double[] _derivativeWeights = new double[] { 1, 0, -1 };
        private readonly double[] _interpolationWeights = new double[] { 3, 10, 3 };

        public GradientCalculatorWithFilter(INativeMemoryProvider memoryProvider)
        {
            _memoryProvider = memoryProvider;
        }

        public void CalculateGradient(CartesianModel model)
        {
            var filter = GradientFilter.GenerateFilter(_interpolationWeights, _derivativeWeights);

            var l = filter.Length;

            var nx = model.Nx + l + 1;
            var ny = model.Ny + l + 1;
            var nz = model.Nz + l + 1;
            var length = nx * ny * nz;

            var bufferX = Alloc(nx, ny, nz);
            var bufferY = Alloc(nx, ny, nz);
            var bufferZ = Alloc(nx, ny, nz);

            var bufferSigma = Alloc(nx, ny, nz);


            var bufferXres = Alloc(nx, ny, nz);
            var bufferYres = Alloc(nx, ny, nz);
            var bufferZres = Alloc(nx, ny, nz);


            var fft = new FftWTransform(1);

            var fwd = fft.CreatePlan3D(bufferX.IntPtr, bufferX.IntPtr, nx, ny, nz, Direction.Forward, Flags.Estimate);
            var bwd = fft.CreatePlan3D(bufferX.IntPtr, bufferX.IntPtr, nx, ny, nz, Direction.Backward, Flags.Estimate);

            PutFilterInBuffer(filter.X, bufferX, nx, ny, nz);
            PutFilterInBuffer(filter.Y, bufferY, nx, ny, nz);
            PutFilterInBuffer(filter.Z, bufferZ, nx, ny, nz);

            PutModelInBuffer(model, bufferSigma);


            for (int i = 0; i < model.Nx; i++)
                for (int j = 0; j < model.Ny; j++)
                    for (int k = 0; k < model.Nz; k++)
                    {
                        bufferSigma[i, j, k] = i * 11 + j * 17 + k * 29;

                    }




            fft.ExecutePlan(fwd, bufferX.IntPtr, bufferX.IntPtr);
            fft.ExecutePlan(fwd, bufferY.IntPtr, bufferY.IntPtr);
            fft.ExecutePlan(fwd, bufferZ.IntPtr, bufferZ.IntPtr);

            fft.ExecutePlan(fwd, bufferSigma.IntPtr, bufferSigma.IntPtr);


            MultAll(length, bufferX, bufferSigma, bufferXres);
            MultAll(length, bufferY, bufferSigma, bufferYres);
            MultAll(length, bufferZ, bufferSigma, bufferZres);




            fft.ExecutePlan(bwd, bufferXres.IntPtr, bufferXres.IntPtr);
            fft.ExecutePlan(bwd, bufferYres.IntPtr, bufferYres.IntPtr);
            fft.ExecutePlan(bwd, bufferZres.IntPtr, bufferZres.IntPtr);


            Normalize(length, bufferXres);
            Normalize(length, bufferYres);
            Normalize(length, bufferZres);


            НапечатайПожалуйстаЭтуХерню(model, bufferXres);
            НапечатайПожалуйстаЭтуХерню(model, bufferYres);
            НапечатайПожалуйстаЭтуХерню(model, bufferZres);

        }

        private void Normalize(int length, UnsafeArray buffer)
        {
            for (int i = 0; i < length; i++)
                buffer[i] /= length;
        }

        private void НапечатайПожалуйстаЭтуХерню(CartesianModel model, UnsafeArray buffer)
        {
            for (int i = 0; i < model.Nx; i++)
                for (int j = 0; j < model.Ny; j++)
                    for (int k = 0; k < model.Nz; k++)
                    {
                        Console.WriteLine($"[{i},{j},{k}] = {buffer[i, j, k]}");

                    }

            Console.WriteLine();
        }

        private void MultAll(int length, UnsafeArray src, UnsafeArray sigma, UnsafeArray dst)
        {
            for (int i = 0; i < length; i++)
                dst[i] = src[i] * sigma[i];
        }


        private void PutModelInBuffer(CartesianModel model, UnsafeArray buffer)
        {
            var nx = model.Nx;
            var ny = model.Ny;
            var nz = model.Nz;

            var sigma = model.Anomaly.Sigma;

            for (int i = 0; i < nx; i++)
                for (int j = 0; j < ny; j++)
                    for (int k = 0; k < nz; k++)
                        buffer[i, j, k] = sigma[i, j, k];
        }

        private void PutFilterInBuffer(double[,,] filter, UnsafeArray buffer, int nx, int ny, int nz)
        {
            int length = filter.GetLength(0);

            int shift = (length - 1) / 2;

            for (int i = 0; i < length; i++)
                for (int j = 0; j < length; j++)
                    for (int k = 0; k < length; k++)
                    {
                        var dstI = (i - shift + nx) % nx;
                        var dstJ = (j - shift + ny) % ny;
                        var dstK = (k - shift + nz) % nz;

                        buffer[dstI, dstJ, dstK] = filter[i, j, k];
                    }
        }

        private unsafe UnsafeArray Alloc(int nx, int ny, int nz)
              => new UnsafeArray(_memoryProvider.AllocateComplex(nx * ny * nz)).ReShape(nx, ny, nz);
    }
}
