﻿using System;
using System.Collections.Generic;
using System.Linq;
using Extreme.Cartesian;
using Extreme.Cartesian.Core;
using Extreme.Cartesian.Forward;
using Extreme.Cartesian.Logger;
using Extreme.Cartesian.Model;
using Extreme.Cartesian.Project;
using Extreme.Core;
//using Porvem.Inversion;

namespace Extreme.Cartesian.Ionosphere
{
    public sealed class IonosphereForwardSolver : ForwardSolver
    {
        private readonly Dictionary<SourceLayer, AnomalyCurrent> _sourceLayers;
        private StoOCalculator _stoO;

        private bool _atoACalculated = false;

        public IonosphereForwardSolver(ILogger logger, INativeMemoryProvider memoryProvider, ForwardSettings settings)
            : base(logger, memoryProvider, settings)
        {
            _sourceLayers = new Dictionary<SourceLayer, AnomalyCurrent>();
        }

        public void ClearSources()
            => _sourceLayers.Clear();

        public void AddSource(SourceLayer layer, AnomalyCurrent field)
        {
            _sourceLayers.Add(layer, field);
        }

        public void Solve(OmegaModel model)
        {
            SetModel(model);

            using (Profiler.StartAuto(ProfilerEvent.ForwardSolving))
            {
                if (!_atoACalculated)
                {
                    Logger.WriteStatus("Calculate Green Tensor AtoA");
                    CalculateGreenTensor();
                    _atoACalculated = true;
                }

                Logger.WriteStatus("Starting for one source");
                SolveForOneSource();
            }
        }

        protected unsafe override void CalculateNormalFieldFromSource(AnomalyCurrent field)
        {
            var sToA = new StoACalculator(this);
            Clear(field);

            int counter = 1;
            foreach (var sls in _sourceLayers)
            {
                Logger.WriteStatus($"Source layer {counter++} of {_sourceLayers.Count} " +
                                   $"x:{sls.Key.ShiftAlongX} y:{sls.Key.ShiftAlongY} z:{sls.Key.Z}");
                sToA.CalculateAnomalyFieldE(sls.Key, sls.Value, field);
            }

            var thicknesses = Model.Anomaly.Layers.Select(l => (double)l.Thickness).ToArray();

            int length = field.Nx * field.Ny * 3;

            for (int i = 0; i < length; i++)
            {
                var shift = thicknesses.Length * i;

                for (int k = 0; k < thicknesses.Length; k++)
                    field.Ptr[shift + k] /= thicknesses[k];
            }
        }

        #region Observations

        protected override void OnObservationsCalculating()
        {
            _stoO = new StoOCalculator(this);
        }

        #region Levels

        public event EventHandler<FieldsAtLevelCalculatedEventArgs> EFieldsAtLevelCalculated;
        public event EventHandler<FieldsAtLevelCalculatedEventArgs> HFieldsAtLevelCalculated;

        protected override void OnEFieldsAtLevelCalculated(ObservationLevel level,
            AnomalyCurrent normalField, AnomalyCurrent anomalyField)
        {
            var e = new FieldsAtLevelCalculatedEventArgs(level, normalField, anomalyField);
            EFieldsAtLevelCalculated?.Invoke(this, e);
        }

        protected override void OnHFieldsAtLevelCalculated(ObservationLevel level,
            AnomalyCurrent normalField, AnomalyCurrent anomalyField)
        {
            var e = new FieldsAtLevelCalculatedEventArgs(level, normalField, anomalyField);
            HFieldsAtLevelCalculated?.Invoke(this, e);
        }

        protected override AnomalyCurrent CalculateNormalFieldE(ObservationLevel level)
            => CalculateNormalField((sl, src, dst) =>
                _stoO.CalculateAnomalyFieldE(sl, level, src, dst));

        protected override AnomalyCurrent CalculateNormalFieldH(ObservationLevel level)
            => CalculateNormalField((sl, src, dst) =>
                _stoO.CalculateAnomalyFieldH(sl, level, src, dst));

        private unsafe AnomalyCurrent CalculateNormalField(Action<SourceLayer, AnomalyCurrent, AnomalyCurrent> calc)
        {
            var normalField = AnomalyCurrent.AllocateNewOneLayer(MemoryProvider, Model);
            Clear(normalField);

            foreach (var sls in _sourceLayers)
                calc(sls.Key, sls.Value, normalField);

            return normalField;
        }


        #endregion

        #region Sites

        protected override unsafe ComplexVector CalculateNormalFieldE(ObservationSite site)
        {
            throw new NotImplementedException();
        }

        protected override unsafe ComplexVector CalculateNormalFieldH(ObservationSite site)
        {
            throw new NotImplementedException();
        }


        protected override unsafe void OnEFieldsAtSiteCalculated(ObservationSite site, ComplexVector normalField, ComplexVector anomalyField)
        {
            throw new NotImplementedException();
        }

        protected override unsafe void OnHFieldsAtSiteCalculated(ObservationSite site, ComplexVector normalField, ComplexVector anomalyField)
        {
            throw new NotImplementedException();
        }


        #endregion

        #endregion
    }
}
