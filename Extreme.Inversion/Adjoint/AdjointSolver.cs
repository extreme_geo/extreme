﻿using System;
using System.Collections.Generic;
using System.Linq;
using Extreme.Cartesian;
using Extreme.Cartesian.Core;
using Extreme.Cartesian.Forward;
using Extreme.Cartesian.Green.Tensor;
using Extreme.Cartesian.Model;
using Extreme.Cartesian.Project;
using Extreme.Core;

namespace Extreme.Inversion
{
    public class AdjointSolver : ForwardSolver
    {
        private readonly Dictionary<ObservationLevel, AnomalyCurrent> _eSources;
        private readonly Dictionary<ObservationLevel, AnomalyCurrent> _hSources;

        private readonly Dictionary<ObservationLevel, GreenTensor> _eGreenTensors;
        private readonly Dictionary<ObservationLevel, GreenTensor> _hGreenTensors;


        private readonly AdjointToACalculator _adjointToACalculator;

        private AnomalyCurrent _normalFieldAtAnomaly;
        private AnomalyCurrent _fullFieldAtAnomaly;


        public AdjointSolver(ILogger logger, INativeMemoryProvider memoryProvider, ForwardSettings settings)
            : base(logger, memoryProvider, settings)
        {
            _eSources = new Dictionary<ObservationLevel, AnomalyCurrent>();
            _hSources = new Dictionary<ObservationLevel, AnomalyCurrent>();

            _eGreenTensors = new Dictionary<ObservationLevel, GreenTensor>();
            _hGreenTensors = new Dictionary<ObservationLevel, GreenTensor>();

            _adjointToACalculator = new AdjointToACalculator(this);
        }

        public void SetAtoAGreenTensorAndModel(GreenTensor aToA, OmegaModel model)
        {
            SetModel(model);
            SetNewGreenTensor(aToA);
        }


        public void AddSourceE(ObservationLevel level, AnomalyCurrent source)
        {
            _eSources.Add(level, source);
        }

        public void AddSourceH(ObservationLevel level, AnomalyCurrent source)
        {
            _hSources.Add(level, source);
        }

        public void AddGreenTensorE(ObservationLevel level, GreenTensor gt)
        {
            var newGt = GreenTensor.ReShape(gt, gt.Nx, gt.Ny, gt.NRc, gt.NTr);
            _eGreenTensors.Add(level, newGt);
        }

        public void AddGreenTensorH(ObservationLevel level, GreenTensor gt)
        {
            var newGt = GreenTensor.ReShape(gt, gt.Nx, gt.Ny, gt.NRc, gt.NTr);
            _hGreenTensors.Add(level, newGt);
        }

        public void ClearAllSources()
        {
            ClearAll(_eSources);
            ClearAll(_hSources);
        }

        public void ClearAllGreenTensors()
        {
            ClearAll(_eGreenTensors);
            ClearAll(_hGreenTensors);
        }

        private void ClearAll<T>(Dictionary<ObservationLevel, T> dict) where T : IDisposable
        {
            foreach (var value in dict.Values)
                value.Dispose();

            dict.Clear();
        }

        public AnomalyCurrent Solve(OmegaModel model)
        {
            SetModel(model);
            

            SolveForOneSource();

            return _fullFieldAtAnomaly;
        }

        protected override void CalculateNormalFieldFromSource(AnomalyCurrent field)
        {
            Clear(field);

            foreach (var level in _eSources.Keys)
            {
                var eSrc = _eSources[level];
                var hSrc = _hSources[level];

                var gtE = _eGreenTensors.First(t => t.Key == level).Value;
                var gtH = _hGreenTensors.First(t => t.Key == level).Value;

                _adjointToACalculator.CalculateAnomalyFieldE(gtE, eSrc, field);
			

                _adjointToACalculator.CalculateAnomalyFieldH(gtH, hSrc, field);
            }

            _normalFieldAtAnomaly = field;
        }

        protected override unsafe void OnEScatteredCalculated(AnomalyCurrent eScattered)
        {
            _fullFieldAtAnomaly = GetNewAnomalyCurrent();

            long size = _fullFieldAtAnomaly.GetFullLength();
            UnsafeNativeMethods.AddElementwise(size, eScattered.Ptr, _normalFieldAtAnomaly.Ptr, _fullFieldAtAnomaly.Ptr);
        }

        #region ObservationFields (should be skipped)

        protected override ComplexVector CalculateNormalFieldE(ObservationSite site)
        {
            throw new NotImplementedException();
        }

        protected override ComplexVector CalculateNormalFieldH(ObservationSite site)
        {
            throw new NotImplementedException();
        }

        protected override AnomalyCurrent CalculateNormalFieldE(ObservationLevel level)
        {
            throw new NotImplementedException();
        }

        protected override AnomalyCurrent CalculateNormalFieldH(ObservationLevel level)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
