﻿using System;
using System.Numerics;
using Extreme.Cartesian.Core;
using Extreme.Cartesian.Magnetotellurics;
using Extreme.Core;
using Extreme.Inversion.Observed;
using Porvem.Magnetotellurics;

namespace Extreme.Inversion.Adjoint
{
    public class AdjointCalculator
    {
        private readonly Size2D _localSize2D;
        private readonly LateralDimensions _lateral;

        private ImpedanceTensor[,] _impedances;
        private TensorWeights[,] _impedanceWeights;

        private MagneticTipper[,] _tippers;
        private TipperWeights[,] _tipperWeights;

        private ElectricTipper[,] _electricTippers;
        private TipperWeights[,] _electricTipperWeights;

        private readonly Complex[][,] _fields = new Complex[12][,];

        private const int Ex1 = 0;
        private const int Ex2 = 1;
        private const int Ey1 = 2;
        private const int Ey2 = 3;
        private const int Ez1 = 4;
        private const int Ez2 = 5;

        private const int Hx1 = 6;
        private const int Hx2 = 7;
        private const int Hy1 = 8;
        private const int Hy2 = 9;
        private const int Hz1 = 10;
        private const int Hz2 = 11;

        private readonly Func<AllFieldsAtSite, Complex>[] _fieldAccessors =
        {
             s => s.AnomalyE1.X,
             s => s.AnomalyE2.X,
             s => s.AnomalyE1.Y,
             s => s.AnomalyE2.Y,
             s => s.AnomalyE1.Z,
             s => s.AnomalyE2.Z,
             s => s.AnomalyH1.X,
             s => s.AnomalyH2.X,
             s => s.AnomalyH1.Y,
             s => s.AnomalyH2.Y,
             s => s.AnomalyH1.Z,
             s => s.AnomalyH2.Z,
        };

        public AdjointCalculator(Size2D localSize2D, LateralDimensions lateral)
        {
            _localSize2D = localSize2D;
            _lateral = lateral;
        }

        public AdjointCalculator With(ImpedanceTensor[,] impedances, TensorWeights[,] weights)
        {
            CheckSizes(impedances);
            CheckSizes(weights);

            _impedances = impedances;
            _impedanceWeights = weights;
            return this;
        }

        public AdjointCalculator With(MagneticTipper[,] tippers, TipperWeights[,] weights)
        {
            CheckSizes(tippers);
            CheckSizes(weights);

            _tippers = tippers;
            _tipperWeights = weights;
            return this;
        }

        public AdjointCalculator With(ElectricTipper[,] electricTippers, TipperWeights[,] weights)
        {
            CheckSizes(electricTippers);
            CheckSizes(weights);

            _electricTippers = electricTippers;
            _electricTipperWeights = weights;
            return this;
        }

        private AdjointCalculator WithField(Complex[,] field, int index)
        {
            CheckSizes(field);
            _fields[index] = field;
            return this;
        }

        public AdjointCalculator WithEx1(Complex[,] field) => WithField(field, Ex1);
        public AdjointCalculator WithEx2(Complex[,] field) => WithField(field, Ex2);
        public AdjointCalculator WithEy1(Complex[,] field) => WithField(field, Ey1);
        public AdjointCalculator WithEy2(Complex[,] field) => WithField(field, Ey2);
        public AdjointCalculator WithEz1(Complex[,] field) => WithField(field, Ez1);
        public AdjointCalculator WithEz2(Complex[,] field) => WithField(field, Ez2);
        public AdjointCalculator WithHx1(Complex[,] field) => WithField(field, Hx1);
        public AdjointCalculator WithHx2(Complex[,] field) => WithField(field, Hx2);
        public AdjointCalculator WithHy1(Complex[,] field) => WithField(field, Hy1);
        public AdjointCalculator WithHy2(Complex[,] field) => WithField(field, Hy2);
        public AdjointCalculator WithHz1(Complex[,] field) => WithField(field, Hz1);
        public AdjointCalculator WithHz2(Complex[,] field) => WithField(field, Hz2);

        private void CheckSizes<T>(T[,] data)
        {
            if (data == null) throw new ArgumentNullException(nameof(data));

            if (data.GetLength(0) != _localSize2D.Nx) throw new ArgumentOutOfRangeException(nameof(data));
            if (data.GetLength(1) != _localSize2D.Ny) throw new ArgumentOutOfRangeException(nameof(data));
        }

        public void Calculate(AllFieldsAtLevel fields, AnomalyCurrent source, PdcComponent type)
        {
            if (source.Nx != _localSize2D.Nx) throw new ArgumentOutOfRangeException(nameof(source));
            if (source.Ny != _localSize2D.Ny) throw new ArgumentOutOfRangeException(nameof(source));

            int counter = 0;


            //int i = 0;
            //int j = 0;
            for (int i = 0; i < _localSize2D.Nx; i++)
                for (int j = 0; j < _localSize2D.Ny; j++)
                {
                    var site = fields.GetSite(_lateral, i, j);

                    if (_impedances != null)
                    {
                        PartialDerivativesCalculator.CalculateFor(_impedances[i, j], _impedanceWeights[i, j], site, type);
                        AddToSource(source, counter);
                    }

                    if (_tippers != null)
                    {
                        PartialDerivativesCalculator.CalculateFor(_tippers[i, j], _tipperWeights[i, j], site, type);
                        AddToSource(source, counter);
                    }

                    if (_electricTippers != null)
                    {
                        PartialDerivativesCalculator.CalculateFor(_electricTippers[i, j], _electricTipperWeights[i, j], site, type);
                        AddToSource(source, counter);
                    }

                    for (int k = 0; k < _fields.Length; k++)
                    {
                        if (_fields[k] != null)
                        {
                            PartialDerivativesCalculator.CalculateForField(_fields[k][i, j], k, _fieldAccessors[k](site), type);
                            AddToSource(source, counter);
                        }
                    }

                    counter++;
                }
        }

        private unsafe void AddToSource(AnomalyCurrent source, int counter)
        {
            var ptr = source.Ptr;
            ptr[counter * 3] += PartialDerivativesCalculator.Result[0];
            ptr[counter * 3 + 1] += PartialDerivativesCalculator.Result[1];
            ptr[counter * 3 + 2] += PartialDerivativesCalculator.Result[2];
        }
    }
}
