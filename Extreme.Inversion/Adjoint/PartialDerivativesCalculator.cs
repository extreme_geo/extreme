﻿using System;
using System.Numerics;
using Extreme.Inversion.Observed;
using Extreme.Cartesian.Magnetotellurics;
using Porvem.Magnetotellurics;
using static System.Numerics.Complex;

// ReSharper disable InconsistentNaming

namespace Extreme.Inversion.Adjoint
{
    public enum PdcComponent
    {
        E1 = 0,
        E2 = 1,
        H1 = 2,
        H2 = 3,
    }

    public static class PartialDerivativesCalculator
    {
        private static Tensor c;
        private static ResponseFactor r;
        private static ResponseFactor s;
        private static Complex detS;

        private static readonly TensorMap ImpedenceMap;
        private static readonly TensorMap TipperMap;
        private static readonly TensorMap ElectricTipperMap;
        private static readonly TensorMap QuasiElectricMap;

        private static readonly TensorMap[] FieldMaps;

        private const int X = 0;
        private const int Y = 1;
        private const int Z = 2;

        public delegate Complex[] CalcPd();

        public static Complex[] Result = new Complex[3];

        static PartialDerivativesCalculator()
        {
            ImpedenceMap = new TensorMap(new[]
            {
                new[] {new Dp(X, CalcRx1), new Dp(Y, CalcRy1),},
                new[] {new Dp(X, CalcRx2), new Dp(Y, CalcRy2),},
                new[] {new Dp(X, CalcSx1), new Dp(Y, CalcSy1),},
                new[] {new Dp(X, CalcSx2), new Dp(Y, CalcSy2),},
            });

            TipperMap = new TensorMap(new[]
            {
                new Dp[] {},
                new Dp[] {},
                new[] {new Dp(X, CalcSx1), new Dp(Y, CalcSy1), new Dp(Z, CalcRx1),},
                new[] {new Dp(X, CalcSx2), new Dp(Y, CalcSy2), new Dp(Z, CalcRx2),},
            });

            ElectricTipperMap = new TensorMap(new[]
            {
                new[] {new Dp(X, CalcSx1), new Dp(Y, CalcSy1), new Dp(Z, CalcRx1),},
                new[] {new Dp(X, CalcSx2), new Dp(Y, CalcSy2), new Dp(Z, CalcRx2),},
                new Dp[] {},
                new Dp[] {},
            });

            QuasiElectricMap = new TensorMap(new[]
            {
                new[] {new Dp(X, CalcRx1), new Dp(Y, CalcRy1),},
                new[] {new Dp(X, CalcRx2), new Dp(Y, CalcRy2),},
                new[] {new Dp(X, CalcSx1), new Dp(Y, CalcSy1),},
                new[] {new Dp(X, CalcSx2), new Dp(Y, CalcSy2),},
            });

            FieldMaps = new[]
            {
                new TensorMap(new[] { new[] { new Dp(X, null) }, new Dp[0], new Dp[0], new Dp[0] }),
                new TensorMap(new[] { new Dp[0], new[] { new Dp(X, null) }, new Dp[0], new Dp[0] }),
                new TensorMap(new[] { new[] { new Dp(Y, null) }, new Dp[0], new Dp[0], new Dp[0] }),
                new TensorMap(new[] { new Dp[0], new[] { new Dp(Y, null) }, new Dp[0], new Dp[0] }),
                new TensorMap(new[] { new[] { new Dp(Z, null) }, new Dp[0], new Dp[0], new Dp[0] }),
                new TensorMap(new[] { new Dp[0], new[] { new Dp(Z, null) }, new Dp[0], new Dp[0] }),

                new TensorMap(new[] { new Dp[0], new Dp[0], new[] { new Dp(X, null) }, new Dp[0], }),
                new TensorMap(new[] { new Dp[0], new Dp[0], new Dp[0], new[] { new Dp(X, null) }, }),
                new TensorMap(new[] { new Dp[0], new Dp[0], new[] { new Dp(Y, null) }, new Dp[0], }),
                new TensorMap(new[] { new Dp[0], new Dp[0], new Dp[0], new[] { new Dp(Y, null) }, }),
                new TensorMap(new[] { new Dp[0], new Dp[0], new[] { new Dp(Z, null) }, new Dp[0], }),
                new TensorMap(new[] { new Dp[0], new Dp[0], new Dp[0], new[] { new Dp(Z, null) }, }),
            };
        }

        private static void CalcDet()
        {
            detS = s.X1 * s.Y2 - s.X2 * s.Y1;
        }

        public static Complex[] CalcRx1() => new[] { s.Y2 / detS, -s.X2 / detS, 0, 0 };
        public static Complex[] CalcRx2() => new[] { -s.Y1 / detS, s.X1 / detS, 0, 0 };
        public static Complex[] CalcRy1() => new[] { 0, 0, s.Y2 / detS, -s.X2 / detS };
        public static Complex[] CalcRy2() => new[] { 0, 0, -s.Y1 / detS, s.X1 / detS };

        public static Complex[] CalcSx1() => new[]
        {
            (-s.Y2*c.Xx)/detS, (r.X2 - s.Y2*c.Xy)/detS,
            (-s.Y2*c.Yx)/detS, (r.Y2 - s.Y2*c.Yy)/detS
        };

        public static Complex[] CalcSx2() => new[]
        {
            (s.Y1*c.Xx)/detS, (-r.X1 + s.Y1*c.Xy)/detS,
            (s.Y1*c.Yx)/detS, (-r.Y1 + s.Y1*c.Yy)/detS
        };

        public static Complex[] CalcSy1() => new[]
        {
            (-r.X2 + s.X2*c.Xx)/detS, (s.X2*c.Xy)/detS,
            (-r.Y2 + s.X2*c.Yx)/detS, (s.X2*c.Yy)/detS
        };

        public static Complex[] CalcSy2() => new[]
        {
            (r.X1 - s.X1*c.Xx)/detS, (-s.X1*c.Xy)/detS,
            (r.Y1 - s.X1*c.Yx)/detS, (-s.X1*c.Yy)/detS
        };


        private struct Dp
        {
            public Dp(int comp, CalcPd calcPd)
            {
                Comp = comp;
                CalcPd = calcPd;
            }

            public int Comp;
            public CalcPd CalcPd;

        }

        private class TensorMap
        {
            public Dp[][] Maps;

            public TensorMap(Dp[][] maps)
            {
                Maps = maps;
            }
        }

        private struct ResponseFactor
        {
            public Complex X1;
            public Complex X2;
            public Complex Y1;
            public Complex Y2;
        }


        public static void CalculateForField(Complex obs, int index, Complex calc, PdcComponent type)
        {
            Result[0] = 0;
            Result[1] = 0;
            Result[2] = 0;

            foreach (var map in FieldMaps[index].Maps[(int)type])
                Result[map.Comp] = Conjugate(calc - obs);
        }

        public static void CalculateFor(MagneticTipper obs, TipperWeights weights, AllFieldsAtSite site, PdcComponent type)
        {
            var calc = ResponseFunctionsCalculator.CalculateTipper(site);
            var o = new Tensor(obs.Zx, obs.Zy, 0, 0);

            c = new Tensor(calc.Zx, calc.Zy, 0, 0);

            r = new ResponseFactor()
            {
                X1 = site.FullH1.Z,
                Y1 = 0,
                X2 = site.FullH2.Z,
                Y2 = 0,
            };

            s = new ResponseFactor()
            {
                X1 = site.FullH1.X,
                Y1 = site.FullH1.Y,
                X2 = site.FullH2.X,
                Y2 = site.FullH2.Y,
            };

            var tensorWeights = new TensorWeights(weights.Zx, weights.Zy, 0, 0);

            CalcDet();
            CalcPart(TipperMap, tensorWeights, o, c, type);
        }

        public static void CalculateFor(ElectricTipper obs, TipperWeights weights, AllFieldsAtSite site, PdcComponent type)
        {
            var calc = ResponseFunctionsCalculator.CalculateElectricTipper(site);
            var o = new Tensor(obs.Zx, obs.Zy, 0, 0);

            c = new Tensor(calc.Zx, calc.Zy, 0, 0);

            r = new ResponseFactor()
            {
                X1 = site.FullE1.Z,
                Y1 = 0,
                X2 = site.FullE2.Z,
                Y2 = 0,
            };

            s = new ResponseFactor()
            {
                X1 = site.FullE1.X,
                Y1 = site.FullE1.Y,
                X2 = site.FullE2.X,
                Y2 = site.FullE2.Y,
            };

            var tensorWeights = new TensorWeights(weights.Zx, weights.Zy, 0, 0);

            CalcDet();
            CalcPart(ElectricTipperMap, tensorWeights, o, c, type);
        }

        public static void CalculateFor(ImpedanceTensor obs, TensorWeights weights, AllFieldsAtSite site, PdcComponent type)
        {
            c = ResponseFunctionsCalculator.CalculateImpedanceTensor(site);
            r = new ResponseFactor()
            {
                X1 = site.FullE1.X,
                Y1 = site.FullE1.Y,
                X2 = site.FullE2.X,
                Y2 = site.FullE2.Y,
            };

            s = new ResponseFactor()
            {
                X1 = site.FullH1.X,
                Y1 = site.FullH1.Y,
                X2 = site.FullH2.X,
                Y2 = site.FullH2.Y,
            };

            CalcDet();
            CalcPart(ImpedenceMap, weights, obs, c, type);
        }

        private static void CalcPart(TensorMap tensorMaps, TensorWeights weights, Tensor obs, Tensor calc, PdcComponent type)
        {
            Result[0] = 0;
            Result[1] = 0;
            Result[2] = 0;

            var xx = calc.Xx - obs.Xx;
            var xy = calc.Xy - obs.Xy;
            var yx = calc.Yx - obs.Yx;
            var yy = calc.Yy - obs.Yy;

            foreach (var map in tensorMaps.Maps[(int)type])
            {
                var pd = map.CalcPd();

                var part = Conjugate(xx) * weights.Xx * pd[0] +
                           Conjugate(xy) * weights.Xy * pd[1] +
                           Conjugate(yx) * weights.Yx * pd[2] +
                           Conjugate(yy) * weights.Yy * pd[3];

                Result[map.Comp] = part;
            }
        }
    }
}
