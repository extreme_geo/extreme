﻿using System;
using System.Numerics;
using Extreme.Cartesian.Core;
using Extreme.Cartesian.Forward;
using Extreme.Cartesian.Green.Tensor;
using Extreme.Core;
using UNF = Extreme.Cartesian.Forward.UnsafeNativeMethods;
using Extreme.Cartesian.Model;
using Extreme.Parallel;

namespace Extreme.Inversion
{
    public unsafe class AdjointToACalculator : ForwardSolverComponent
    {
        public AdjointToACalculator(ForwardSolver solver) : base(solver)
        {
        }

        public void CalculateAnomalyFieldE(GreenTensor gtE, AnomalyCurrent src, AnomalyCurrent dst)
        {
            CalculateAnomalyField(src, dst, gtE, electric: true);
        }

        public void CalculateAnomalyFieldH(GreenTensor gtH, AnomalyCurrent src, AnomalyCurrent dst)
        {
            CalculateAnomalyField(src, dst, gtH, electric: false);
        }

        private Complex* PrepareBackwardFactors()
        {
            var backwardFactors3 = MemoryProvider.AllocateComplex(3 * Model.Nz);

            var normalizeFactor = new Complex(1 / (4.0 * Model.Nx * Model.Ny), 0);

            var nz = Model.Nz;

            for (int k = 0; k < nz; k++)
            {
                var thickness = (double)Model.Anomaly.Layers[k].Thickness;

                backwardFactors3[k] = normalizeFactor / thickness;
                backwardFactors3[k + nz] = normalizeFactor / thickness;
                backwardFactors3[k + nz + nz] = normalizeFactor / thickness;
            }

            return backwardFactors3;
        }

        #region Level

        private void CalculateAnomalyField(AnomalyCurrent src, AnomalyCurrent dst, GreenTensor gt, bool electric)
        {
            var forward = src.Nz == 1 ? Pool.Plan3 : Pool.Plan3Nz;
            var backward = dst.Nz == 1 ? Pool.Plan3 : Pool.Plan3Nz;

            UNF.ClearBuffer(forward.Buffer2Ptr, forward.BufferLength);

			PrepareForReverseForwardFft (src, forward.Buffer2Ptr);
            Pool.ExecuteBackward(forward);

			MultForReverseFft (src.Nz, forward.Buffer2Ptr,Complex.ImaginaryOne);

				
            if (electric)
                ApplyGreenTensorAlongZForE(gt, forward.Buffer2Ptr, backward.Buffer1Ptr);
            else
                ApplyGreenTensorAlongZForH(gt, forward.Buffer2Ptr, backward.Buffer1Ptr);

			MultForReverseFft (dst.Nz, backward.Buffer1Ptr,-Complex.ImaginaryOne);
			Pool.ExecuteForward(backward);

			ExtractData(backward.Buffer1Ptr, dst);


        }


		private void CalculateAnomalyFieldOld(AnomalyCurrent src, AnomalyCurrent dst, GreenTensor gt, bool electric)
		{
			var forward = src.Nz == 1 ? Pool.Plan3 : Pool.Plan3Nz;
			var backward = dst.Nz == 1 ? Pool.Plan3 : Pool.Plan3Nz;


			UNF.ClearBuffer(forward.Buffer1Ptr, forward.BufferLength);

			PrepareForForwardFft(src, forward.Buffer1Ptr);
			Pool.ExecuteForward(forward);


			if (electric)
				ApplyGreenTensorAlongZForE(gt, forward.Buffer1Ptr, backward.Buffer2Ptr);
			else
				ApplyGreenTensorAlongZForH(gt, forward.Buffer1Ptr, backward.Buffer2Ptr);

			Pool.ExecuteBackward(backward);

			ExtractData_old(backward.Buffer2Ptr, dst);


		}


        #endregion

        private static void ApplyGreenTensorAlongZForE(GreenTensor gt, Complex* input, Complex* result)
        {
            int gtNz = gt.NTr * gt.NRc;
            int srcNz = gt.NTr;
            int dstNz = gt.NRc;

            int length = gt.Nx * gt.Ny;

            //for (int i = 0; i < length; i++)
            Iterate(length, i =>
            {
                int dstShift = i * 3 * dstNz;
                int srcShift = i * 3 * srcNz;
                int gtShift = i * gtNz;

                var dstX = result + dstShift;
                var dstY = result + dstShift + dstNz;
                var dstZ = result + dstShift + dstNz + dstNz;

                var srcX = input + srcShift;
                var srcY = input + srcShift + srcNz;
                var srcZ = input + srcShift + srcNz + srcNz;

                Zgemv(srcNz, dstNz, Complex.One, Complex.Zero, gt["xx"][gtShift], srcX, dstX);
                Zgemv(srcNz, dstNz, Complex.One, Complex.One, gt["xy"][gtShift], srcY, dstX);
                Zgemv(srcNz, dstNz, Complex.One, Complex.One, gt["zx"][gtShift], srcZ, dstX);

                Zgemv(srcNz, dstNz, Complex.One, Complex.Zero, gt["xy"][gtShift], srcX, dstY);
                Zgemv(srcNz, dstNz, Complex.One, Complex.One, gt["yy"][gtShift], srcY, dstY);
                Zgemv(srcNz, dstNz, Complex.One, Complex.One, gt["zy"][gtShift], srcZ, dstY);

                Zgemv(srcNz, dstNz, Complex.One, Complex.Zero, gt["xz"][gtShift], srcX, dstZ);
                Zgemv(srcNz, dstNz, Complex.One, Complex.One, gt["yz"][gtShift], srcY, dstZ);
                Zgemv(srcNz, dstNz, Complex.One, Complex.One, gt["zz"][gtShift], srcZ, dstZ);
                //}
            });
        }

        private static void ApplyGreenTensorAlongZForH(GreenTensor gt, Complex* input, Complex* result)
        {
            int gtNz = gt.NTr * gt.NRc;
            int srcNz = gt.NTr;
            int dstNz = gt.NRc;

            int length = gt.Nx * gt.Ny;

            Iterate(length, i =>
            //for (int i = 0; i < length; i++)
            {
                int dstShift = i * 3 * dstNz;
                int srcShift = i * 3 * srcNz;
                int gtShift = i * gtNz;

                var dstX = result + dstShift;
                var dstY = result + dstShift + dstNz;
                var dstZ = result + dstShift + dstNz + dstNz;

                var srcX = input + srcShift;
                var srcY = input + srcShift + srcNz;
                var srcZ = input + srcShift + srcNz + srcNz;

                Zgemv(srcNz, dstNz, -Complex.One, Complex.Zero, gt["xx"][gtShift], srcX, dstX);
                Zgemv(srcNz, dstNz, Complex.One, Complex.One, gt["yx"][gtShift], srcY, dstX);
                Zgemv(srcNz, dstNz, -Complex.One, Complex.One, gt["zx"][gtShift], srcZ, dstX);

                Zgemv(srcNz, dstNz, -Complex.One, Complex.Zero, gt["xy"][gtShift], srcX, dstY);
                Zgemv(srcNz, dstNz, Complex.One, Complex.One, gt["xx"][gtShift], srcY, dstY);
                Zgemv(srcNz, dstNz, Complex.One, Complex.One, gt["zy"][gtShift], srcZ, dstY);

                Zgemv(srcNz, dstNz, -Complex.One, Complex.Zero, gt["xz"][gtShift], srcX, dstZ);
                Zgemv(srcNz, dstNz, Complex.One, Complex.One, gt["yz"][gtShift], srcY, dstZ);
                //}
            });
        }


        private static void Iterate(int length, Action<int> action)
        {
            var options = MultiThreadUtils.CreateParallelOptions();
            System.Threading.Tasks.Parallel.For(0, length, options, action);
        }

        private static void PrepareForForwardFft(AnomalyCurrent src, Complex* inputPtr)
        {
            int nx = src.Nx;
            int ny = src.Ny;
            int nz = src.Nz;

            for (int i = 0; i < nx; i++)
                for (int j = 0; j < ny; j++)
                {
                    long shiftSrc = (i * ny + j) * 3 * nz;
                    long shiftDst = ((2 * nx - 1 - i) * ny * 2 + (2 * ny - 1 - j)) * 3 * nz;

                    Copy(3 * nz, src.Ptr + shiftSrc, inputPtr + shiftDst);
                }
        }


		private static void PrepareForReverseForwardFft(AnomalyCurrent src, Complex* inputPtr)
		{
			int nx = src.Nx;
			int ny = src.Ny;
			int nz = src.Nz;

			for (int i = 0; i < nx; i++)
				for (int j = 0; j < ny; j++)
				{
					long shiftSrc = (i * ny + j) * 3 * nz;
					long shiftDst = (i * ny * 2 +  j) * 3 * nz;

					Copy(3 * nz, src.Ptr + shiftSrc, inputPtr + shiftDst);
				}
		}

		private  void MultForReverseFft(int nz, Complex* inputPtr,Complex c)
		{
			int ny = 2 * Model.Ny;
			int shift = 0;
			int nx = 2 * Model.Nx;
			if (Mpi.IsParallel) {
				nx=Mpi.CalcLocalNxLength (2*Model.Nx);
				shift=nx*Mpi.Rank;
			}


			for (int i = 0; i < nx; i++) {
				var factorX = Complex.Exp (Math.PI * c * (i+shift) / Model.Nx);	
				for (int j = 0; j < ny; j++) {
					var factorXY = Complex.Exp (Math.PI * c * j / Model.Ny) * factorX;
					for (int k = 0; k < 3*nz; k++) {
						var index = k + 3 * nz * (j + i * ny);
						inputPtr [index] *= factorXY;
					}

				}
			}
		}


        private void ExtractData(Complex* outputPtr, AnomalyCurrent field)
        {
            var backwardFactors3 = PrepareBackwardFactors();

            int nx = field.Nx;
            int ny = field.Ny;
            int nz = field.Nz;
		
            for (int i = 0; i < nx; i++)
                for (int j = 0; j < ny; j++)
                {
                    long shiftDst = (i * ny + j) * 3L * nz;
                    long shiftSrc = (i * ny * 2L + j) * 3L * nz;

                    UNF.MultiplyElementwise(3*nz, backwardFactors3, outputPtr + shiftSrc, outputPtr + shiftSrc);
                    Zaxpy(3 * nz, Complex.One, outputPtr + shiftSrc, field.Ptr + shiftDst);
                }
			

            MemoryProvider.Release(backwardFactors3);

        }

		private void ExtractData_old(Complex* outputPtr, AnomalyCurrent field)
		{
			var backwardFactors3 = PrepareBackwardFactors();

			int nx = field.Nx;
			int ny = field.Ny;
			int nz = field.Nz;

			for (int i = 0; i < nx; i++)
				for (int j = 0; j < ny; j++)
				{
					long shiftDst = (i * ny + j) * 3L * nz;
					long shiftSrc = ((2L * nx - 1 - i) * ny * 2L + (2 * ny - 1 - j)) * 3L * nz;

					UNF.MultiplyElementwise(3*nz, backwardFactors3, outputPtr + shiftSrc, outputPtr + shiftSrc);
					Zaxpy(3 * nz, Complex.One, outputPtr + shiftSrc, field.Ptr + shiftDst);
				}


			MemoryProvider.Release(backwardFactors3);

		}



        private static void Zgemv(int n, int m, Complex a, Complex b, Complex* gt, Complex* src, Complex* dst)
            => UNF.ZgemvAtoO(n, m, &a, &b, gt, src, dst);

        private static void Zaxpy(long n, Complex alpha, Complex* x, Complex* y)
            => UNF.Zaxpy(n, alpha, x, y);

        private static void Copy(long length, Complex* ptr, Complex* complexPtr)
            => UNF.Zcopy(length, ptr, complexPtr);
    }
}
