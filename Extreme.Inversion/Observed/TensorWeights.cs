﻿namespace Extreme.Inversion.Observed
{
    public struct TensorWeights
    {
        public double Xx { get; }
        public double Xy { get; }
        public double Yx { get; }
        public double Yy { get; }

        public static TensorWeights EqualOne = new TensorWeights(1, 1, 1, 1);
        public static TensorWeights EqualZero = new TensorWeights(0, 0, 0, 0);

        public TensorWeights(double xx, double xy, double yx, double yy)
        {
            Xx = xx;
            Xy = xy;
            Yx = yx;
            Yy = yy;
        }





        public bool Equals(TensorWeights other)
        {
            return Xx.Equals(other.Xx) && Xy.Equals(other.Xy) && Yx.Equals(other.Yx) && Yy.Equals(other.Yy);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            return obj is TensorWeights && Equals((TensorWeights) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = Xx.GetHashCode();
                hashCode = (hashCode*397) ^ Xy.GetHashCode();
                hashCode = (hashCode*397) ^ Yx.GetHashCode();
                hashCode = (hashCode*397) ^ Yy.GetHashCode();
                return hashCode;
            }
        }
    }
}
