﻿using System;
using System.Collections.Generic;
using System.Numerics;
using Extreme.Cartesian.Magnetotellurics;
using Extreme.Core;
using Extreme.Inversion.Observed;
using Porvem.Magnetotellurics;

namespace Extreme.Inversion
{
    public class ResponsesWithWeightsPlainTextReader : PlainTextReaderBase<ResponsesWithWeightsAtLevel>
    {
        private static readonly string[] ImpedanceHeaderStrings =
        {
            "z_xx_re", "z_xx_im", "z_xx_w",
            "z_xy_re", "z_xy_im", "z_xy_w",
            "z_yx_re", "z_yx_im", "z_yx_w",
            "z_yy_re", "z_yy_im", "z_yy_w"
        };

        private static readonly string[] TipperHeaderStrings =
        {
            "w_zx_re", "w_zx_im", "w_zx_w",
            "w_zy_re", "w_zy_im", "w_zy_w"
        };

        public ResponsesWithWeightsPlainTextReader(LateralDimensions lateralDimensions, ObservationLevel[] observationLevels)
            : base(lateralDimensions, observationLevels)
        {
        }

        public static string GetInvertionObservedDataFileName(double frequency)
            => $"inv_freq{PlainTextExporter.FrequencyToString(frequency)}.dat";


        private static string ErrorMessage = "Response function's locations do not correspond to any observation leves";


        protected override ResponsesWithWeightsAtLevel FillResponsesForLevel(ObservationLevel level,
            int startIndex, string[] header, List<double[]> values)
        {
            var result = new ResponsesWithWeightsAtLevel(level, Dimensions);

            int index = HasImpedances(header);
            if (index != -1)
                FillImpedances(result, startIndex, index, values);

            index = HasTippers(header);
            if (index != -1)
                FillMagneticTippers(result, startIndex, index, values);

            return result;
        }

        private void FillImpedances(ResponsesWithWeightsAtLevel resps, int startRow, int startColumn, List<double[]> values)
        {
            var l = Dimensions;

            var tensors = new ImpedanceTensor[l.Nx, l.Ny];
            var weights = new TensorWeights[l.Nx, l.Ny];
            int counter = 0;

            var prevFreq = values[startRow][0];

            for (int i = 0; i < l.Nx; i++)
                for (int j = 0; j < l.Ny; j++)
                {
                    var vals = values[startRow + counter++];

                    var freq = vals[0];
                    if (freq != prevFreq)
                        throw new InvalidOperationException(ErrorMessage);

                    prevFreq = freq;

                    var xx = new Complex(vals[startColumn + 0], vals[startColumn + 1]);
                    var xxw = vals[startColumn + 2];
                    var xy = new Complex(vals[startColumn + 3], vals[startColumn + 4]);
                    var xyw = vals[startColumn + 5];
                    var yx = new Complex(vals[startColumn + 6], vals[startColumn + 7]);
                    var yxw = vals[startColumn + 8];
                    var yy = new Complex(vals[startColumn + 9], vals[startColumn + 10]);
                    var yyw = vals[startColumn + 11];

                    tensors[i, j] = new ImpedanceTensor(xx, xy, yx, yy);
                    weights[i, j] = new TensorWeights(xxw, xyw, yxw, yyw);
                }

            resps.ImpedanceTensors = tensors;
            resps.ImpedanceWeights = weights;
        }


        private void FillMagneticTippers(ResponsesWithWeightsAtLevel resps, int startRow, int startColumn, List<double[]> values)
        {
            var l = Dimensions;

            var tippers = new MagneticTipper[l.Nx, l.Ny];
            var weights = new TipperWeights[l.Nx, l.Ny];

            int counter = 0;

            for (int i = 0; i < l.Nx; i++)
                for (int j = 0; j < l.Ny; j++)
                {
                    var vals = values[startRow + counter++];

                    var zx = new Complex(vals[startColumn + 0], vals[startColumn + 1]);
                    var zxw = vals[startColumn + 2];
                    var zy = new Complex(vals[startColumn + 3], vals[startColumn + 4]);
                    var zyw = vals[startColumn + 2];

                    tippers[i, j] = new MagneticTipper(zx, zy);
                    weights[i, j] = new TipperWeights(zxw, zyw);
                }

            resps.Tippers = tippers;
            resps.TipperWeights = weights;
        }

        private int HasImpedances(string[] header)
            => Has(ImpedanceHeaderStrings, header);

        private int HasTippers(string[] header)
            => Has(TipperHeaderStrings, header);
    }
}
