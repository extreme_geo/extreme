﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using Extreme.Core;
using Extreme.Inversion.Observed;

namespace Extreme.Inversion
{
    public abstract class PlainTextReaderBase<T>
    {
        protected LateralDimensions Dimensions { get; }
        private ObservationLevel[] ObservationLevels { get; }

        protected PlainTextReaderBase(LateralDimensions lateralDimensions, ObservationLevel[] observationLevels)
        {
            if (lateralDimensions == null) throw new ArgumentNullException(nameof(lateralDimensions));
            if (observationLevels == null) throw new ArgumentNullException(nameof(observationLevels));

            Dimensions = lateralDimensions;
            ObservationLevels = observationLevels;
        }


        public List<T> Load(string path)
        {
            using (var sr = new StreamReader(path))
                return Load(sr);
        }

        private List<T> Load(StreamReader sr)
        {
            string[] header = ReadHeader(sr);

            var coordinates = new List<decimal[]>();
            var values = new List<double[]>();

            while (!sr.EndOfStream)
                ParseNextLine(sr, coordinates, values, header.Length - 4);

            var result = FillObservedContainer(header, coordinates, values);
            return result;
        }

        private static string ErrorMessage = "Response function's locations do not correspond to any observation leves";

        private List<T> FillObservedContainer(string[] header, List<decimal[]> coordinates, List<double[]> values)
        {
            var result = new List<T>();

            foreach (var level in ObservationLevels)
            {
                var startX = level.ShiftAlongX + Dimensions.CellSizeX / 2;
                var startY = level.ShiftAlongY + Dimensions.CellSizeY / 2;

                int startIndex = coordinates.FindIndex(c => (c[0] == startX) && (c[1] == startY) && (c[2] == level.Z));

                if (startIndex == -1)
                    throw new InvalidOperationException(ErrorMessage);

                CheckLastIndex(level, startIndex, coordinates);
                var allResponses = FillResponsesForLevel(level, startIndex, header, values);

                result.Add(allResponses);
            }

            return result;
        }

        private void CheckLastIndex(ObservationLevel level, int startIndex, List<decimal[]> coordinates)
        {
            var l = Dimensions;
            var length = l.Nx * l.Ny;
            var coord = coordinates[startIndex + length - 1];

            var endX = level.ShiftAlongX + l.CellSizeX * l.Nx - l.CellSizeX / 2;
            var endY = level.ShiftAlongY + l.CellSizeY * l.Ny - l.CellSizeY / 2;

            if (coord[0] != endX || coord[1] != endY || coord[2] != level.Z)
                throw new InvalidOperationException(ErrorMessage);
        }

        protected abstract T FillResponsesForLevel(ObservationLevel level,
            int startIndex, string[] header, List<double[]> values);

        protected int Has(string[] template, string[] header)
        {
            int startIndex = header.ToList().FindIndex(s => s == template[0]);

            if (startIndex == -1)
                return -1;

            for (int i = 0; i < template.Length; i++)
                if (header[startIndex + i] != template[i])
                    return -1;

            return startIndex - 4;
        }

        private void ParseNextLine(StreamReader sr, List<decimal[]> coordinates, List<double[]> values, int numberOfResponses)
        {
            var strs = sr.ReadLine()
                .Split(' ')
                .Where(s => !string.IsNullOrEmpty(s))
                .ToArray();


            var coord = new decimal[3];
            var vals = new double[numberOfResponses];

            for (int i = 0; i < coord.Length; i++)
                coord[i] = GetDecimal(strs[i]);

            for (int i = 0; i < vals.Length; i++)
                vals[i] = GetDouble(strs[i + coord.Length]);

            coordinates.Add(coord);
            values.Add(vals);
        }

        private static double GetDouble(string str)
            => double.Parse(str, NumberStyles.Float, CultureInfo.InvariantCulture);

        private static decimal GetDecimal(string str)
            => decimal.Parse(str);

        private static string[] ReadHeader(StreamReader sr)
        {
            return sr.ReadLine()?
                        .Split(' ')
                        .Where(s => !string.IsNullOrEmpty(s))
                        .Select(s => s.ToLower())
                        .ToArray();
        }
    }
}
