﻿using System;
using System.Collections.Generic;
using System.Numerics;
using Extreme.Core;
using Extreme.Inversion.Observed;
using Porvem.Magnetotellurics;

namespace Extreme.Inversion
{
    public class ObservedPlainTextReader : PlainTextReaderBase<AllResponsesAtLevel>
    {
        private static readonly string[] ImpedanceHeaderStrings = { "z_xx_re", "z_xx_im", "z_xy_re", "z_xy_im", "z_yx_re", "z_yx_im", "z_yy_re", "z_yy_im", };
        private static readonly string[] TipperHeaderStrings = { "w_zx_re", "w_zx_im", "w_zy_re", "w_zy_im" };
        private static readonly string[] ElectricTipperHeaderStrings = { "ew_zx_re", "ew_zx_im", "ew_zy_re", "ew_zy_im" };

        private static readonly string[] Aex1 = { "a_polx_ex_re", "a_polx_ex_im" };
        private static readonly string[] Aex2 = { "a_poly_ex_re", "a_poly_ex_im" };

        private static readonly string[] Aey1 = { "a_polx_ey_re", "a_polx_ey_im" };
        private static readonly string[] Aey2 = { "a_poly_ey_re", "a_poly_ey_im" };

        private static readonly string[] Aez1 = { "a_polx_ez_re", "a_polx_ez_im" };
        private static readonly string[] Aez2 = { "a_poly_ez_re", "a_poly_ez_im" };

        private static readonly string[] Ahx1 = { "a_polx_hx_re", "a_polx_hx_im" };
        private static readonly string[] Ahx2 = { "a_poly_hx_re", "a_poly_hx_im" };

        private static readonly string[] Ahy1 = { "a_polx_hy_re", "a_polx_hy_im" };
        private static readonly string[] Ahy2 = { "a_poly_hy_re", "a_poly_hy_im" };

        private static readonly string[] Ahz1 = { "a_polx_hz_re", "a_polx_hz_im" };
        private static readonly string[] Ahz2 = { "a_poly_hz_re", "a_poly_hz_im" };

        public ObservedPlainTextReader(LateralDimensions lateralDimensions, ObservationLevel[] observationLevels)
            : base(lateralDimensions, observationLevels)
        {
        }

        private static string ErrorMessage = "Response function's locations do not correspond to any observation leves";

        protected override AllResponsesAtLevel FillResponsesForLevel(ObservationLevel level,
            int startIndex, string[] header, List<double[]> values)
        {
            var result = new AllResponsesAtLevel(level, Dimensions);

            int index = HasImpedances(header);
            if (index != -1)
                result.ImpedanceTensors = FillImpedances(startIndex, index, values);

            index = HasTippers(header);
            if (index != -1)
                result.Tippers = FillMagneticTippers(startIndex, index, values);

            index = HasElectricTippers(header);
            if (index != -1)
                result.ElectricTippers = FillElectricTippers(startIndex, index, values);

            index = Has(Aex1, header);
            if (index != -1)
                result.Ex1 = FillField(startIndex, index, values);

            index = Has(Aex2, header);
            if (index != -1)
                result.Ex2 = FillField(startIndex, index, values);

            index = Has(Aey1, header);
            if (index != -1)
                result.Ey1 = FillField(startIndex, index, values);

            index = Has(Aey2, header);
            if (index != -1)
                result.Ey2 = FillField(startIndex, index, values);

            index = Has(Ahx1, header);
            if (index != -1)
                result.Hx1 = FillField(startIndex, index, values);

            index = Has(Ahx2, header);
            if (index != -1)
                result.Hx2 = FillField(startIndex, index, values);

            index = Has(Ahy1, header);
            if (index != -1)
                result.Hy1 = FillField(startIndex, index, values);

            index = Has(Ahy2, header);
            if (index != -1)
                result.Hy2 = FillField(startIndex, index, values);

            index = Has(Ahz1, header);
            if (index != -1)
                result.Hz1 = FillField(startIndex, index, values);

            index = Has(Ahz2, header);
            if (index != -1)
                result.Hz2 = FillField(startIndex, index, values);

            index = Has(Aez1, header);
            if (index != -1)
                result.Ez1 = FillField(startIndex, index, values);

            index = Has(Aez2, header);
            if (index != -1)
                result.Ez2 = FillField(startIndex, index, values);

            return result;
        }

        private ImpedanceTensor[,] FillImpedances(int startRow, int startColumn, List<double[]> values)
        {
            var l = Dimensions;

            var tensors = new ImpedanceTensor[l.Nx, l.Ny];
            int counter = 0;

            double prevFreq = values[startRow][0];


            for (int i = 0; i < l.Nx; i++)
                for (int j = 0; j < l.Ny; j++)
                {
                    var vals = values[startRow + counter++];

                    var freq = vals[0];
                    if (freq != prevFreq)
                        throw new InvalidOperationException(ErrorMessage);

                    prevFreq = freq;

                    var xx = new Complex(vals[startColumn + 0], vals[startColumn + 1]);
                    var xy = new Complex(vals[startColumn + 2], vals[startColumn + 3]);
                    var yx = new Complex(vals[startColumn + 4], vals[startColumn + 5]);
                    var yy = new Complex(vals[startColumn + 6], vals[startColumn + 7]);

                    tensors[i, j] = new ImpedanceTensor(xx, xy, yx, yy);
                }

            return tensors;
        }


        private MagneticTipper[,] FillMagneticTippers(int startRow, int startColumn, List<double[]> values)
        {
            var l = Dimensions;

            var tippers = new MagneticTipper[l.Nx, l.Ny];

            int counter = 0;

            for (int i = 0; i < l.Nx; i++)
                for (int j = 0; j < l.Ny; j++)
                {
                    var vals = values[startRow + counter++];

                    var xz = new Complex(vals[startColumn + 0], vals[startColumn + 1]);
                    var xy = new Complex(vals[startColumn + 2], vals[startColumn + 3]);

                    tippers[i, j] = new MagneticTipper(xz, xy);
                }

            return tippers;
        }

        private ElectricTipper[,] FillElectricTippers(int startRow, int startColumn, List<double[]> values)
        {
            var l = Dimensions;

            var tippers = new ElectricTipper[l.Nx, l.Ny];
            int counter = 0;

            for (int i = 0; i < l.Nx; i++)
                for (int j = 0; j < l.Ny; j++)
                {
                    var vals = values[startRow + counter++];

                    var xz = new Complex(vals[startColumn + 0], vals[startColumn + 1]);
                    var xy = new Complex(vals[startColumn + 2], vals[startColumn + 3]);

                    tippers[i, j] = new ElectricTipper(xz, xy);
                }

            return tippers;
        }


        private Complex[,] FillField(int startRow, int startColumn, List<double[]> values)
        {
            var l = Dimensions;

            var field = new Complex[l.Nx, l.Ny];
            int counter = 0;

            for (int i = 0; i < l.Nx; i++)
                for (int j = 0; j < l.Ny; j++)
                {
                    var vals = values[startRow + counter++];
                    field[i, j] = new Complex(vals[startColumn + 0], vals[startColumn + 1]);
                }

            return field;
        }


        private int HasImpedances(string[] header)
            => Has(ImpedanceHeaderStrings, header);

        private int HasTippers(string[] header)
            => Has(TipperHeaderStrings, header);

        private int HasElectricTippers(string[] header)
            => Has(ElectricTipperHeaderStrings, header);
    }
}
