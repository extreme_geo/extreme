﻿namespace Extreme.Inversion.Observed
{
    public struct TipperWeights
    {
        public double Zx { get; }
        public double Zy { get; }

        public static TipperWeights EqualOne = new TipperWeights(1, 1);
        public static TipperWeights EqualZero = new TipperWeights(0, 0);

        public TipperWeights(double zx, double zy)
        {
            Zx = zx;
            Zy = zy;
        }

        public bool Equals(TipperWeights other)
        {
            return Zx.Equals(other.Zx) && Zy.Equals(other.Zy);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            return obj is TipperWeights && Equals((TipperWeights) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (Zx.GetHashCode()*397) ^ Zy.GetHashCode();
            }
        }
    }
}
