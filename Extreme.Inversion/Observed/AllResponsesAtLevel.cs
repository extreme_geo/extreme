﻿using System.Numerics;
using Extreme.Core;
using Porvem.Magnetotellurics;

namespace Extreme.Inversion.Observed
{
    public class AllResponsesAtLevel
    {
        public LateralDimensions Lateral { get; }
        public ObservationLevel Level { get; }

        public AllResponsesAtLevel(ObservationLevel level, LateralDimensions lateral)
        {
            Lateral = lateral;
            Level = level;
        }

        public ImpedanceTensor[,] ImpedanceTensors { get; set; }
        public MagneticTipper[,] Tippers { get; set; }
        public ElectricTipper[,] ElectricTippers { get; set; }

        public Complex[,] Ex1 { get; set; }
        public Complex[,] Ex2 { get; set; }
        public Complex[,] Ey1 { get; set; }
        public Complex[,] Ey2 { get; set; }

        public Complex[,] Ez1 { get; set; }
        public Complex[,] Ez2 { get; set; }

        public Complex[,] Hx1 { get; set; }
        public Complex[,] Hx2 { get; set; }
        public Complex[,] Hy1 { get; set; }
        public Complex[,] Hy2 { get; set; }

        public Complex[,] Hz1 { get; set; }
        public Complex[,] Hz2 { get; set; }
    }
}
