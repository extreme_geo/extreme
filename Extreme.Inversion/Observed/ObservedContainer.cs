﻿using System.Collections.Generic;
using Extreme.Core;

namespace Extreme.Inversion.Observed
{
    public class ObservedContainer
    {
        private readonly Dictionary<double, List<ResponsesWithWeightsAtLevel>> _allResponses;
        public LateralDimensions Lateral { get; }
        public Size2D LocalSize { get; }
        public ObservationLevel[] Levels { get; }
        public IReadOnlyDictionary<double, List<ResponsesWithWeightsAtLevel>> AllResponses => _allResponses;

        public ObservedContainer(LateralDimensions lateral, ObservationLevel[] levels)
        {
            Lateral = lateral;
            LocalSize = new Size2D(lateral.Nx, lateral.Ny);
            Levels = levels;
            _allResponses = new Dictionary<double, List<ResponsesWithWeightsAtLevel>>();
        }

        public ObservedContainer(LateralDimensions lateral, Size2D localSize, ObservationLevel[] levels)
        {
            Lateral = lateral;
            LocalSize = localSize;
            _allResponses = new Dictionary<double, List<ResponsesWithWeightsAtLevel>>();
        }


        public void Add(double frequency, ResponsesWithWeightsAtLevel responses)
        {
            if (!_allResponses.ContainsKey(frequency))
                _allResponses.Add( frequency, new List<ResponsesWithWeightsAtLevel>());

            _allResponses[frequency].Add(responses);
        }

        public void AddRange(double frequency, IEnumerable<ResponsesWithWeightsAtLevel> responses)
        {
            if (!_allResponses.ContainsKey(frequency))
                _allResponses.Add(frequency, new List<ResponsesWithWeightsAtLevel>());

            _allResponses[frequency].AddRange(responses);
        }
    }
}
