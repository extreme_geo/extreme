﻿using System.Numerics;
using Extreme.Core;
using Porvem.Magnetotellurics;

namespace Extreme.Inversion.Observed
{
    public class ResponsesWithWeightsAtLevel
    {
        public LateralDimensions Lateral { get; }
        public ObservationLevel Level { get; }

        public ResponsesWithWeightsAtLevel(ObservationLevel level, LateralDimensions lateral)
        {
            Lateral = lateral;
            Level = level;
        }

        public ImpedanceTensor[,] ImpedanceTensors { get; set; }
        public TensorWeights [,] ImpedanceWeights { get; set; }

        public MagneticTipper[,] Tippers { get; set; }
        public TipperWeights[,] TipperWeights { get; set; }
    }
}
