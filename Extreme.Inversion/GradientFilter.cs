﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using static System.Math;


namespace Extreme.Inversion
{
    public class GradientFilter
    {
        public int Length { get; }
        public double[,,] X { get; }
        public double[,,] Y { get; }
        public double[,,] Z { get; }

        private GradientFilter(int length, double[,,] x, double[,,] y, double[,,] z)
        {
            Length = length;
            X = x;
            Y = y;
            Z = z;
        }

        public static GradientFilter GenerateFilter(double[] interpolationWeights, double[] derivativeWeights)
        {
            if (interpolationWeights.Length != derivativeWeights.Length)
                throw new ArgumentOutOfRangeException();

            var length = interpolationWeights.Length;

            var a = interpolationWeights;
            var b = derivativeWeights;

            var filterX = new double[length, length, length];
            var filterY = new double[length, length, length];
            var filterZ = new double[length, length, length];

            var normalizeFactor = b.Sum(val => Abs(val)) * (a.Sum() * a.Sum());

            for (int i = 0; i < length; i++)
                for (int j = 0; j < length; j++)
                    for (int k = 0; k < length; k++)
                    {
                        filterX[i, j, k] = (b[i] * a[j] * a[k]) / normalizeFactor;
                        filterY[i, j, k] = (a[i] * b[j] * a[k]) / normalizeFactor;
                        filterZ[i, j, k] = (a[i] * a[j] * b[k]) / normalizeFactor;
                    }


            return new GradientFilter(length, filterX, filterY, filterZ);
        }
    }
}
