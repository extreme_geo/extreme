﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Extreme.Cartesian.Model;

namespace Extreme.Inversion
{
    public class InversionDomain
    {
        private readonly CartesianModel _model;
        public int[,,] Mask { get; }

        public int Length { get; private set; }

        public InversionDomain(CartesianModel model)
        {
            _model = model;
            Mask = new int[model.Nx, model.Ny, model.Nz];
        }

        public void GroupBy(int xCells, int yCells, int zCells)
        {
            if (_model.Nx % xCells != 0) throw new ArgumentOutOfRangeException(nameof(xCells));
            if (_model.Ny % yCells != 0) throw new ArgumentOutOfRangeException(nameof(yCells));
            if (_model.Nz % zCells != 0) throw new ArgumentOutOfRangeException(nameof(zCells));

            int xStep = _model.Nx / xCells;
            int yStep = _model.Ny / yCells;
            int zStep = _model.Nz / zCells;

            int counter = 0;

            for (int i = 0; i < xCells; i++)
                for (int j = 0; j < yCells; j++)
                    for (int k = 0; k < zCells; k++)
                    {
                        int iStart = i * xStep;
                        int jStart = j * yStep;
                        int kStart = k * zStep;

                        for (int i1 = 0; i1 < xStep; i1++)
                            for (int j1 = 0; j1 < yStep; j1++)
                                for (int k1 = 0; k1 < zStep; k1++)
                                    Mask[iStart + i1, jStart + j1, kStart + k1] = counter;

                        counter++;
                    }

            Length = xCells * yCells * zCells;
        }
    }

}
