﻿using System;
using System.IO;

namespace Extreme.Inversion
{
    public unsafe class InversionDomainWriter
    {
        public static void SaveInversionDomain(string fileName, int length, double* sigmaM)
        {
            using (var bw = new BinaryWriter(File.OpenWrite(fileName)))
            {
                SaveInversionDomain(bw, length, sigmaM);
            }

        }

        public static void SaveInversionDomain(BinaryWriter bw, int length, double* sigmaM)
        {
            bw.Write(length);

            for (int i = 0; i < length; i++)
                bw.Write(Math.Exp(sigmaM[i]));
        }

        public static double[] LoadInversionDomain(string fileName)
        {
            using (var br = new BinaryReader(File.OpenRead(fileName)))
            {
                return LoadInversionDomain(br);
            }
        }

        public static double[] LoadInversionDomain(BinaryReader br)
        {
            int length = br.ReadInt32();

            var result = new double[length];

            for (int i = 0; i < length; i++)
                result[i] = br.ReadDouble();

            return result;
        }


        public static void SaveInversionDomainMask(string fileName, InversionDomain domain)
        {
            using (var bw = new BinaryWriter(File.OpenWrite(fileName)))
            {
                SaveInversionDomainMask(bw, domain);
            }
        }

        public static void SaveInversionDomainMask(BinaryWriter bw, InversionDomain domain)
        {
            int nx = domain.Mask.GetLength(0);
            int ny = domain.Mask.GetLength(1);
            int nz = domain.Mask.GetLength(2);

            bw.Write(nx);
            bw.Write(ny);
            bw.Write(nz);

            for (int i = 0; i < nx; i++)
                for (int j = 0; j < ny; j++)
                    for (int k = 0; k < nz; k++)
                        bw.Write(domain.Mask[i, j, k]);
        }


        public static int[,,] LoadInversionDomainMask(string fileName)
        {
            using (var br = new BinaryReader(File.OpenRead(fileName)))
            {
                return LoadInversionDomainMask(br);
            }
        }

        public static int[,,] LoadInversionDomainMask(BinaryReader br)
        {
            int nx = br.ReadInt32();
            int ny = br.ReadInt32();
            int nz = br.ReadInt32();

            int[,,] mask = new int[nx,ny,nz];

            for (int i = 0; i < nx; i++)
                for (int j = 0; j < ny; j++)
                    for (int k = 0; k < nz; k++)
                        mask[i, j, k] = br.ReadInt32();

            return mask;
        }
    }
}
