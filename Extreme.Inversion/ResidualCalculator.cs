﻿using System;
using System.Numerics;
using Extreme.Cartesian.Magnetotellurics;
using Extreme.Core;
using Extreme.Inversion.Observed;
using Porvem.Magnetotellurics;

namespace Extreme.Inversion
{
    public class ResidualCalculator
    {
        private readonly LateralDimensions _lateral;
        private readonly Size2D _localSize;

        public ResidualCalculator(LateralDimensions lateral, Size2D locaSize)
        {
            _lateral = lateral;
            _localSize = locaSize;
        }

        public double CalculateResidualSqr(
            ImpedanceTensor[,] impedances,
            TensorWeights[,] weights,
            AllFieldsAtLevel fields)
        {
            return CalculateResidualSqr(impedances, weights, fields, CalculateResidualSqrForTensor);
        }

        public double CalculateResidualSqr(
            MagneticTipper[,] tippers,
            TipperWeights[,] weights,
            AllFieldsAtLevel fields)
        {
            return CalculateResidualSqr(tippers, weights, fields, CalculateResidualSqrForTipper);
        }


        private double CalculateResidualSqr<TU, TV>(TU[,] resps, TV[,] weights, 
            AllFieldsAtLevel fields,
            Func<TU, TV, AllFieldsAtSite, double> calcRsd)
        {
            double residualSqr = 0;
            
            for (int i = 0; i < _localSize.Nx; i++)
                for (int j = 0; j < _localSize.Ny; j++)
                {
                    var site = fields.GetSite(_lateral, i, j);

                    residualSqr += calcRsd(resps[i, j], weights[i, j], site);
                }

            return residualSqr;
        }

        private static double CalculateResidualSqrFor(Complex obs, Complex calc)
        {
            var val = calc - obs;
            return (val * val).Magnitude;
        }

        private static double CalculateResidualSqrForTipper(MagneticTipper obs, TipperWeights weight, AllFieldsAtSite site )
        {
            var calc = ResponseFunctionsCalculator.CalculateTipper(site);
            return CalculateResidualSqrFor(obs, calc, weight);
        }

        private static double CalculateResidualSqrFor(Tipper obs, Tipper calc, TipperWeights weight)
        {
            var zx = calc.Zx - obs.Zx;
            var zy = calc.Zy - obs.Zy;

            return ((zx * zx).Magnitude * weight.Zx +
                    (zy * zy).Magnitude * weight.Zy);
        }
        
        private static double CalculateResidualSqrForTensor(Tensor obs, TensorWeights weight, AllFieldsAtSite site)
        {
            var calc = ResponseFunctionsCalculator.CalculateImpedanceTensor(site);

            var xx = calc.Xx - obs.Xx;
            var xy = calc.Xy - obs.Xy;
            var yx = calc.Yx - obs.Yx;
            var yy = calc.Yy - obs.Yy;

            return ((xx * xx).Magnitude * weight.Xx +
                    (xy * xy).Magnitude * weight.Xy +
                    (yx * yx).Magnitude * weight.Yx +
                    (yy * yy).Magnitude * weight.Yy);
        }
    }
}
