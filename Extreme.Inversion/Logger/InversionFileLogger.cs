﻿using Extreme.Core;

namespace Extreme.Inversion.Logger
{
    public class InversionFileLogger : FileLogger
    {
        public InversionFileLogger(string fileName, bool rewrite) : base(fileName, rewrite)
        {
        }

        protected override bool Filter(int logLevel)
            => !((InversionLogLevel)logLevel).HasFlag(InversionLogLevel.Inversion);
    }
}
