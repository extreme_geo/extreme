﻿using Extreme.Core;

namespace Extreme.Inversion.Logger
{
    public class InversionConsoleLogger : ConsoleLogger
    {
        public InversionConsoleLogger() : base("Inversion")
        {
        }

        protected override bool Filter(int logLevel)
            => !((InversionLogLevel)logLevel).HasFlag(InversionLogLevel.Inversion);
    }
}
