﻿using Extreme.Core;
using Extreme.Core.Logger;

namespace Extreme.Inversion.Logger
{
    public static class InversionLoggerHelper
    {
        public static void WriteStatus(this ILogger logger, string message)
        {
            logger.Write((int)LogLevel.Status | (int)InversionLogLevel.Inversion, message);
        }

        public static void WriteWarning(this ILogger logger, string message)
        {
            logger.Write((int)LogLevel.Warning | (int)InversionLogLevel.Inversion, message);
        }

        public static void WriteError(this ILogger logger, string message)
        {
            logger.Write((int)LogLevel.Error | (int)InversionLogLevel.Inversion, message);
        }
    }
}
