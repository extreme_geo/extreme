﻿using System;
using System.Linq;
using Extreme.Cartesian.Model;
using static System.Math;

namespace Extreme.Inversion
{
    public class StabilizerCalculator
    {
        private readonly CartesianModel _model;
        private readonly StabilizerParameters _parameters;

        private readonly int _nx;
        private readonly int _ny;
        private readonly int _nz;

        private readonly double _sigmaTop;
        private readonly double _sigmaBottom;
        private readonly double[] _bkgSigmas;

        private readonly double[,,] _dx;
        private readonly double[,,] _dy;
        private readonly double[,,] _dz;

        private readonly double _volume;

        public StabilizerCalculator(CartesianModel model, StabilizerParameters parameters)
        {
            if (model == null) throw new ArgumentNullException(nameof(model));
            if (parameters == null) throw new ArgumentNullException(nameof(parameters));

            _model = model;
            _parameters = parameters;

            _nx = model.Anomaly.LocalSize.Nx;
            _ny = model.Anomaly.LocalSize.Ny;
            _nz = model.Nz;

            var bkgDepths = ModelUtils.GetBackgroundLayerDepths(model.Section1D);

            _sigmaTop = Log(GetSigmaTop(_model, bkgDepths));
            _sigmaBottom = Log(GetSigmaBot(_model, bkgDepths));

            _bkgSigmas = new double[_nz];

            for (int k = 0; k < _bkgSigmas.Length; k++)
                _bkgSigmas[k] = Log(GetBackgoundSigma(k));

            _dx = new double[_nx + 1, _ny, _nz];
            _dy = new double[_nx, _ny + 1, _nz];
            _dz = new double[_nx, _ny, _nz + 1];

            _volume = (double)(_model.LateralDimensions.CellSizeX * _model.LateralDimensions.Nx *
                               _model.LateralDimensions.CellSizeY * _model.LateralDimensions.Ny *
                               _model.Anomaly.Layers.Sum(l => l.Thickness));
        }


        private double GetBackgoundSigma(int k)
            => ModelUtils.FindCorrespondingBackgroundLayer(
                _model.Section1D, _model.Anomaly.Layers[k]).Sigma;

        private double GetSigmaTop(CartesianModel model, decimal[] bkgDepths)
        {
            var topAl = model.Anomaly.Layers.First();
            var topBlIndex = ModelUtils.FindCorrespondingBackgroundLayerIndex(model.Section1D, topAl);

            if (topAl.Depth > bkgDepths[topBlIndex])
                return model.Section1D[topBlIndex].Sigma;

            if (topBlIndex - 1 == 0)
                return model.Section1D[1].Sigma;

            if (model.Section1D[topBlIndex - 1].Sigma == 0)
                return model.Section1D[topBlIndex].Sigma;

            return model.Section1D[topBlIndex - 1].Sigma;
        }

        private double GetSigmaBot(CartesianModel model, decimal[] bkgDepths)
        {
            var botAl = model.Anomaly.Layers.Last();
            var botBlIndex = ModelUtils.FindCorrespondingBackgroundLayerIndex(model.Section1D, botAl);

            if (botAl.Depth + botAl.Thickness < bkgDepths[botBlIndex] + model.Section1D[botBlIndex].Thickness)
                return model.Section1D[botBlIndex].Sigma;

            return model.Section1D[botBlIndex + 1].Sigma;
        }

        private delegate double F(int i, int j, int k, double[,,] d);

        private delegate double G(int i, int j, int k, double sigma);


        public unsafe void CalculateDerivatives(double[,,] sigma, int shift, double* currentM, InversionDomain inversionDomain)
        {
            CalculateGradientAlongX(sigma, shift, currentM, inversionDomain, _dx);
            CalculateGradientAlongY(sigma, _dy);
            CalculateGradientAlongZ(sigma, _dz);
        }

        public double CalculateDefaultStabilizer(double[,,] sigma)
            => CalculateStabilizer(sigma, DefaultFx, DefaultFy, DefaultFz, DefaultG);

        public double[,,] CalculateDefaultStabilizerGradient(double[,,] sigma)
            => CalculateStabilizerGradient(sigma, DefaultDerivativeFx, DefaultDerivativeFy, DefaultDerivativeFz, DefaultDerivativeG);

        private double CalculateStabilizer(double[,,] sigma, F fx, F fy, F fz, G g)
        {
            double stabilizer = 0;

            for (int i = 0; i < _nx; i++)
                for (int j = 0; j < _ny; j++)
                    for (int k = 0; k < _nz; k++)
                        stabilizer += fx(i, j, k, _dx) +
                                      fy(i, j, k, _dy) +
                                      fz(i, j, k, _dz);

            for (int j = 0; j < _ny; j++)
                for (int k = 0; k < _nz; k++)
                    stabilizer += fx(_nx, j, k, _dx);

            for (int i = 0; i < _nx; i++)
                for (int k = 0; k < _nz; k++)
                    stabilizer += fy(i, _ny, k, _dy);

            for (int i = 0; i < _nx; i++)
                for (int j = 0; j < _ny; j++)
                    stabilizer += fz(i, j, _nz, _dz);

            stabilizer *= _parameters.Beta;

            for (int i = 0; i < _nx; i++)
                for (int j = 0; j < _ny; j++)
                    for (int k = 0; k < _nz; k++)
                        stabilizer += g(i, j, k, sigma[i, j, k]);

            stabilizer /= _volume;

            return stabilizer;
        }

        private double[,,] CalculateStabilizerGradient(double[,,] sigma, F fx, F fy, F fz, G g)
        {
            double[,,] grad = new double[_nx, _ny, _nz];

            for (int i = 0; i < _nx; i++)
                for (int j = 0; j < _ny; j++)
                    for (int k = 0; k < _nz; k++)
                    {
                        int iIndex = i == 0 ? _nx : i - 1;
                        int jIndex = j == 0 ? _ny : j - 1;
                        int kIndex = k == 0 ? _nz : k - 1;

                        var value = fx(iIndex, j, k, _dx) * _dx[iIndex, j, k] - fx(i, j, k, _dx) * _dx[i, j, k] +
                                    fx(iIndex, j, k, _dx) * _dy[i, jIndex, k] - fy(i, j, k, _dy) * _dy[i, j, k] +
                                    fz(i, j, kIndex, _dz) * _dz[i, j, kIndex] - fz(i, j, k, _dz) * _dz[i, j, k];

                        //if (double.IsInfinity(value))
                        //  Console.WriteLine($"[{i},{j},{k}]: {_dx[iIndex, j, k]} {_dy[i, jIndex, k]} {_dz[i, j, kIndex]}");

                        value = 2 * _parameters.Beta * value + g(i, j, k, sigma[i, j, k]);

                        grad[i, j, k] = value / _volume;
                    }

            return grad;
        }


        private double DefaultG(int i, int j, int k, double sigma)
        {
            var cellVolume = (double)(_model.LateralDimensions.CellSizeX *
                    _model.LateralDimensions.CellSizeY *
                    _model.Anomaly.Layers[k].Thickness);

            var bkg = _bkgSigmas[k];

            return (Exp(2 * (sigma - bkg)) + _parameters.Alpha * Exp(2 * (bkg - sigma))) * cellVolume;
        }

        private double DefaultFx(int i, int j, int k, double[,,] d)
        {
            var h = (double)(_model.LateralDimensions.CellSizeY * _model.Anomaly.Layers[k].Thickness);
            var val = d[i, j, k];

            if (i < _nx - 1)
                return 2 * h * val * val;

            return h * val * val;
        }

        private double DefaultFy(int i, int j, int k, double[,,] d)
        {
            var h = (double)(_model.LateralDimensions.CellSizeX * _model.Anomaly.Layers[k].Thickness);
            var val = d[i, j, k];

            if (j < _ny - 1)
                return 2 * h * val * val;

            return h * val * val;
        }

        private double DefaultFz(int i, int j, int k, double[,,] d)
        {
            var h = (double)(_model.LateralDimensions.CellSizeX * _model.LateralDimensions.CellSizeY);
            var val = d[i, j, k];

            if (j < _nz - 1)
                return 2 * h * val * val;

            return h * val * val;
        }

        private double DefaultDerivativeG(int i, int j, int k, double sigma)
        {
            var cellVolume = (double)(_model.LateralDimensions.CellSizeX *
                    _model.LateralDimensions.CellSizeY *
                    _model.Anomaly.Layers[k].Thickness);

            var bkg = _bkgSigmas[k];

            return 2 * (Exp(2 * (sigma - bkg)) - _parameters.Alpha * Exp(2 * (bkg - sigma))) * cellVolume;
        }

        private double DefaultDerivativeFx(int i, int j, int k, double[,,] d)
        {
            var h = (double)(_model.LateralDimensions.CellSizeY * _model.Anomaly.Layers[k].Thickness);

            if (i < _nx - 1)
                return 2 * h;

            return h;
        }

        private double DefaultDerivativeFy(int i, int j, int k, double[,,] d)
        {
            var h = (double)(_model.LateralDimensions.CellSizeX * _model.Anomaly.Layers[k].Thickness);

            if (j < _ny - 1)
                return 2 * h;

            return h;
        }

        private double DefaultDerivativeFz(int i, int j, int k, double[,,] d)
        {
            var h = (double)(_model.LateralDimensions.CellSizeX * _model.LateralDimensions.CellSizeY);

            if (j < _nz - 1)
                return 2 * h;

            return h;
        }

        private void CalculateGradientAlongZ(double[,,] sigma, double[,,] dz)
        {
            for (int i = 0; i < _nx; i++)
                for (int j = 0; j < _ny; j++)
                {
                    for (int k = 0; k < _nz - 1; k++)
                        dz[i, j, k] = sigma[i, j, k + 1] - sigma[i, j, k];

                    dz[i, j, _nz - 1] = _sigmaBottom - sigma[i, j, _nz - 1];
                    dz[i, j, _nz] = sigma[i, j, 0] - _sigmaTop;
                }
        }

        private void CalculateGradientAlongY(double[,,] sigma, double[,,] dy)
        {
            for (int k = 0; k < _nz; k++)
            {
                var backgroundSigma = _bkgSigmas[k];

                for (int i = 0; i < _nx; i++)
                {
                    for (int j = 0; j < _ny - 1; j++)
                        dy[i, j, k] = sigma[i, j + 1, k] - sigma[i, j, k];

                    dy[i, _ny - 1, k] = backgroundSigma - sigma[i, _ny - 1, k];
                    dy[i, _ny, k] = sigma[i, 0, k] - backgroundSigma;
                }
            }
        }

        private unsafe void CalculateGradientAlongX(double[,,] sigma, int shift, double* currentM, InversionDomain inversionDomain, double[,,] dx)
        {
            for (int k = 0; k < _nz; k++)
            {
                var backgroundSigma = _bkgSigmas[k];

                for (int j = 0; j < _ny; j++)
                {
                    for (int i = 0; i < _nx - 1; i++)
                        dx[i, j, k] = sigma[i + 1, j, k] - sigma[i, j, k];

                    double nextSigma;
                    if (LastPart(shift, _model))
                        nextSigma = backgroundSigma;
                    else
                    {
                        var index = inversionDomain.Mask[_nx - 1 + shift, j, k];
                        nextSigma = currentM[index];
                    }
                    dx[_nx - 1, j, k] = nextSigma - sigma[_nx - 1, j, k];

                    double prevSigma;
                    if (shift == 0)
                        prevSigma = backgroundSigma;
                    else
                    {
                        var index = inversionDomain.Mask[shift - 1, j, k];
                        prevSigma = currentM[index];
                    }
                    dx[_nx, j, k] = sigma[0, j, k] - prevSigma;
                }
            }
        }

        private bool LastPart(int shift, CartesianModel model)
        {
            return (model.Nx - model.Anomaly.LocalSize.Nx) == shift;
        }
    }
}
