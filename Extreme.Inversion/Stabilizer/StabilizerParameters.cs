﻿namespace Extreme.Inversion
{
    public class StabilizerParameters
    {
        public double Lambda { get; set; }
        public double Alpha{ get; set; }
        public double Beta { get; set; }
    }
}
