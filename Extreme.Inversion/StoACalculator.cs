﻿using Extreme.Cartesian.Core;
using Extreme.Cartesian.Forward;
using Extreme.Cartesian.Green;
using Extreme.Cartesian.Green.Scalar;
using Extreme.Cartesian.Green.Tensor;
using Extreme.Cartesian.Green.Tensor.Impl;
using Extreme.Cartesian.Logger;
using Extreme.Cartesian.Model;
using Extreme.Core;

namespace Extreme.Cartesian.Ionosphere
{
    public class StoACalculator : ToOCalculator
    {
        private const decimal SourceThickness = 100m;

        private readonly TensorPlan _templateTensorPlan;
        private readonly ScalarPlansCreater _plansCreater;

        public StoACalculator(ForwardSolver solver) : base(solver)
        {
            _plansCreater = new ScalarPlansCreater(Model.LateralDimensions, HankelCoefficients.LoadN40(), Solver.Settings.NumberOfHankels);
            _templateTensorPlan = DistibutedUtils.CreateTensorPlanAtoO(Mpi, Model, 1, Model.Nz);
        }

        private GreenTensor CalculateGreenTensorElectric(SourceLayer source)
        {
            Logger.WriteStatus("\t\tscalar");
            var plan = _plansCreater.CreateForSourceToAnomaly(source);
            var transceivers = TranceiverUtils.CreateSourceToAnomaly(Model.Anomaly, SourceThickness, source);
            var scalarCalculator = GreenScalarCalculator.NewStoAElectricCalculator(Logger, Model);
            var scalars = scalarCalculator.Calculate(plan, transceivers);

            using (var segments = ScalarSegments.AllocateAndConvert(MemoryProvider, plan, scalars))
            {
                var calc = new AtoOLevelGreenTensorCalculator(Logger, Model, MemoryProvider);
                var tensorPlan = new TensorPlan(_templateTensorPlan, source);
                Logger.WriteStatus("\t\ttensor");
                var gt = calc.CalculateAtoOElectric(segments, MemoryLayoutOrder.AlongVertical, tensorPlan);
                Logger.WriteStatus("\t\tfft");
                PerformGreenTensorFft(gt);

                return gt;
            }
        }

        public void CalculateAnomalyFieldE(SourceLayer layer, AnomalyCurrent jQ, AnomalyCurrent dst)
        {
            using (var greenTensor = CalculateGreenTensorElectric(layer))
                CalculateAnomalyFieldE(jQ, dst, greenTensor);
        }

        private unsafe void PerformGreenTensorFft(GreenTensor gt)
        {
            foreach (var component in gt.GetAvailableComponents())
            {
                var ptr = gt[component].Ptr;
                long length = gt.Nx * gt.Ny * gt.NTr * gt.NRc;
                Copy(length, ptr, Pool.Plan1Nz.Buffer1Ptr);
                Pool.ExecuteForward(Pool.Plan1Nz);
                Copy(length, Pool.Plan1Nz.Buffer1Ptr, ptr);
            }
        }
    }
}
