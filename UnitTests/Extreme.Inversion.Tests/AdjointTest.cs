﻿using System;
using System.Numerics;
using Extreme.Cartesian;
using Extreme.Cartesian.Magnetotellurics;
using Extreme.Core;
using Extreme.Inversion.Adjoint;
using Extreme.Inversion.Observed;
using Porvem.Magnetotellurics;
using Porvem.Tests.Utils;
using Xunit;

namespace Extreme.Inversion.Tests
{
    public class AdjointTest
    {
        private readonly AllFieldsAtSite _site;
        private const double Koeff = 1.001;
        private readonly DoubleEqualityComparer _comparer = new DoubleEqualityComparer(1E-6);

        public AdjointTest()
        {
            _site = new AllFieldsAtSite(new ObservationSite(0, 0, 0))
            {
                NormalE1 = new ComplexVector(new Complex(1, 0), new Complex(0, 0), new Complex(0, 0)),
                NormalE2 = new ComplexVector(new Complex(0, 0), new Complex(1, 0), new Complex(0, 0)),
                NormalH1 = new ComplexVector(new Complex(0, 0), new Complex(712.774258467068, 557.952468935657), new Complex(0, 0)),
                NormalH2 = new ComplexVector(new Complex(-712.774258467068, -557.952468935657), new Complex(0, 0), new Complex(0, 0)),

                AnomalyE1 = new ComplexVector(new Complex(-0.78348359338126, -0.0693878714355624), new Complex(-0.0193910175275797, 2.11860723165255E-05), new Complex(-4.93682816324336E-12, 5.70522309199826E-12)),
                AnomalyE2 = new ComplexVector(new Complex(-0.00193443626363009, 0.000963106537170058), new Complex(-0.578144695389375, -0.0227848377324526), new Complex(9.98697423381948E-13, -3.49821172796921E-13)),
                AnomalyH1 = new ComplexVector(new Complex(48.2854704939365, 15.7688187063827), new Complex(-69.7721538691631, -32.9342185097576), new Complex(22.782600826525, 19.3645055067532)),
                AnomalyH2 = new ComplexVector(new Complex(-329.70761725038, -173.529670928826), new Complex(-8.85326501037637, -3.7246653247907), new Complex(-187.647145937917, -202.820184302026)),
            };
        }

        [Fact]
        public void TestAllTableImpedance()
        {
            TestWithImpedance(() => ModifyE1X(_site, Koeff), PartialDerivativesCalculator.CalcRx1, s => s.FullE1.X);
            TestWithImpedance(() => ModifyE2X(_site, Koeff), PartialDerivativesCalculator.CalcRx2, s => s.FullE2.X);
            TestWithImpedance(() => ModifyE1Y(_site, Koeff), PartialDerivativesCalculator.CalcRy1, s => s.FullE1.Y);
            TestWithImpedance(() => ModifyE2Y(_site, Koeff), PartialDerivativesCalculator.CalcRy2, s => s.FullE2.Y);

            TestWithImpedance(() => ModifyH1X(_site, Koeff), PartialDerivativesCalculator.CalcSx1, s => s.FullH1.X);
            TestWithImpedance(() => ModifyH2X(_site, Koeff), PartialDerivativesCalculator.CalcSx2, s => s.FullH2.X);
            TestWithImpedance(() => ModifyH1Y(_site, Koeff), PartialDerivativesCalculator.CalcSy1, s => s.FullH1.Y);
            TestWithImpedance(() => ModifyH2Y(_site, Koeff), PartialDerivativesCalculator.CalcSy2, s => s.FullH2.Y);
        }

        [Fact]
        public void TestAllTableTippers()
        {
            TestWithTippers(() => ModifyH1X(_site, Koeff), PartialDerivativesCalculator.CalcSx1, s => s.FullH1.X);
            TestWithTippers(() => ModifyH2X(_site, Koeff), PartialDerivativesCalculator.CalcSx2, s => s.FullH2.X);
            TestWithTippers(() => ModifyH1Y(_site, Koeff), PartialDerivativesCalculator.CalcSy1, s => s.FullH1.Y);
            TestWithTippers(() => ModifyH2Y(_site, Koeff), PartialDerivativesCalculator.CalcSy2, s => s.FullH2.Y);
            TestWithTippers(() => ModifyH1Z(_site, Koeff), PartialDerivativesCalculator.CalcRx1, s => s.FullH1.Z);
            TestWithTippers(() => ModifyH2Z(_site, Koeff), PartialDerivativesCalculator.CalcRx2, s => s.FullH2.Z);
        }

        private void TestWithImpedance(Func<AllFieldsAtSite> modifier, PartialDerivativesCalculator.CalcPd calcPd, Func<AllFieldsAtSite, Complex> getComp)
        {
            var orgImp = ResponseFunctionsCalculator.CalculateImpedanceTensor(_site);

            var modSite = modifier();
            var modImp = ResponseFunctionsCalculator.CalculateImpedanceTensor(modSite);

            PartialDerivativesCalculator.CalculateFor(orgImp, TensorWeights.EqualOne, _site, 0);
            var pd = calcPd();

            var dImp = new[]
            {
                modImp.Xx - orgImp.Xx,
                modImp.Xy - orgImp.Xy,
                modImp.Yx - orgImp.Yx,
                modImp.Yy - orgImp.Yy,
            };

            for (int i = 0; i < dImp.Length; i++)
            {
                var c1 = dImp[i] / (getComp(modSite) - getComp(_site));
                var c2 = pd[i];
                Compare(c1, c2, $"{calcPd.Method.Name} {i}");
            }
        }

        private void TestWithTippers(Func<AllFieldsAtSite> modifier, PartialDerivativesCalculator.CalcPd calcPd, Func<AllFieldsAtSite, Complex> getComp)
        {
            var orgTip = ResponseFunctionsCalculator.CalculateTipper(_site);

            var modSite = modifier();
            var modTip = ResponseFunctionsCalculator.CalculateTipper(modSite);

            PartialDerivativesCalculator.CalculateFor(orgTip, TipperWeights.EqualOne, _site, 0);
            var pd = calcPd();

            var dImp = new[]
            {
                modTip.Zx - orgTip.Zx,
                modTip.Zy - orgTip.Zy,
                0,
                0,
            };

            for (int i = 0; i < dImp.Length; i++)
            {
                var c1 = dImp[i] / (getComp(modSite) - getComp(_site));
                var c2 = pd[i];
                Compare(c1, c2, $"{calcPd.Method.Name} {i}");
            }
        }


        private void Compare(Complex c1, Complex c2, string message = "")
        {
            Console.WriteLine($"{message} {c1}, {c2}, {c2 - c1}");

            Assert.Equal(c1.Real, c2.Real, _comparer);
            Assert.Equal(c1.Imaginary, c2.Imaginary, _comparer);
        }


        private AllFieldsAtSite ModifyE1X(AllFieldsAtSite site, double koeff)
        {
            var ae = site.AnomalyE1;
            var ne = site.NormalE1;

            return new AllFieldsAtSite(site)
            {
                NormalE1 = new ComplexVector(ne.X * koeff, ne.Y, ne.Z),
                AnomalyE1 = new ComplexVector(ae.X * koeff, ae.Y, ae.Z),
            };
        }

        private AllFieldsAtSite ModifyE2X(AllFieldsAtSite site, double koeff)
        {
            var ae = site.AnomalyE2;
            var ne = site.NormalE2;

            return new AllFieldsAtSite(site)
            {
                NormalE2 = new ComplexVector(ne.X * koeff, ne.Y, ne.Z),
                AnomalyE2 = new ComplexVector(ae.X * koeff, ae.Y, ae.Z),
            };
        }

        private AllFieldsAtSite ModifyE1Y(AllFieldsAtSite site, double koeff)
        {
            var ae = site.AnomalyE1;
            var ne = site.NormalE1;

            return new AllFieldsAtSite(site)
            {
                NormalE1 = new ComplexVector(ne.X, ne.Y * koeff, ne.Z),
                AnomalyE1 = new ComplexVector(ae.X, ae.Y * koeff, ae.Z),
            };
        }

        private AllFieldsAtSite ModifyE2Y(AllFieldsAtSite site, double koeff)
        {
            var ae = site.AnomalyE2;
            var ne = site.NormalE2;

            return new AllFieldsAtSite(site)
            {
                NormalE2 = new ComplexVector(ne.X, ne.Y * koeff, ne.Z),
                AnomalyE2 = new ComplexVector(ae.X, ae.Y * koeff, ae.Z),
            };
        }

        private AllFieldsAtSite ModifyH1X(AllFieldsAtSite site, double koeff)
        {
            var ae = site.AnomalyH1;
            var ne = site.NormalH1;

            return new AllFieldsAtSite(site)
            {
                NormalH1 = new ComplexVector(ne.X * koeff, ne.Y, ne.Z),
                AnomalyH1 = new ComplexVector(ae.X * koeff, ae.Y, ae.Z),
            };
        }

        private AllFieldsAtSite ModifyH2X(AllFieldsAtSite site, double koeff)
        {
            var ae = site.AnomalyH2;
            var ne = site.NormalH2;

            return new AllFieldsAtSite(site)
            {
                NormalH2 = new ComplexVector(ne.X * koeff, ne.Y, ne.Z),
                AnomalyH2 = new ComplexVector(ae.X * koeff, ae.Y, ae.Z),
            };
        }

        private AllFieldsAtSite ModifyH1Y(AllFieldsAtSite site, double koeff)
        {
            var ae = site.AnomalyH1;
            var ne = site.NormalH1;

            return new AllFieldsAtSite(site)
            {
                NormalH1 = new ComplexVector(ne.X, ne.Y * koeff, ne.Z),
                AnomalyH1 = new ComplexVector(ae.X, ae.Y * koeff, ae.Z),
            };
        }

        private AllFieldsAtSite ModifyH2Y(AllFieldsAtSite site, double koeff)
        {
            var ae = site.AnomalyH2;
            var ne = site.NormalH2;

            return new AllFieldsAtSite(site)
            {
                NormalH2 = new ComplexVector(ne.X, ne.Y * koeff, ne.Z),
                AnomalyH2 = new ComplexVector(ae.X, ae.Y * koeff, ae.Z),
            };
        }

        private AllFieldsAtSite ModifyH1Z(AllFieldsAtSite site, double koeff)
        {
            var ae = site.AnomalyH1;
            var ne = site.NormalH1;

            return new AllFieldsAtSite(site)
            {
                NormalH1 = new ComplexVector(ne.X, ne.Y, ne.Z * koeff),
                AnomalyH1 = new ComplexVector(ae.X, ae.Y, ae.Z * koeff),
            };
        }

        private AllFieldsAtSite ModifyH2Z(AllFieldsAtSite site, double koeff)
        {
            var ae = site.AnomalyH2;
            var ne = site.NormalH2;

            return new AllFieldsAtSite(site)
            {
                NormalH2 = new ComplexVector(ne.X, ne.Y, ne.Z * koeff),
                AnomalyH2 = new ComplexVector(ae.X, ae.Y, ae.Z * koeff),
            };
        }
    }
}
