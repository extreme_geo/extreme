﻿using System.IO;
using Extreme.Inversion.Project;
using Xunit;

namespace Extreme.Inversion.Tests
{

    public class InversionProjectSerializationTest
    {
        private const string TestCategory = @"Inversion Project serialization";

        [Fact]
        public void GeneralTest()
        {
            var project = CreateTestProject();
            var loadedProject = SaveAndLoadProject(project);

            CompareProjects(project, loadedProject);
        }

        private static InversionProject SaveAndLoadProject(InversionProject project)
        {
            var tempFileName = Path.GetTempFileName();
            //var tempFileName = @"z:\test.xproj";
            return SaveAndLoadProject(project, tempFileName);
        }

        private static InversionProject SaveAndLoadProject(InversionProject project, string fileName)
        {
            InversionProjectSerializer.Save(project, fileName);
            return InversionProjectSerializer.Load(fileName);
        }

        private void CompareProjects(InversionProject expected, InversionProject actual)
        {
            CompareInversionSettings(expected.InversionSettings, actual.InversionSettings);
        }

        private void CompareInversionSettings(InversionSettings expected, InversionSettings actual)
        {
            Assert.NotNull(expected);
            Assert.NotNull(actual);

            Assert.Equal(expected.Responses.Impedance, actual.Responses.Impedance);
            Assert.Equal(expected.Responses.Tipper, actual.Responses.Tipper);

            Assert.Equal(expected.Lbfgs.Memory, actual.Lbfgs.Memory);
            Assert.Equal(expected.Lbfgs.Tolerance, actual.Lbfgs.Tolerance);

            Assert.Equal(expected.ObservedData.Path, actual.ObservedData.Path);


            Assert.Equal(expected.Stabilizer.Alpha, actual.Stabilizer.Alpha);
            Assert.Equal(expected.Stabilizer.Beta, actual.Stabilizer.Beta);
            Assert.Equal(expected.Stabilizer.Lambda, actual.Stabilizer.Lambda);

            Assert.Equal(expected.Domain.Nx, actual.Domain.Nx);
            Assert.Equal(expected.Domain.Ny, actual.Domain.Ny);
            Assert.Equal(expected.Domain.Nz, actual.Domain.Nz);

            Assert.Equal(expected.StartModel.MaskFile, actual.StartModel.MaskFile);
            Assert.Equal(expected.StartModel.InversionDomainFile, actual.StartModel.InversionDomainFile);
        }

        private InversionProject CreateTestProject()
        {
            var freqs = CreateFrequencies();
            var project = InversionProject.NewWithFrequencies(freqs);
            var settings = project.InversionSettings;

            settings.ObservedData.Path = @"ExperimentalData\obs1";

            settings.Lbfgs.Memory = 17;
            settings.Lbfgs.Tolerance = 7.34E-6;

            settings.Stabilizer.Lambda = 7.32;
            settings.Stabilizer.Alpha = 777.7;
            settings.Stabilizer.Beta = 0.00088;

            settings.Domain.Nx = 10;
            settings.Domain.Ny = 23;
            settings.Domain.Nz = 15;
            settings.Domain.Type = "simple";

            settings.StartModel.MaskFile = @"mask_11-29-2015_16-15-38.bin";
            settings.StartModel.InversionDomainFile = @"inv_dom_00001_11-29-2015_16-11-08.bin";

            settings.Responses.Tipper = true;
            settings.Responses.Impedance = false;

            return project;
        }

        private static double[] CreateFrequencies()
        {
            return new[] { 0.1 };
        }
    }
}
