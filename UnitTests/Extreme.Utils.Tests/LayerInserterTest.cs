﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Porvem.Cartesian.Forward.Magnetotellurics;
using Porvem.Cartesian.Green.Scalar;
using Porvem.Core.Model;

namespace Porvem.Utils.Tests
{
    [TestClass]
    public class LayerInserterTest
    {
        private class TestLayer : Layer1D
        {
            private readonly int _value;

            public TestLayer(decimal thickness, int value)
                : base(thickness)
            {
                _value = value;
            }

            public int Value
            {
                get { return _value; }
            }
        }

        [TestMethod]
        public void InsertLayer()
        {
            var layers = new[]
                {
                    new TestLayer (10, 1),
                    new TestLayer (20, 2),
                    new TestLayer (40, 3),
                    new TestLayer (80, 4),
                };

            var inserter = new LayerInserter<TestLayer>(100, layers, (l, t) => new TestLayer(t, l.Value));

            bool result = inserter.InsertLayer(126m);

            Assert.IsTrue(result);


            var newLayers = inserter.GetCurrentLayers();

            Assert.AreEqual(newLayers.Length, 5);

            // layers

            Assert.AreEqual(newLayers[0].Thickness, 10);

            Assert.AreEqual(newLayers[1].Thickness, 16);
            Assert.AreEqual(newLayers[2].Thickness, 4);
            Assert.AreEqual(newLayers[3].Thickness, 40);
            Assert.AreEqual(newLayers[4].Thickness, 80);

            Assert.AreEqual(newLayers[0].Value, 1);

            Assert.AreEqual(newLayers[1].Value, 2);
            Assert.AreEqual(newLayers[2].Value, 2);
            Assert.AreEqual(newLayers[3].Value, 3);
            Assert.AreEqual(newLayers[4].Value, 4);


        }
    }
}
