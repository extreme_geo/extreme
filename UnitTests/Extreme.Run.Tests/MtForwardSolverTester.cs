﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using Extreme.Cartesian.Core;
using Extreme.Cartesian.Fft;
using Extreme.Cartesian.FftW;
using Extreme.Cartesian.Forward;
using Extreme.Cartesian.Model;
using Extreme.Core;
using Extreme.Model;
using Extreme.Cartesian.Magnetotellurics;
using Porvem.Tests.Utils;
using Xunit;

namespace Extreme.Run
{
    public class MtForwardSolverTester
    {
        private readonly DoubleEqualityComparer _comparer = new DoubleEqualityComparer(1E-7);

        public MtForwardSolverTester()
        {
            AssemblyInit();
        }

        private static void AssemblyInit()
            => Environment.CurrentDirectory = @"..\..\..\..\NativeDll";

        [Fact]
        public void TestSimpleModelNotChanging()
        {
            var project = CreateXProject();
            var logger = GetLogger();
            var model = CreateModel();

            var eFields = new List<FieldsAtLevelCalculatedEventArgs>();
            var hFields = new List<FieldsAtLevelCalculatedEventArgs>();

            using (var memoryProvider = new IntelMklMemoryProvider())
            {
                FftBuffersPool.PrepareBuffersForModel(model, memoryProvider);

                var omegaModel = OmegaModelBuilder.BuildOmegaModel(model, project.Frequencies.First());
                var solver = new Mt3DForwardSolver(logger, memoryProvider, project.ForwardSettings);

                solver.EFieldsAtLevelCalculated += (s, field) => eFields.Add(field);
                solver.HFieldsAtLevelCalculated += (s, field) => hFields.Add(field);

                solver
                    .With(project.ObservationLevels)
                    .With(project.ObservationSites);

                solver.Solve(omegaModel);

                AnalyzeResultsNotChange(eFields, hFields);
            }
        }

        private static ForwardProject CreateXProject()
        {
            var project = ForwardProject.NewWithFrequencies(0.01);
            project.Extreme.WithObservations(new ObservationLevel(0));
            SetForwardSettings(project.ForwardSettings);

            return project;
        }


        private static void SetForwardSettings(ForwardSettings settings)
        {
            settings.Residual = 1E-8f;
            settings.NumberOfHankels = 1;
            settings.OuterBufferLength = 30;
            settings.InnerBufferLength = 10;
        }

        private static CartesianModel CreateModel()
        {
            var creater = new SimpleCommemi3DModelCreater();

            var nonMeshed = creater.CreateNonMeshedModel(1, 0.01);
            var converter = new NonMeshedToCartesianModelConverter(nonMeshed);
            var mesh = new MeshParameters(4, 8, 1000, 2000, 5000, 10000);
            var cartesian = converter.Convert(mesh);

            return cartesian;
        }

        private ILogger GetLogger()
        {
            return new ConsoleLogger();
        }

        private void AnalyzeResultsNotChange(List<FieldsAtLevelCalculatedEventArgs> eFields, List<FieldsAtLevelCalculatedEventArgs> hFields)
        {
            var avgE = CalculateAverage(eFields.First());
            var avgH = CalculateAverage(hFields.First());

            const double avgEMagExpected = 0.381480774904232;
            const double avgHMagExpected = 290.4279276;

            const double avgEPhsExpected = -0.0458631103099262;
            const double avgHPhsExpected = 0.670339340437314;

            Assert.Equal(avgEMagExpected, avgE.Magnitude, _comparer);
            Assert.Equal(avgHMagExpected, avgH.Magnitude, _comparer);

            Assert.Equal(avgEPhsExpected, avgE.Phase, _comparer);
            Assert.Equal(avgHPhsExpected, avgH.Phase, _comparer);
        }

        private unsafe Complex CalculateAverage(FieldsAtLevelCalculatedEventArgs args)
        {
            var af = args.AnomalyField;
            var nf = args.NormalField;

            Complex result = 0;

            var aX = VerticalLayerAccessor.NewX(af, 0);
            var aY = VerticalLayerAccessor.NewY(af, 0);
            var aZ = VerticalLayerAccessor.NewZ(af, 0);

            var nX = VerticalLayerAccessor.NewX(nf, 0);
            var nY = VerticalLayerAccessor.NewY(nf, 0);
            var nZ = VerticalLayerAccessor.NewZ(nf, 0);

            for (int i = 0; i < af.Nx; i++)
                for (int j = 0; j < af.Ny; j++)
                {
                    var x = aX[i, j] + nX[i, j];
                    var y = aY[i, j] + nY[i, j];
                    var z = aZ[i, j] + nZ[i, j];

                    result += (x + y + z) / (3 * af.Nx * af.Ny);
                }

            return result;
        }
    }
}
