﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;

using Xunit;

using Extreme.Cartesian.Project;
using Extreme.Core;

namespace Extreme.Cartesian.Tests
{

    
    public class ExtremeProjectSerializationTest
    {
        private const string TestCategory = @"Cartesian project serialization";

        private const string OutputPath = "output";
        private const string ModelPath = "model.xmodel";

        [Fact]//,TestCategory(TestCategory), TestMethod]
        public void GeneralTest()
        {
            var project = CreateTestProject();
            var loadedProject = SaveAndLoadProject(project);

            CompareProjects(project, loadedProject);
        }

        [Fact, Trait(TestCategory, "")]//        [TestCategory(TestCategory), TestMethod]
        public void ObservationLevelsTest()
        {
            var project = CreateTestProjectWithObservations();
            var loadedProject = SaveAndLoadProject(project);

            CompareObservationLevels(project.ObservationLevels.ToArray(), loadedProject.ObservationLevels.ToArray());
        }

        [Fact]// [TestCategory(TestCategory), TestMethod]
        public void SourceLayersTest()
        {
            var project = CreateTestProjectWithSources();
            var loadedProject = SaveAndLoadProject(project);

            CompareSourceLayers(project.SourceLayers.ToArray(), loadedProject.SourceLayers.ToArray());
        }

        [Fact]//[TestCategory(TestCategory), TestMethod]
        public void ObservationSites()
        {
            var project = CreateTestProjectWithObservations();
            var loadedProject = SaveAndLoadProject(project);

            CompareObservationSites(loadedProject.ObservationSites.ToArray(), loadedProject.ObservationSites.ToArray());
        }

        private void CompareObservationSites(ObservationSite[] expected, ObservationSite[] actual)
        {
            Assert.Equal(expected.Length, actual.Length);

            for (int i = 0; i < expected.Length; i++)
            {
                Assert.Equal(expected[i].X, actual[i].X);
                Assert.Equal(expected[i].Y, actual[i].Y);
                Assert.Equal(expected[i].Z, actual[i].Z);
                Assert.Equal(expected[i].Name, actual[i].Name);
            }
        }

        private void CompareSourceLayers(SourceLayer[] expected, SourceLayer[] actual)
        {
            Assert.Equal(expected.Length, actual.Length);

            for (int i = 0; i < expected.Length; i++)
            {
                Assert.Equal(expected[i].Z, actual[i].Z);

                Assert.Equal(expected[i].ShiftAlongX, actual[i].ShiftAlongX);
                Assert.Equal(expected[i].ShiftAlongY, actual[i].ShiftAlongY);
            }
        }

        private void CompareObservationLevels(ObservationLevel[] expected, ObservationLevel[] actual)
        {
            Assert.Equal(expected.Length, actual.Length);

            for (int i = 0; i < expected.Length; i++)
            {
                Assert.Equal(expected[i].Z, actual[i].Z);

                Assert.Equal(expected[i].ShiftAlongX, actual[i].ShiftAlongX);
                Assert.Equal(expected[i].ShiftAlongY, actual[i].ShiftAlongY);

                Assert.Equal(expected[i].Name, actual[i].Name);
            }
        }

        private static ExtremeProject SaveAndLoadProject(ExtremeProject project)
        {
            var tempFileName = Path.GetTempFileName();
            return SaveAndLoadProject(project, tempFileName);
        }

        private static ExtremeProject SaveAndLoadProject(ExtremeProject project, string fileName)
        {
            var writer = new ProjectWriter(project);
            var xdoc = writer.ToXDocument();
            xdoc.Save(fileName);

            var reader = new ProjectReader();
            return reader.FromXDocument(XDocument.Load(fileName));
        }

        private void CompareProjects(ExtremeProject expected, ExtremeProject actual)
        {
            CompareFrequencies(expected.Frequencies, actual.Frequencies);

            Assert.Equal(expected.ModelFile, actual.ModelFile);
            Assert.Equal(expected.ResultsPath, actual.ResultsPath);
        }

        private void CompareFrequencies(IEnumerable<double> expected, IEnumerable<double> actual)
        {
            CompareArrays(expected, actual);
        }

        private void CompareArrays(IEnumerable<double> expected, IEnumerable<double> actual)
        {
            var exp = expected.ToArray();
            var act = actual.ToArray();

            Assert.Equal(exp.Length, act.Length);

            for (int i = 0; i < exp.Length; i++)
                Assert.Equal(exp[i], act[i]);
        }




        private ExtremeProject CreateTestProject()
        {
            var freqs = CreateFrequencies();

            var project = new ExtremeProject(freqs);

            project
                .WithModelFile(ModelPath)
                .WithResultsPath(OutputPath);

            return project;
        }

        private ExtremeProject CreateTestProjectWithObservations()
        {
            var freqs = CreateFrequencies();

            var project = new ExtremeProject(freqs);

            project
                .WithModelFile(ModelPath)
                .WithResultsPath(OutputPath)
                .WithObservations(CreateObservationLevels())
                .WithObservations(CreateObservationSites());

            return project;
        }

        private ExtremeProject CreateTestProjectWithSources()
        {
            var freqs = CreateFrequencies();

            var project = new ExtremeProject(freqs);

            project
                .WithModelFile(ModelPath)
                .WithResultsPath(OutputPath)
                .WithSources(CreateSourceLayers());

            return project;
        }

        private static ObservationLevel[] CreateObservationLevels()
        {
            return new[]
            {
                new ObservationLevel(410, -232, 3333, "test1"),
                new ObservationLevel(60, 435, 222),
                new ObservationLevel(500),
                new ObservationLevel(-2000, "test4"),
            };
        }

        private static SourceLayer[] CreateSourceLayers()
        {
            return new[]
            {
                new SourceLayer(-20000, -30000, 0),
                new SourceLayer(20000, -30000, 1000),
            };
        }

        private static ObservationSite[] CreateObservationSites()
        {
            return new[]
            {
                new ObservationSite(410, -232, 3333, "test1"),
                new ObservationSite(60, 435, 222),
            };
        }


        private static double[] CreateFrequencies()
        {
            return new[]
            {
                1,
                0.1,
                0.01,
                0.003
            };
        }
    }
}
