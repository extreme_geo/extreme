﻿using System.IO;
using Extreme.Cartesian.Forward;
using Xunit;

namespace Extreme.Cartesian.Tests
{

    public class ForwardProjectSerializationTest
    {
        private const string TestCategory = @"Cartesian Forward Project serialization";

        private const int NumberOfHankels = 15;
        private const int MaxNumberOfIterations = 667;
        private const float Residual = 0.013f;

        [Fact]// [TestCategory(TestCategory), TestMethod]
        public void GeneralTest()
        {
            var project = CreateTestProject();
            var loadedProject = SaveAndLoadProject(project);

            CompareProjects(project, loadedProject);
        }

        private static ForwardProject SaveAndLoadProject(ForwardProject project)
        {
            var tempFileName = Path.GetTempFileName();
            return SaveAndLoadProject(project, tempFileName);
        }

        private static ForwardProject SaveAndLoadProject(ForwardProject project, string fileName)
        {
            ForwardProjectSerializer.Save(project, fileName);
            return ForwardProjectSerializer.Load(fileName);
        }

        private void CompareProjects(ForwardProject expected, ForwardProject actual)
        {
            Assert.Equal(expected.ForwardSettings.Residual, actual.ForwardSettings.Residual);
            Assert.Equal(expected.ForwardSettings.MaxRepeatsNumber, actual.ForwardSettings.MaxRepeatsNumber);
            Assert.Equal(expected.ForwardSettings.NumberOfHankels, actual.ForwardSettings.NumberOfHankels);
        }

        private ForwardProject CreateTestProject()
        {
            var freqs = CreateFrequencies();

            var project = ForwardProject.NewWithFrequencies(freqs);

            project.ForwardSettings.MaxRepeatsNumber = MaxNumberOfIterations;
            project.ForwardSettings.Residual = Residual;
            project.ForwardSettings.NumberOfHankels = NumberOfHankels;

            return project;
        }

        private static double[] CreateFrequencies()
        {
            return new[]
            {
                1,
                0.1,
                0.01,
                0.003
            };
        }
    }
}
