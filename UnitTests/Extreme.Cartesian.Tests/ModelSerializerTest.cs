﻿using System;
using System.Collections.Generic;
using System.IO;

using Xunit;

using Extreme.Cartesian.Model;
using Extreme.Core;

using Extreme.Model;
using Extreme.Model.SimpleCommemi3D;
using Porvem.Tests.Utils;

namespace Extreme.Cartesian.Tests
{

    public class ModelSerializerTest
    {
        private const string TestCategory = @"Cartesian model serialization";
        private static readonly Size2D AnomalySize = new Size2D(9, 18);
        private readonly DoubleEqualityComparer _comparer = new DoubleEqualityComparer(1E-7);

        private static string GetFileName()
        {
            return Path.GetTempFileName();
            //return @"z:\set.xmodel";
        }



        [Fact()]
        public void NaserSettings1Test()
        {
            var mesh = new MeshParameters(40, 40, 40);
            var settings = new NaserModelSettings(mesh)
                .WithTopConductivity(0.1)
                .WithLeftConductivity(777)
                .WithRightConductivity(0.3);


            var fileName = GetFileName();
            ModelSettingsSerializer.SaveToXml(fileName, settings);
            Assert.True(ModelSettingsSerializer.IsModelNaser(fileName));
            var actual = ModelSettingsSerializer.LoadNaserFromXml(fileName);

            Assert.Equal(settings.LeftConductivity, actual.LeftConductivity);
            Assert.Equal(settings.RightConductivity, actual.RightConductivity);
            Assert.Equal(settings.TopConductivity, actual.TopConductivity);
        }

        [Fact()]
        public void OneBlockSettings1Test()
        {
            var settings = CreateOneBlockModel();

            var fileName = GetFileName();
            ModelSettingsSerializer.SaveToXml(fileName, settings);
            Assert.True(ModelSettingsSerializer.IsModelOneBlock(fileName));
            var actual = ModelSettingsSerializer.LoadOneBlockFromXml(fileName);

            CompareOneBlockModel(settings, actual);
        }

        private static OneBlockModelSettings CreateOneBlockModel()
        {
            var mesh = new MeshParameters(40, 40, 40);
            var settings = new OneBlockModelSettings(mesh)
                .WithAnomalySizeX(80000)
                .WithAnomalySizeY(80000)
                .WithAnomalySizeZ(20000);

            var layers = new[]
            {
                Sigma1DLayer.Air(),
                Sigma1DLayer.Layer(10000, 0.1),
                Sigma1DLayer.Layer(8000, 0.01),
                Sigma1DLayer.HalfSpace(10),
            };

            settings.Section1D = new CartesianSection1D(1000, layers);
            return settings;
        }


        [Fact()]
        public void OneBlockSettings2Test()
        {
            var settings = CreateOneBlockModel()
                .WithConductivity(7);

            var fileName = GetFileName();
            ModelSettingsSerializer.SaveToXml(fileName, settings);
            Assert.True(ModelSettingsSerializer.IsModelOneBlock(fileName));
            var actual = ModelSettingsSerializer.LoadOneBlockFromXml(fileName);

            CompareOneBlockModel(settings, actual);
        }

        private void CompareOneBlockModel(OneBlockModelSettings settings, OneBlockModelSettings actual)
        {
            CompareModelSettings(settings, actual);

            CompareBackgroundLayers(settings.Section1D, actual.Section1D);

            Assert.Equal(settings.AnomalyStartDepth, actual.AnomalyStartDepth);
            Assert.Equal(settings.AnomalySizeX, actual.AnomalySizeX);
            Assert.Equal(settings.AnomalySizeY, actual.AnomalySizeY);
            Assert.Equal(settings.AnomalySizeZ, actual.AnomalySizeZ);
            Assert.Equal(settings.Conductivity, actual.Conductivity);
        }

        [Fact()]
        public void CommemiSettings1Test()
        {
            var mesh = new MeshParameters(10, 10, 20);
            var settings = new CommemiModelSettings(mesh);

            settings
                .WithAnomalySizeInMeters(50000)
                .WithConductivities(3, 0.007);

            var fileName = GetFileName();

            ModelSettingsSerializer.SaveToXml(fileName, settings);
            var actual = ModelSettingsSerializer.LoadCommemiFromXml(fileName);

            CompareModelSettings(settings, actual);

            Assert.Equal(settings.AnomalySizeInMeters, actual.AnomalySizeInMeters);
            Assert.Equal(settings.LeftConductivity, actual.LeftConductivity);
            Assert.Equal(settings.RightConductivity, actual.RightConductivity);
        }

        [Fact()]
        public void CommemiSettingsManualBoundariesTest()
        {
            var mb = new ManualBoundaries(-500, -200, 3000, 2000);
            var mesh = new MeshParameters(10, 10, 20);
            var settings = new CommemiModelSettings(mesh, mb);

            var fileName = GetFileName();

            ModelSettingsSerializer.SaveToXml(fileName, settings);
            var actual = ModelSettingsSerializer.LoadCommemiFromXml(fileName);

            CompareModelSettings(settings, actual);

            Assert.Equal(settings.AnomalySizeInMeters, actual.AnomalySizeInMeters);
            Assert.Equal(settings.LeftConductivity, actual.LeftConductivity);
            Assert.Equal(settings.RightConductivity, actual.RightConductivity);
        }

        [Fact()]
        public void Commemi3D3Settings1Test()
        {
            var mesh = new MeshParameters(352, 448, 236);
            var settings = new Commemi3D3ModelSettings(mesh);

            var fileName = GetFileName();

            ModelSettingsSerializer.SaveToXml(fileName, settings);
            var actual = ModelSettingsSerializer.LoadCommemi3D3FromXml(fileName);

            CompareModelSettings(settings, actual);
        }

        [Fact()]
        public void Commemi3D3Settings2Test()
        {
            var mesh = new MeshParameters(352, 448, 236)
            {
                UseGeometricStepAlongZ = true,
                GeometricRation = 1.745,
            };

            var settings = new Commemi3D3ModelSettings(mesh);

            var fileName = GetFileName();

            ModelSettingsSerializer.SaveToXml(fileName, settings);
            var actual = ModelSettingsSerializer.LoadCommemi3D3FromXml(fileName);

            CompareModelSettings(settings, actual);
        }

        [Fact()]
        public void Commemi3D3SettingsManualBoundariesTest()
        {
            var mesh = new MeshParameters(352, 448, 236)
            {
                UseGeometricStepAlongZ = true,
                GeometricRation = 1.745,
            };

            var mb = new ManualBoundaries(-500, -200, 3000, 2000);

            var settings = new Commemi3D3ModelSettings(mesh, mb);

            var fileName = GetFileName();

            ModelSettingsSerializer.SaveToXml(fileName, settings);
            var actual = ModelSettingsSerializer.LoadCommemi3D3FromXml(fileName);

            CompareModelSettings(settings, actual);
        }


        private void CompareModelSettings(ModelSettings expected, ModelSettings actual)
        {
            Console.WriteLine($"{actual.Mesh.Nx}");

            Assert.Equal(expected.Mesh.Nx, actual.Mesh.Nx);
            Assert.Equal(expected.Mesh.Ny, actual.Mesh.Ny);
            Assert.Equal(expected.Mesh.Nz, actual.Mesh.Nz);

            Assert.Equal(expected.Mesh.UseGeometricStepAlongZ, actual.Mesh.UseGeometricStepAlongZ);

            if (actual.Mesh.UseGeometricStepAlongZ)
                Assert.Equal(expected.Mesh.GeometricRation, actual.Mesh.GeometricRation);

            bool actualIsAutoMb = actual.ManualBoundaries == ManualBoundaries.Auto;
            bool expectedIsAutoMb = expected.ManualBoundaries == ManualBoundaries.Auto;

            Assert.Equal(expectedIsAutoMb, actualIsAutoMb);

            if (!actualIsAutoMb)
            {
                Assert.Equal(expected.ManualBoundaries.StartX, actual.ManualBoundaries.StartX);
                Assert.Equal(expected.ManualBoundaries.StartY, actual.ManualBoundaries.StartY);
                Assert.Equal(expected.ManualBoundaries.EndX, actual.ManualBoundaries.EndX);
                Assert.Equal(expected.ManualBoundaries.EndY, actual.ManualBoundaries.EndY);
            }
        }

        [Fact()]
        public void EmptyAnomalyTest()
        {
            var model = CreateTestModelWithoutAnomaly();
            var loadedModel = SaveAndGetLoadedModel(model);

            CompareLateralDimensions(model.LateralDimensions, loadedModel.LateralDimensions);
            CompareBackgroundLayers(model.Section1D, loadedModel.Section1D);
            CompareAnomalyLayers(model.Anomaly, loadedModel.Anomaly);
        }

        [Fact()]
        public void LateralDimensionsTest()
        {
            var model = CreateTestModel();
            var loadedModel = SaveAndGetLoadedModel(model);

            CompareLateralDimensions(model.LateralDimensions, loadedModel.LateralDimensions);
        }

        [Fact()]
        public void BackgroundLayersTest()
        {
            var model = CreateTestModel();
            var loadedModel = SaveAndGetLoadedModel(model);

            CompareBackgroundLayers(model.Section1D, loadedModel.Section1D);
        }

        [Fact()]
        public void AnomalyLayersTest()
        {
            var model = CreateTestModel();
            var loadedModel = SaveAndGetLoadedModel(model);

            CompareAnomalyLayers(model.Anomaly, loadedModel.Anomaly);
        }

        private static CartesianModel SaveAndGetLoadedModel(CartesianModel model)
        {
            var fileName = GetFileName();
            return SaveAndGetLoadedModel(model, fileName);
        }

        private static CartesianModel SaveAndGetLoadedModel(CartesianModel model, string fileName)
        {
            SerializationManager.SaveModelWithPlaintTextAnomaly(fileName, model);
            var loadedModel = SerializationManager.LoadModel(fileName);
            return loadedModel;
        }


        private void CompareAnomalyLayers(CartesianAnomaly expected, CartesianAnomaly actual)
        {
            Assert.Equal(expected.LocalSize, actual.LocalSize);
            Assert.Equal(expected.Layers.Count, actual.Layers.Count);
            CompareAnomalyLayer(expected.Sigma, actual.Sigma);
        }

        private void CompareAnomalyLayer(double[,,] expected, double[,,] actual)
        {
            int nx = expected.GetLength(0);
            int ny = expected.GetLength(1);
            int nz = expected.GetLength(2);

            Assert.Equal(nx, actual.GetLength(0));
            Assert.Equal(ny, actual.GetLength(1));
            Assert.Equal(nz, actual.GetLength(2));

            double delta = 0.00001;

            for (int i = 0; i < nx; i++)
                for (int j = 0; j < ny; j++)
                    for (int k = 0; k < nz; k++)
                        Assert.Equal(expected[i, j, k], actual[i, j, k], _comparer);
        }

        private void CompareBackgroundLayers(CartesianSection1D expected, CartesianSection1D actual)
        {
            Assert.Equal(expected.ZeroAirLevelAlongZ, actual.ZeroAirLevelAlongZ);
            Assert.Equal(expected.NumberOfLayers, actual.NumberOfLayers);

            for (int i = 0; i < expected.NumberOfLayers; i++)
                CompareBackgroundLayer(expected[i], actual[i]);
        }

        private void CompareBackgroundLayer(Sigma1DLayer expected, Sigma1DLayer actual)
        {
            Assert.Equal(expected.Thickness, actual.Thickness);
            Assert.Equal(expected.Sigma, actual.Sigma);
        }

        private void CompareLateralDimensions(LateralDimensions expected, LateralDimensions actual)
        {
            Assert.Equal(expected.CellSizeX, actual.CellSizeX);
            Assert.Equal(expected.CellSizeY, actual.CellSizeY);
            Assert.Equal(expected.Nx, actual.Nx);
            Assert.Equal(expected.Ny, actual.Ny);
        }

        private CartesianModel CreateTestModel()
        {
            var lateralDimensions = CreateLateralDimansions();
            var section1D = CreateSection1D();
            var anomaly = CreateCartesianAnomaly();
            var model = new CartesianModel(lateralDimensions, section1D, anomaly);
            return model;
        }

        private CartesianModel CreateTestModelWithoutAnomaly()
        {
            var lateralDimensions = CreateLateralDimansions();
            var section1D = CreateSection1D();
            var anomaly = new CartesianAnomaly(new Size2D(lateralDimensions.Nx, lateralDimensions.Ny), new CartesianAnomalyLayer[0]);
            anomaly.CreateSigma();
            var model = new CartesianModel(lateralDimensions, section1D, anomaly);
            return model;
        }

        private CartesianAnomaly CreateCartesianAnomaly()
        {
            var anomalyLayers = new List<CartesianAnomalyLayer>
            {
                new CartesianAnomalyLayer(1000, 250),
                new CartesianAnomalyLayer(1250, 250),
                new CartesianAnomalyLayer(1500, 500)
            };

            var anomaly = new CartesianAnomaly(AnomalySize, anomalyLayers);

            anomaly.CreateSigma();
            FillAnomaly(anomaly);

            return anomaly;
        }

        private void FillAnomaly(CartesianAnomaly anomaly)
        {
            var size = anomaly.LocalSize;
            var rnd = new Random();

            for (int i = 0; i < size.Nx; i++)
                for (int j = 0; j < size.Ny; j++)
                    for (int k = 0; k < anomaly.Layers.Count; k++)
                        anomaly.Sigma[i, j, k] = (rnd.NextDouble() * 15 + 0.01);
        }

        private CartesianSection1D CreateSection1D()
        {
            return new CartesianSection1D(-20000, new[]
            {
                new Sigma1DLayer(0, 0),
                new Sigma1DLayer(20000, 0),
                new Sigma1DLayer(1000, 3.4),
                new Sigma1DLayer(2000, 1.4),
                new Sigma1DLayer(3000, 7.7),
                new Sigma1DLayer(0, 0.01),
            });
        }

        private LateralDimensions CreateLateralDimansions()
        {
            return new LateralDimensions(AnomalySize.Nx, AnomalySize.Ny, 234.5m, 333);
        }
    }
}
