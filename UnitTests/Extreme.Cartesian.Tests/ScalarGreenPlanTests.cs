﻿using System;
using Extreme.Cartesian.Green.Scalar;
using Extreme.Core;
using Porvem.Tests.Utils;
using Xunit;
using static System.Math;

namespace Extreme.Cartesian.Tests
{
    public class ScalarGreenPlanTests
    {
        private readonly DoubleEqualityComparer _comparer = new DoubleEqualityComparer(1E-7);

        [Fact()]
        public void FindRhoMaxLevelTest()
        {
            var dims = new LateralDimensions(2, 4, 20000, 10000);

            Assert.Equal(Sqrt(3725) * 1000, ScalarPlansCreater.FindRhoMax(dims, 1 * dims.CellSizeX, 0), _comparer);
            Assert.Equal(Sqrt(3725) * 1000, ScalarPlansCreater.FindRhoMax(dims, -1 * dims.CellSizeX, 0), _comparer);

            Assert.Equal(Sqrt(30 * 30 + 45 * 45) * 1000, ScalarPlansCreater.FindRhoMax(dims, 0, 1 * dims.CellSizeY), _comparer);
            Assert.Equal(Sqrt(30 * 30 + 45 * 45) * 1000, ScalarPlansCreater.FindRhoMax(dims, 0, -1 * dims.CellSizeY), _comparer);
        }

        [Fact()]
        public void FindRhoMinXLevelTest()
        {
            var dims = new LateralDimensions(2, 4, 20000, 10000);

            Assert.Equal(10000, ScalarPlansCreater.FindRhoMinX(dims, 0 * dims.CellSizeX), _comparer);
            Assert.Equal(10000, ScalarPlansCreater.FindRhoMinX(dims, 1 * dims.CellSizeX), _comparer);
            Assert.Equal(10000, ScalarPlansCreater.FindRhoMinX(dims, 2 * dims.CellSizeX), _comparer);
            Assert.Equal(30000, ScalarPlansCreater.FindRhoMinX(dims, 3 * dims.CellSizeX), _comparer);
            Assert.Equal(50000, ScalarPlansCreater.FindRhoMinX(dims, 4 * dims.CellSizeX), _comparer);

            Assert.Equal(10000, ScalarPlansCreater.FindRhoMinX(dims, -1 * dims.CellSizeX), _comparer);
            Assert.Equal(10000, ScalarPlansCreater.FindRhoMinX(dims, -2 * dims.CellSizeX), _comparer);
            Assert.Equal(30000, ScalarPlansCreater.FindRhoMinX(dims, -3 * dims.CellSizeX), _comparer);
            Assert.Equal(50000, ScalarPlansCreater.FindRhoMinX(dims, -4 * dims.CellSizeX), _comparer);
        }

        [Fact()]
        public void FindRhoMinStoOLevelTest()
        {
            var dims = new LateralDimensions(2, 4, 10000, 2500);


            Action<double, SourceLayer, ObservationLevel> check = (v, s, d) =>
                Assert.Equal(v, ScalarPlansCreater.FindRhoMin(dims, s, d), _comparer);

            Assert.Equal(0.1, 0.1000000001, _comparer);

            // inside
            check(1250, new SourceLayer(0, 0, 0), new ObservationLevel(0, 0, 0));
            check(1249, new SourceLayer(0, 0, 0), new ObservationLevel(3751, 0, 0));
            check(0, new SourceLayer(0, 0, 0), new ObservationLevel(5000, 0, 0));
            check(263, new SourceLayer(0, 0, 0), new ObservationLevel(4737, 0, 0));
            check(1, new SourceLayer(0, 0, 0), new ObservationLevel(4999, 0, 0));
            check(11, new SourceLayer(0, 0, 0), new ObservationLevel(5011, 0, 0));
            check(1248, new SourceLayer(0, 0, 0), new ObservationLevel(13752, 0, 0));

            // ouside

            check(1251, new SourceLayer(0, 0, 0), new ObservationLevel(0, 10001, 0));
            check(1251, new SourceLayer(0, 0, 0), new ObservationLevel(0, -10001, 0));
            check(15000, new SourceLayer(0, 0, 0), new ObservationLevel(30000, 10000000, 0));

            check(0, new SourceLayer(5000, 0, 0), new ObservationLevel(0, 0, 0));
            check(0, new SourceLayer(-15000, 0, 0), new ObservationLevel(0, 0, 0));
            check(1, new SourceLayer(-15001, 0, 0), new ObservationLevel(0, 0, 0));
            check(13, new SourceLayer(-15013, 0, 0), new ObservationLevel(0, 0, 0));


            check(5000, new SourceLayer(20000, 0, 0), new ObservationLevel(0, 10000000, 0));
            check(5000, new SourceLayer(10000, 0, 0), new ObservationLevel(0, 10000000, 0));
            check(25000, new SourceLayer(-20000, 0, 0), new ObservationLevel(20000, 10000000, 0));
        }

        [Fact()]
        public void FindRhoMaxStoOLevelTest()
        {
            var dims = new LateralDimensions(2, 4, 10000, 2500);

            Action<double, SourceLayer, ObservationLevel> check = (v, s, d) =>
                Assert.Equal(v, ScalarPlansCreater.FindRhoMax(dims, s, d), _comparer);

            Func<decimal, decimal, decimal, decimal, double> calc = (x1, y1, x2, y2) =>
                Sqrt((double)((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2)));

            check(calc(0, 0, (dims.Nx - 0.5m) * dims.CellSizeX, (dims.Ny - 0.5m) * dims.CellSizeY), new SourceLayer(0, 0, 0), new ObservationLevel(0, 0, 0));
            check(calc(0, 0, (dims.Nx - 0.5m) * dims.CellSizeX + 1, (dims.Ny - 0.5m) * dims.CellSizeY), new SourceLayer(1, 0, 0), new ObservationLevel(0, 0, 0));
            check(calc(0, 0, (dims.Nx - 0.5m) * dims.CellSizeX + 1000, (dims.Ny - 0.5m) * dims.CellSizeY), new SourceLayer(1000, 0, 0), new ObservationLevel(0, 0, 0));
            check(calc(0, 0, (dims.Nx - 0.5m) * dims.CellSizeX + 1500, (dims.Ny - 0.5m) * dims.CellSizeY), new SourceLayer(1000, 0, 0), new ObservationLevel(-500, 0, 0));

            check(calc(0, 0, (dims.Nx - 0.5m) * dims.CellSizeX + 20000, (dims.Ny - 0.5m) * dims.CellSizeY + 10000), new SourceLayer(-20000, -10000, 0), new ObservationLevel(0, 0, 0));

            dims = new LateralDimensions(16, 16, 2500, 2500);
            check(calc(0, -20000, 78750, 78750), new SourceLayer(40000, 40000, 0), new ObservationLevel(-20000, 0, 0));
        }

        [Fact()]
        public void FindRhoMinYLevelTest()
        {
            var dims = new LateralDimensions(2, 4, 20000, 10000);

            Assert.Equal(5000, ScalarPlansCreater.FindRhoMinY(dims, 0 * dims.CellSizeY), _comparer);
            Assert.Equal(5000, ScalarPlansCreater.FindRhoMinY(dims, 1 * dims.CellSizeY), _comparer);
            Assert.Equal(5000, ScalarPlansCreater.FindRhoMinY(dims, 2 * dims.CellSizeY), _comparer);
            Assert.Equal(5000, ScalarPlansCreater.FindRhoMinY(dims, 3 * dims.CellSizeY), _comparer);
            Assert.Equal(5000, ScalarPlansCreater.FindRhoMinY(dims, 4 * dims.CellSizeY), _comparer);
            Assert.Equal(15000, ScalarPlansCreater.FindRhoMinY(dims, 5 * dims.CellSizeY), _comparer);
            Assert.Equal(25000, ScalarPlansCreater.FindRhoMinY(dims, 6 * dims.CellSizeY), _comparer);

            Assert.Equal(5000, ScalarPlansCreater.FindRhoMinY(dims, -1 * dims.CellSizeY), _comparer);
            Assert.Equal(5000, ScalarPlansCreater.FindRhoMinY(dims, -2 * dims.CellSizeY), _comparer);
            Assert.Equal(5000, ScalarPlansCreater.FindRhoMinY(dims, -3 * dims.CellSizeY), _comparer);
            Assert.Equal(5000, ScalarPlansCreater.FindRhoMinY(dims, -4 * dims.CellSizeY), _comparer);
            Assert.Equal(15000, ScalarPlansCreater.FindRhoMinY(dims, -5 * dims.CellSizeY), _comparer);
            Assert.Equal(25000, ScalarPlansCreater.FindRhoMinY(dims, -6 * dims.CellSizeY), _comparer);
        }


        [Fact()]
        public void FindMinYInsideTest()
        {
            var dims = new LateralDimensions(400, 100, 250, 1000);

            Func<LateralDimensions, ObservationSite, decimal> find = ScalarPlansCreater.FindMinYInsideAnomaly;

            Assert.Equal(500, find(dims, new ObservationSite(125, 500, 0)));
            Assert.Equal(499, find(dims, new ObservationSite(125, 501, 0)));
            Assert.Equal(499, find(dims, new ObservationSite(125, 499, 0)));
            Assert.Equal(100, find(dims, new ObservationSite(125, 99900, 0)));
        }

        [Fact()]
        public void FindMinXInsideTest()
        {
            var dims = new LateralDimensions(400, 100, 250, 1000);

            Func<LateralDimensions, ObservationSite, decimal> find = ScalarPlansCreater.FindMinXInsideAnomaly;

            Assert.Equal(125, find(dims, new ObservationSite(125, 500, 0)));
            Assert.Equal(124, find(dims, new ObservationSite(126, 501, 0)));
            Assert.Equal(124, find(dims, new ObservationSite(124, 499, 0)));
            Assert.Equal(20, find(dims, new ObservationSite(99980, 99900, 0)));
        }
    }
}