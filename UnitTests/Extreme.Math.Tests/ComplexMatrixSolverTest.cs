﻿using System.Numerics;
using Extreme.Core;

namespace Extreme.Math.Tests
{
 
    public class ComplexMatrixSolverTest
    {
        protected const string TestCategory = @"Solver tests";
        
        public static Complex[,] CreateToeplitzMatrix(int order, double gamma)
        {
            var matrix = new Complex[order, order];

            FillDiagonal(matrix, 0, new Complex(4, 0));
            FillDiagonal(matrix, 2, new Complex(1, 0));
            FillDiagonal(matrix, 3, new Complex(.7, 0));

            FillDiagonal(matrix, -1, new Complex(0, gamma));

            return matrix;
        }

        public static Complex[] CreateVector(int order, Complex value)
        {
            var vector = new Complex[order];

            for (int i = 0; i < vector.Length; i++)
                vector[i] = value;

            return vector;
        }

   
        public static void FillDiagonal(Complex[,] matrix, int shift, Complex value)
        {
            int limit = matrix.GetLength(0) - System.Math.Abs(shift);

            if (System.Math.Sign(shift) >= 0)
            {
                for (int i = 0; i < limit; i++)
                    matrix[i, i + shift] = value;
            }
            else
            {
                for (int i = 0; i < limit; i++)
                    matrix[i - shift, i] = value;
            }
        }
    }
}
