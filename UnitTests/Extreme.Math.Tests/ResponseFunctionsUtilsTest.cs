﻿using System;
using System.Numerics;
using Extreme.Cartesian;
using Porvem.Magnetotellurics;
using Porvem.Tests.Utils;
using Xunit;

namespace Extreme.Math.Tests
{
    public class ResponseFunctionsUtilsTest : IDisposable
    {
        private readonly DoubleEqualityComparer _comparer = new DoubleEqualityComparer(1E-7);

        private Random _random;

        private Complex _a;
        private Complex _b;
        private Complex _c;
        private Complex _d;

        private Complex _hz1;
        private Complex _hz2;


        public ResponseFunctionsUtilsTest()
        {
            _random = new Random(DateTime.Now.Millisecond);

            _a = GetRandomSingleComplex();
            _b = GetRandomSingleComplex();
            _c = GetRandomSingleComplex();
            _d = GetRandomSingleComplex();

            _hz1 = GetRandomSingleComplex();
            _hz2 = GetRandomSingleComplex();
        }

        public void Dispose()
        {
            ClearRandom();
        }

        private void ClearRandom()
        {
            _random = null;

            _a = Complex.Zero;
            _b = Complex.Zero;
            _c = Complex.Zero;
            _d = Complex.Zero;

            _hz1 = Complex.Zero;
            _hz2 = Complex.Zero;
        }

        private Complex GetRandomSingleComplex()
        {
            var re = (_random.Next(-100, 100) * System.Math.PI);
            var im = (_random.Next(-100, 100) * System.Math.PI);

            return new Complex(re, im);
        }


        private class TestComplex
        {
            public TestComplex(ComplexVector e1, ComplexVector e2, ComplexVector h1, ComplexVector h2)
            {
                E1 = e1;
                E2 = e2;
                H1 = h1;
                H2 = h2;
            }

            public ComplexVector E1 { get; }
            public ComplexVector E2 { get; }
            public ComplexVector H1 { get; }
            public ComplexVector H2 { get; }
        }

        private TestComplex CreateTestComplect()
        {
            // we use Vandermonde matrix filling
            // to make it simplier to calculate analytic answer

            var e1 = new ComplexVector(Complex.One, Complex.One, Complex.Zero);
            var e2 = new ComplexVector(_a, _b, Complex.Zero);

            var h1 = new ComplexVector(Complex.One, Complex.One, _hz1);
            var h2 = new ComplexVector(_c, _d, _hz2);

            var result = new TestComplex(e1, e2, h1, h2);

            return result;
        }

        [Fact]
        public void CalculateImpedanceTest()
        {
            var c = CreateTestComplect();

            var impedance = ResponseFunctionsCalculator.CalculateImpedanceTensor(c.E1, c.E2, c.H1, c.H2);

            Check(impedance);

        }

        [Fact]
        public void CalculateTipperTest()
        {
            var c = CreateTestComplect();

            var tipper = ResponseFunctionsCalculator.CalculateTipper(c.H1, c.H2);

            Check(tipper);
        }

        private void Check(Tipper tipper)
        {
            var d = 1 / (_d - _c);

            var expWzx = (_hz1 * _d - _hz2) * d;
            var expWzy = (_hz1 * -_c + _hz2) * d;

            CompareSingleComplex(expWzx, tipper.Zx);
            CompareSingleComplex(expWzy, tipper.Zy);
        }

        private void Check(ImpedanceTensor impedanceTensor)
        {
            var d = 1 / (_d - _c);

            var expZxx = (_d - _a) * d;
            var expZxy = (_a - _c) * d;
            var expZyx = (_d - _b) * d;
            var expZyy = (_b - _c) * d;

            CompareSingleComplex(expZxx, impedanceTensor.Xx);
            CompareSingleComplex(expZxy, impedanceTensor.Xy);
            CompareSingleComplex(expZyx, impedanceTensor.Yx);
            CompareSingleComplex(expZyy, impedanceTensor.Yy);
        }
        
        private void CompareSingleComplex(Complex exp, Complex act)
        {
            Assert.Equal(exp.Real, act.Real, _comparer);
            Assert.Equal(exp.Imaginary, act.Imaginary, _comparer);
        }
    }
}
