﻿using Extreme.Cartesian;

namespace Extreme.Tests.Utils
{
    public interface IFieldPointComparator
    {
        double Compare(ComplexVector[,] fp1, ComplexVector[,] fp2);
    }
}
