﻿using System;
using System.Numerics;
using Extreme.Cartesian;

namespace Extreme.Tests.Utils
{
    public class FieldPointComparator : IFieldPointComparator
    {
        public double Compare(ComplexVector[,] fp1, ComplexVector[,] fp2)
        {
            if (fp1.GetLength(0) != fp2.GetLength(0) ||
                fp1.GetLength(1) != fp2.GetLength(1))
                throw new ArgumentOutOfRangeException();

            return ComparePrivate(fp1, fp2);
        }

        private double ComparePrivate(ComplexVector[,] fp1, ComplexVector[,] fp2)
        {
            double result = 0;

            double length = fp1.Length;

            for (int i = 0; i < fp1.GetLength(0); i++)
                for (int j = 0; j < fp1.GetLength(1); j++)
                    result += (Compare(fp1[i, j], fp2[i, j]) / length);

            return result;
        }

        private double Compare(ComplexVector fp1, ComplexVector fp2)
        {
            double x = CalculateFor(fp1, fp2, (fp => fp.X));
            double y = CalculateFor(fp1, fp2, (fp => fp.Y));
            double z = CalculateFor(fp1, fp2, (fp => fp.Z));

            return (x + y + z)/3;
        }

        private double CalculateFor(ComplexVector fp1, ComplexVector fp2, Func<ComplexVector, Complex> func)
        {
            return ComplexMath.GetMagnitudeDistance(func(fp1), func(fp2));
        }
    }
}
