﻿using System.Collections.Generic;
using static System.Math;

namespace Porvem.Tests.Utils
{

    public class DoubleEqualityComparer : IEqualityComparer<double>
    {
        private readonly double _epsilon = 1E-7;

        public DoubleEqualityComparer(double epsilon)
        {
            _epsilon = epsilon;
        }

        public bool Equals(double x, double y)
            => Abs(x - y) < _epsilon;

        public int GetHashCode(double obj)
            => obj.GetHashCode();
    }
}
