﻿using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Numerics;
using ComplexVector = Extreme.Cartesian.ComplexVector;

namespace Extreme.Tests.Utils
{
    public class X3DOutputFieldReader
    {
        public static ComplexVector[,] LoadFromBytes(byte[] bytes)
        {
            using (var sr = new StreamReader(new MemoryStream(bytes)))
                return LoadFromStream(sr);
        }

        public static ComplexVector[,] LoadFromFile(string fileName)
        {
            using (var sr = new StreamReader(fileName))
                return LoadFromStream(sr);
        }

        private struct Row
        {
            public int IndexX;
            public int IndexY;
            public float ReX;
            public float ImX;
            public float ReY;
            public float ImY;
            public float ReZ;
            public float ImZ;

        }

        private static ComplexVector[,] LoadFromStream(StreamReader sr)
        {
            var rows = new List<Row>();

            while (!sr.EndOfStream)
                rows.Add(ParseLine(sr.ReadLine()));

            int maxX = rows.Max(r => r.IndexX);
            int maxY = rows.Max(r => r.IndexY);

            int minX = rows.Min(r => r.IndexX);
            int minY = rows.Min(r => r.IndexY);


            var result = new ComplexVector[maxX - minX + 1, maxY - minY + 1];

            foreach (var row in rows)
                result[row.IndexX - minX, row.IndexY - minY] = CreateFieldPointFrom(row);

            return result;
        }

        private static ComplexVector CreateFieldPointFrom(Row row)
        {
            var x = new Complex(row.ReX, row.ImX);
            var y = new Complex(row.ReY, row.ImY);
            var z = new Complex(row.ReZ, row.ImZ);

            return new ComplexVector(x, y, z);
        }

        private static Row ParseLine(string line)
        {
            var strings = SplitString(line);

            return new Row()
            {
                IndexX = GetInt(strings[1]),
                IndexY = GetInt(strings[0]),
                ReX = GetFloat(strings[2]),
                ImX = GetFloat(strings[3]),
                ReY = GetFloat(strings[4]),
                ImY = GetFloat(strings[5]),
                ReZ = GetFloat(strings[6]),
                ImZ = GetFloat(strings[7]),
            };
        }

        private static List<string> SplitString(string line)
        {
            return line.Split(' ').Where(s => !string.IsNullOrEmpty(s)).ToList();
        }

        private static int GetInt(string str)
        {
            return int.Parse(str);
        }

        private static float GetFloat(string str)
        {
            return float.Parse(str, NumberStyles.Float, CultureInfo.InvariantCulture);
        }
    }
}
