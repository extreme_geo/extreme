﻿using System;
using System.Numerics;

namespace Extreme.Tests.Utils
{
    public static class ComplexMath
    {
        public static double GetMagnitudeDistance(Complex c1, Complex c2)
        {
            return System.Math.Sqrt((c1.Magnitude - c2.Magnitude) * (c1.Magnitude - c2.Magnitude));
        }
    }
}
